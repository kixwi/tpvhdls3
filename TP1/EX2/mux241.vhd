library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity entitymux241 is
	port(
		Y1, Y2 : in std_logic_vector(3 downto 0);
		Sel : in std_logic;
		Z1 : out std_logic_vector(3 downto 0)
	);
end;

architecture descript of entitymux241 is
begin
	WITH Sel SELECT
		Z1 <= Y1 WHEN '0', Y2 WHEN '1', Y1 WHEN OTHERS;
END descript;