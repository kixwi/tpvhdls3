LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY ENTITYDECOD IS
	PORT(
		A : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
		S : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END;

ARCHITECTURE DESCRIPT OF ENTITYDECOD IS
BEGIN
	WITH A SELECT
		S <= "0001" WHEN "00",
			 "0010" WHEN "01",
			 "0100" WHEN "10",
			 "1000" WHEN "11",
			 "0000" WHEN OTHERS;
 END DESCRIPT;