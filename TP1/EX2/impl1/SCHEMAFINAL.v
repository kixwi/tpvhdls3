/* Verilog model created from schematic SCHEMAFINAL.sch -- Sep 20, 2023 16:27 */

module SCHEMAFINAL( A, Q, SEL, SORTIESDECOD, Y1, Y2 );
 input [1:0] A;
output [6:0] Q;
 input SEL;
output [3:0] SORTIESDECOD;
 input [3:0] Y1;
 input [3:0] Y2;
  wire [3:0] Z1;
wire N_3;
wire N_4;
wire N_5;
wire N_2;



VLO I5 ( .Z(N_4) );
VLO I6 ( .Z(N_5) );
VHI I7 ( .Z(N_3) );
defparam I1.NOM_FREQ="2.08";
OSCH I1 ( .OSC(N_2), .STDBY(N_5) );
entitymux241 I2 ( .Sel(SEL), .Y1(Y1[3:0]), .Y2(Y2[3:0]), .Z1(Z1[3:0]) );
ENTITYDECOD I3 ( .A(A[1:0]), .S(SORTIESDECOD[3:0]) );
decodaffich I4 ( .Address(Z1[3:0]), .OutClock(N_2), .OutClockEn(N_3), .Q(Q[6:0]),
              .Reset(N_4) );

endmodule // SCHEMAFINAL
