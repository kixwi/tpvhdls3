// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Tue Sep 19 20:53:34 2023
//
// Verilog Description of module decodaffich
//

module decodaffich (Address, OutClock, OutClockEn, Reset, Q) /* synthesis NGD_DRC_MASK=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(14[8:19])
    input [3:0]Address;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(16[9:16])
    input OutClock;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(17[9:17])
    input OutClockEn;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(18[9:19])
    input Reset;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(19[9:14])
    output [6:0]Q;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(20[9:10])
    
    wire OutClock_c /* synthesis is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(17[9:17])
    
    wire Address_c_3, Address_c_2, Address_c_1, Address_c_0, OutClockEn_c, 
        Reset_c, Q_c_6, Q_c_5, Q_c_4, Q_c_3, Q_c_2, Q_c_1, Q_c_0, 
        scuba_vlo, VCC_net;
    
    VLO scuba_vlo_inst (.Z(scuba_vlo));
    DP8KC decodaffich_0_0_0 (.DIA0(scuba_vlo), .DIA1(scuba_vlo), .DIA2(scuba_vlo), 
          .DIA3(scuba_vlo), .DIA4(scuba_vlo), .DIA5(scuba_vlo), .DIA6(scuba_vlo), 
          .DIA7(scuba_vlo), .DIA8(scuba_vlo), .ADA0(VCC_net), .ADA1(scuba_vlo), 
          .ADA2(scuba_vlo), .ADA3(Address_c_0), .ADA4(Address_c_1), .ADA5(Address_c_2), 
          .ADA6(Address_c_3), .ADA7(scuba_vlo), .ADA8(scuba_vlo), .ADA9(scuba_vlo), 
          .ADA10(scuba_vlo), .ADA11(scuba_vlo), .ADA12(scuba_vlo), .CEA(OutClockEn_c), 
          .OCEA(OutClockEn_c), .CLKA(OutClock_c), .WEA(scuba_vlo), .CSA0(scuba_vlo), 
          .CSA1(scuba_vlo), .CSA2(scuba_vlo), .RSTA(Reset_c), .DIB0(scuba_vlo), 
          .DIB1(scuba_vlo), .DIB2(scuba_vlo), .DIB3(scuba_vlo), .DIB4(scuba_vlo), 
          .DIB5(scuba_vlo), .DIB6(scuba_vlo), .DIB7(scuba_vlo), .DIB8(scuba_vlo), 
          .ADB0(scuba_vlo), .ADB1(scuba_vlo), .ADB2(scuba_vlo), .ADB3(scuba_vlo), 
          .ADB4(scuba_vlo), .ADB5(scuba_vlo), .ADB6(scuba_vlo), .ADB7(scuba_vlo), 
          .ADB8(scuba_vlo), .ADB9(scuba_vlo), .ADB10(scuba_vlo), .ADB11(scuba_vlo), 
          .ADB12(scuba_vlo), .CEB(VCC_net), .OCEB(VCC_net), .CLKB(scuba_vlo), 
          .WEB(scuba_vlo), .CSB0(scuba_vlo), .CSB1(scuba_vlo), .CSB2(scuba_vlo), 
          .RSTB(scuba_vlo), .DOA0(Q_c_0), .DOA1(Q_c_1), .DOA2(Q_c_2), 
          .DOA3(Q_c_3), .DOA4(Q_c_4), .DOA5(Q_c_5), .DOA6(Q_c_6)) /* synthesis MEM_LPC_FILE="decodaffich.lpc", MEM_INIT_FILE="memoire.mem", syn_instantiated=1 */ ;
    defparam decodaffich_0_0_0.DATA_WIDTH_A = 9;
    defparam decodaffich_0_0_0.DATA_WIDTH_B = 9;
    defparam decodaffich_0_0_0.REGMODE_A = "OUTREG";
    defparam decodaffich_0_0_0.REGMODE_B = "NOREG";
    defparam decodaffich_0_0_0.CSDECODE_A = "0b000";
    defparam decodaffich_0_0_0.CSDECODE_B = "0b111";
    defparam decodaffich_0_0_0.WRITEMODE_A = "NORMAL";
    defparam decodaffich_0_0_0.WRITEMODE_B = "NORMAL";
    defparam decodaffich_0_0_0.GSR = "ENABLED";
    defparam decodaffich_0_0_0.RESETMODE = "SYNC";
    defparam decodaffich_0_0_0.ASYNC_RESET_RELEASE = "SYNC";
    defparam decodaffich_0_0_0.INIT_DATA = "STATIC";
    defparam decodaffich_0_0_0.INITVAL_00 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_01 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_02 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_03 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_04 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_05 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_06 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_07 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_08 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_09 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_0A = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_0B = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_0C = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_0D = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_0E = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_0F = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_10 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_11 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_12 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_13 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_14 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_15 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_16 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_17 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_18 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_19 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_1A = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_1B = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_1C = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_1D = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_1E = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_1F = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    OB Q_pad_4 (.I(Q_c_4), .O(Q[4]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(20[9:10])
    OB Q_pad_5 (.I(Q_c_5), .O(Q[5]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(20[9:10])
    OB Q_pad_6 (.I(Q_c_6), .O(Q[6]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(20[9:10])
    OB Q_pad_3 (.I(Q_c_3), .O(Q[3]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(20[9:10])
    OB Q_pad_2 (.I(Q_c_2), .O(Q[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(20[9:10])
    OB Q_pad_1 (.I(Q_c_1), .O(Q[1]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(20[9:10])
    OB Q_pad_0 (.I(Q_c_0), .O(Q[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(20[9:10])
    IB Address_pad_3 (.I(Address[3]), .O(Address_c_3));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(16[9:16])
    IB Address_pad_2 (.I(Address[2]), .O(Address_c_2));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(16[9:16])
    IB Address_pad_1 (.I(Address[1]), .O(Address_c_1));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(16[9:16])
    IB Address_pad_0 (.I(Address[0]), .O(Address_c_0));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(16[9:16])
    IB OutClock_pad (.I(OutClock), .O(OutClock_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(17[9:17])
    IB OutClockEn_pad (.I(OutClockEn), .O(OutClockEn_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(18[9:19])
    IB Reset_pad (.I(Reset), .O(Reset_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decodaffich.vhd(19[9:14])
    GSR GSR_INST (.GSR(VCC_net));
    TSALL TSALL_INST (.TSALL(scuba_vlo));
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    VHI i6 (.Z(VCC_net));
    
endmodule
//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

