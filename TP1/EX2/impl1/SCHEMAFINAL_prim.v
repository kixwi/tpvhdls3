// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Wed Sep 20 16:37:41 2023
//
// Verilog Description of module SCHEMAFINAL
//

module SCHEMAFINAL (A, Q, SEL, SORTIESDECOD, Y1, Y2) /* synthesis syn_module_defined=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(3[8:19])
    input [1:0]A;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(4[14:15])
    output [6:0]Q;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(5[14:15])
    input SEL;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(6[8:11])
    output [3:0]SORTIESDECOD;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(7[14:26])
    input [3:0]Y1;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(8[14:16])
    input [3:0]Y2;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(9[14:16])
    
    wire N_2 /* synthesis is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(14[6:9])
    
    wire A_c_1, A_c_0, Q_c_6, Q_c_5, Q_c_4, Q_c_3, Q_c_2, Q_c_1, 
        Q_c_0, SEL_c, SORTIESDECOD_c_3, SORTIESDECOD_c_2, SORTIESDECOD_c_1, 
        SORTIESDECOD_c_0, Y1_c_3, Y1_c_2, Y1_c_1, Y1_c_0, Y2_c_3, 
        Y2_c_2, Y2_c_1, Y2_c_0;
    wire [3:0]Z1;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(10[14:16])
    
    wire N_3, N_5;
    
    VLO I6 (.Z(N_5));
    VHI I7 (.Z(N_3));
    OSCH I1 (.STDBY(N_5), .OSC(N_2)) /* synthesis syn_instantiated=1 */ ;
    defparam I1.NOM_FREQ = "2.08";
    PUR PUR_INST (.PUR(N_3));
    defparam PUR_INST.RST_PULSE = 1;
    TSALL TSALL_INST (.TSALL(N_5));
    ENTITYDECOD I3 (.A_c_0(A_c_0), .A_c_1(A_c_1), .SORTIESDECOD_c_0(SORTIESDECOD_c_0), 
            .SORTIESDECOD_c_1(SORTIESDECOD_c_1), .SORTIESDECOD_c_2(SORTIESDECOD_c_2), 
            .SORTIESDECOD_c_3(SORTIESDECOD_c_3));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(24[13:53])
    OB Q_pad_6 (.I(Q_c_6), .O(Q[6]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(5[14:15])
    entitymux241 I2 (.Y1_c_3(Y1_c_3), .Y2_c_3(Y2_c_3), .SEL_c(SEL_c), 
            .Z1({Z1}), .Y1_c_2(Y1_c_2), .Y2_c_2(Y2_c_2), .Y1_c_0(Y1_c_0), 
            .Y2_c_0(Y2_c_0), .Y1_c_1(Y1_c_1), .Y2_c_1(Y2_c_1));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(23[14:72])
    OB Q_pad_5 (.I(Q_c_5), .O(Q[5]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(5[14:15])
    OB Q_pad_4 (.I(Q_c_4), .O(Q[4]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(5[14:15])
    OB Q_pad_3 (.I(Q_c_3), .O(Q[3]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(5[14:15])
    OB Q_pad_2 (.I(Q_c_2), .O(Q[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(5[14:15])
    OB Q_pad_1 (.I(Q_c_1), .O(Q[1]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(5[14:15])
    OB Q_pad_0 (.I(Q_c_0), .O(Q[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(5[14:15])
    OB SORTIESDECOD_pad_3 (.I(SORTIESDECOD_c_3), .O(SORTIESDECOD[3]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(7[14:26])
    OB SORTIESDECOD_pad_2 (.I(SORTIESDECOD_c_2), .O(SORTIESDECOD[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(7[14:26])
    OB SORTIESDECOD_pad_1 (.I(SORTIESDECOD_c_1), .O(SORTIESDECOD[1]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(7[14:26])
    OB SORTIESDECOD_pad_0 (.I(SORTIESDECOD_c_0), .O(SORTIESDECOD[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(7[14:26])
    IB A_pad_1 (.I(A[1]), .O(A_c_1));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(4[14:15])
    IB A_pad_0 (.I(A[0]), .O(A_c_0));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(4[14:15])
    IB SEL_pad (.I(SEL), .O(SEL_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(6[8:11])
    IB Y1_pad_3 (.I(Y1[3]), .O(Y1_c_3));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(8[14:16])
    IB Y1_pad_2 (.I(Y1[2]), .O(Y1_c_2));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(8[14:16])
    IB Y1_pad_1 (.I(Y1[1]), .O(Y1_c_1));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(8[14:16])
    IB Y1_pad_0 (.I(Y1[0]), .O(Y1_c_0));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(8[14:16])
    IB Y2_pad_3 (.I(Y2[3]), .O(Y2_c_3));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(9[14:16])
    IB Y2_pad_2 (.I(Y2[2]), .O(Y2_c_2));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(9[14:16])
    IB Y2_pad_1 (.I(Y2[1]), .O(Y2_c_1));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(9[14:16])
    IB Y2_pad_0 (.I(Y2[0]), .O(Y2_c_0));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(9[14:16])
    GSR GSR_INST (.GSR(N_3));
    decodaffich I4 (.Z1({Z1}), .N_2(N_2), .N_3(N_3), .N_5(N_5), .Q_c_6(Q_c_6), 
            .Q_c_5(Q_c_5), .Q_c_4(Q_c_4), .Q_c_3(Q_c_3), .Q_c_2(Q_c_2), 
            .Q_c_1(Q_c_1), .Q_c_0(Q_c_0)) /* synthesis NGD_DRC_MASK=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(25[13] 26[28])
    
endmodule
//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module ENTITYDECOD
//

module ENTITYDECOD (A_c_0, A_c_1, SORTIESDECOD_c_0, SORTIESDECOD_c_1, 
            SORTIESDECOD_c_2, SORTIESDECOD_c_3);
    input A_c_0;
    input A_c_1;
    output SORTIESDECOD_c_0;
    output SORTIESDECOD_c_1;
    output SORTIESDECOD_c_2;
    output SORTIESDECOD_c_3;
    
    
    LUT4 i18_2_lut (.A(A_c_0), .B(A_c_1), .Z(SORTIESDECOD_c_0)) /* synthesis lut_function=(!(A+(B))) */ ;
    defparam i18_2_lut.init = 16'h1111;
    LUT4 i12_2_lut (.A(A_c_0), .B(A_c_1), .Z(SORTIESDECOD_c_1)) /* synthesis lut_function=(!((B)+!A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decod.vhd(15[2] 20[24])
    defparam i12_2_lut.init = 16'h2222;
    LUT4 i9_2_lut (.A(A_c_0), .B(A_c_1), .Z(SORTIESDECOD_c_2)) /* synthesis lut_function=(!(A+!(B))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decod.vhd(15[2] 20[24])
    defparam i9_2_lut.init = 16'h4444;
    LUT4 i10_2_lut (.A(A_c_0), .B(A_c_1), .Z(SORTIESDECOD_c_3)) /* synthesis lut_function=(A (B)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decod.vhd(15[2] 20[24])
    defparam i10_2_lut.init = 16'h8888;
    
endmodule
//
// Verilog Description of module entitymux241
//

module entitymux241 (Y1_c_3, Y2_c_3, SEL_c, Z1, Y1_c_2, Y2_c_2, 
            Y1_c_0, Y2_c_0, Y1_c_1, Y2_c_1);
    input Y1_c_3;
    input Y2_c_3;
    input SEL_c;
    output [3:0]Z1;
    input Y1_c_2;
    input Y2_c_2;
    input Y1_c_0;
    input Y2_c_0;
    input Y1_c_1;
    input Y2_c_1;
    
    
    LUT4 Y2_3__I_0_i4_3_lut (.A(Y1_c_3), .B(Y2_c_3), .C(SEL_c), .Z(Z1[3])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/mux241.vhd(16[2] 17[50])
    defparam Y2_3__I_0_i4_3_lut.init = 16'hcaca;
    LUT4 Y2_3__I_0_i3_3_lut (.A(Y1_c_2), .B(Y2_c_2), .C(SEL_c), .Z(Z1[2])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/mux241.vhd(16[2] 17[50])
    defparam Y2_3__I_0_i3_3_lut.init = 16'hcaca;
    LUT4 Y2_3__I_0_i1_3_lut (.A(Y1_c_0), .B(Y2_c_0), .C(SEL_c), .Z(Z1[0])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/mux241.vhd(16[2] 17[50])
    defparam Y2_3__I_0_i1_3_lut.init = 16'hcaca;
    LUT4 Y2_3__I_0_i2_3_lut (.A(Y1_c_1), .B(Y2_c_1), .C(SEL_c), .Z(Z1[1])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/mux241.vhd(16[2] 17[50])
    defparam Y2_3__I_0_i2_3_lut.init = 16'hcaca;
    
endmodule
//
// Verilog Description of module decodaffich
//

module decodaffich (Z1, N_2, N_3, N_5, Q_c_6, Q_c_5, Q_c_4, Q_c_3, 
            Q_c_2, Q_c_1, Q_c_0) /* synthesis NGD_DRC_MASK=1 */ ;
    input [3:0]Z1;
    input N_2;
    input N_3;
    input N_5;
    output Q_c_6;
    output Q_c_5;
    output Q_c_4;
    output Q_c_3;
    output Q_c_2;
    output Q_c_1;
    output Q_c_0;
    
    wire N_2 /* synthesis is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(14[6:9])
    
    DP8KC decodaffich_0_0_0 (.DIA0(N_5), .DIA1(N_5), .DIA2(N_5), .DIA3(N_5), 
          .DIA4(N_5), .DIA5(N_5), .DIA6(N_5), .DIA7(N_5), .DIA8(N_5), 
          .ADA0(N_3), .ADA1(N_5), .ADA2(N_5), .ADA3(Z1[0]), .ADA4(Z1[1]), 
          .ADA5(Z1[2]), .ADA6(Z1[3]), .ADA7(N_5), .ADA8(N_5), .ADA9(N_5), 
          .ADA10(N_5), .ADA11(N_5), .ADA12(N_5), .CEA(N_3), .OCEA(N_3), 
          .CLKA(N_2), .WEA(N_5), .CSA0(N_5), .CSA1(N_5), .CSA2(N_5), 
          .RSTA(N_5), .DIB0(N_5), .DIB1(N_5), .DIB2(N_5), .DIB3(N_5), 
          .DIB4(N_5), .DIB5(N_5), .DIB6(N_5), .DIB7(N_5), .DIB8(N_5), 
          .ADB0(N_5), .ADB1(N_5), .ADB2(N_5), .ADB3(N_5), .ADB4(N_5), 
          .ADB5(N_5), .ADB6(N_5), .ADB7(N_5), .ADB8(N_5), .ADB9(N_5), 
          .ADB10(N_5), .ADB11(N_5), .ADB12(N_5), .CEB(N_3), .OCEB(N_3), 
          .CLKB(N_5), .WEB(N_5), .CSB0(N_5), .CSB1(N_5), .CSB2(N_5), 
          .RSTB(N_5), .DOA0(Q_c_0), .DOA1(Q_c_1), .DOA2(Q_c_2), .DOA3(Q_c_3), 
          .DOA4(Q_c_4), .DOA5(Q_c_5), .DOA6(Q_c_6)) /* synthesis MEM_LPC_FILE="decodaffich.lpc", MEM_INIT_FILE="memoire.mem", syn_instantiated=1, LSE_LINE_FILE_ID=4, LSE_LCOL=13, LSE_RCOL=28, LSE_LLINE=25, LSE_RLINE=26 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/impl1/schemafinal.v(25[13] 26[28])
    defparam decodaffich_0_0_0.DATA_WIDTH_A = 9;
    defparam decodaffich_0_0_0.DATA_WIDTH_B = 9;
    defparam decodaffich_0_0_0.REGMODE_A = "OUTREG";
    defparam decodaffich_0_0_0.REGMODE_B = "NOREG";
    defparam decodaffich_0_0_0.CSDECODE_A = "0b000";
    defparam decodaffich_0_0_0.CSDECODE_B = "0b111";
    defparam decodaffich_0_0_0.WRITEMODE_A = "NORMAL";
    defparam decodaffich_0_0_0.WRITEMODE_B = "NORMAL";
    defparam decodaffich_0_0_0.GSR = "ENABLED";
    defparam decodaffich_0_0_0.RESETMODE = "SYNC";
    defparam decodaffich_0_0_0.ASYNC_RESET_RELEASE = "SYNC";
    defparam decodaffich_0_0_0.INIT_DATA = "DYNAMIC";
    defparam decodaffich_0_0_0.INITVAL_00 = "0x00000000000000000000000000000000000000000E2790BC390F8770DE7F00E7D0DA6609E5B00C3F";
    defparam decodaffich_0_0_0.INITVAL_01 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_02 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_03 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_04 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_05 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_06 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_07 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_08 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_09 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_0A = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_0B = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_0C = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_0D = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_0E = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_0F = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_10 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_11 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_12 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_13 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_14 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_15 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_16 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_17 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_18 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_19 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_1A = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_1B = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_1C = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_1D = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_1E = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_1F = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    
endmodule
