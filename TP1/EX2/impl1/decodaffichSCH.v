/* Verilog model created from schematic decodaffichSCH.sch -- Sep 20, 2023 16:27 */

module decodaffichSCH( Q, SEDSTBY );
output [6:0] Q;
output SEDSTBY;
wire N_1;
wire N_2;
wire N_3;
wire N_4;



VLO I1 ( .Z(N_4) );
VLO I2 ( .Z(N_2) );
VHI I3 ( .Z(N_3) );
defparam I4.NOM_FREQ="2.08";
OSCH I4 ( .OSC(N_1), .SEDSTDBY(SEDSTBY), .STDBY(N_2) );
decodaffich I5 ( .OutClock(N_1), .OutClockEn(N_3), .Q(Q[6:0]), .Reset(N_4) );

endmodule // decodaffichSCH
