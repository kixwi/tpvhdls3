// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Tue Sep 19 22:43:23 2023
//
// Verilog Description of module ENTITYDECOD
//

module ENTITYDECOD (A, S);   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decod.vhd(6[8:19])
    input [1:0]A;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decod.vhd(8[3:4])
    output [3:0]S;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decod.vhd(9[3:4])
    
    
    wire GND_net, VCC_net, A_c_1, A_c_0, S_c_3, S_c_2, S_c_1, 
        S_c_0;
    
    VHI i13 (.Z(VCC_net));
    LUT4 i15_2_lut (.A(A_c_0), .B(A_c_1), .Z(S_c_1)) /* synthesis lut_function=(!((B)+!A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decod.vhd(15[2] 20[24])
    defparam i15_2_lut.init = 16'h2222;
    VLO i27 (.Z(GND_net));
    OB S_pad_3 (.I(S_c_3), .O(S[3]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decod.vhd(9[3:4])
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    GSR GSR_INST (.GSR(VCC_net));
    LUT4 i26_2_lut (.A(A_c_0), .B(A_c_1), .Z(S_c_0)) /* synthesis lut_function=(!(A+(B))) */ ;
    defparam i26_2_lut.init = 16'h1111;
    LUT4 i18_2_lut (.A(A_c_0), .B(A_c_1), .Z(S_c_3)) /* synthesis lut_function=(A (B)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decod.vhd(15[2] 20[24])
    defparam i18_2_lut.init = 16'h8888;
    LUT4 i17_2_lut (.A(A_c_0), .B(A_c_1), .Z(S_c_2)) /* synthesis lut_function=(!(A+!(B))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decod.vhd(15[2] 20[24])
    defparam i17_2_lut.init = 16'h4444;
    TSALL TSALL_INST (.TSALL(GND_net));
    OB S_pad_2 (.I(S_c_2), .O(S[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decod.vhd(9[3:4])
    OB S_pad_1 (.I(S_c_1), .O(S[1]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decod.vhd(9[3:4])
    OB S_pad_0 (.I(S_c_0), .O(S[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decod.vhd(9[3:4])
    IB A_pad_1 (.I(A[1]), .O(A_c_1));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decod.vhd(8[3:4])
    IB A_pad_0 (.I(A[0]), .O(A_c_0));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex2/decod.vhd(8[3:4])
    
endmodule
//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

