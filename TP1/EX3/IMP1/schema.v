/* Verilog model created from schematic schema.sch -- Sep 28, 2023 14:55 */

module schema( AFFICHEUR, BORROW, CARRY, CDOWN, CUP, D, LEDS, RAZ, SEGMENT );
output [3:0] AFFICHEUR;
output BORROW;
output CARRY;
 input CDOWN;
 input CUP;
 input [3:0] D;
output [5:0] LEDS;
 input RAZ;
output [6:0] SEGMENT;
  wire [3:0] Q;
  wire [1:0] SEL;
  wire [19:0] HORLOGE;
wire N_1;
wire N_2;
wire N_3;
wire N_4;
wire N_5;



VLO I3 ( .Z(N_4) );
VLO I4 ( .Z(SEL[1]) );
VLO I5 ( .Z(SEL[0]) );
VLO I6 ( .Z(N_2) );
VHI I12 ( .Z(LEDS[4]) );
VHI I13 ( .Z(LEDS[5]) );
VHI I14 ( .Z(LEDS[1]) );
VHI I15 ( .Z(LEDS[0]) );
VHI I16 ( .Z(LEDS[2]) );
VHI I17 ( .Z(LEDS[3]) );
VHI I7 ( .Z(N_3) );
VHI I8 ( .Z(N_1) );
ENTITYDECOD I9 ( .A(SEL[1:0]), .S(AFFICHEUR[3:0]) );
compteur20 I10 ( .clock(N_5), .Q(HORLOGE[19:0]), .reset(N_1) );
decodaffich I11 ( .Address(Q[3:0]), .OutClock(N_5), .OutClockEn(N_3),
               .Q(SEGMENT[6:0]), .Reset(N_2) );
defparam I1.NOM_FREQ="2.08";
OSCH I1 ( .OSC(N_5), .STDBY(N_4) );
ENTITYCOMPTDECOMPT I2 ( .BORROW(BORROW), .CARRY(CARRY), .Cdown(CDOWN),
                     .CLK(HORLOGE[19]), .Cup(CUP), .D(D[3:0]), .Q(Q[3:0]),
                     .RAZ(RAZ) );

endmodule // schema
