// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Thu Sep 28 15:19:32 2023
//
// Verilog Description of module schema
//

module schema (AFFICHEUR, BORROW, CARRY, CDOWN, CUP, D, LEDS, 
            RAZ, SEGMENT) /* synthesis syn_module_defined=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(3[8:14])
    output [3:0]AFFICHEUR;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(4[14:23])
    output BORROW;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(5[8:14])
    output CARRY;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(6[8:13])
    input CDOWN;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(7[8:13])
    input CUP;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(8[8:11])
    input [3:0]D;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(9[14:15])
    output [5:0]LEDS;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(10[14:18])
    input RAZ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(11[8:11])
    output [6:0]SEGMENT;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(12[14:21])
    
    wire [19:0]HORLOGE /* synthesis SET_AS_NETWORK=HORLOGE[19], is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(15[15:22])
    wire N_5 /* synthesis is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(20[6:9])
    
    wire CDOWN_c, CUP_c, D_c_3, D_c_2, D_c_1, D_c_0, RAZ_c, SEGMENT_c_6, 
        SEGMENT_c_5, SEGMENT_c_4, SEGMENT_c_3, SEGMENT_c_2, SEGMENT_c_1, 
        SEGMENT_c_0;
    wire [3:0]Q;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(13[14:15])
    
    wire AFFICHEUR_c, CARRY_N_58, BORROW_N_61, VCC_net;
    
    VLO I4 (.Z(AFFICHEUR_c));
    compteur20 I10 (.\HORLOGE[19] (HORLOGE[19]), .N_5(N_5), .AFFICHEUR_c(AFFICHEUR_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(37[12:63])
    OB AFFICHEUR_pad_1 (.I(AFFICHEUR_c), .O(AFFICHEUR[1]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(4[14:23])
    OSCH I1 (.STDBY(AFFICHEUR_c), .OSC(N_5)) /* synthesis syn_instantiated=1 */ ;
    defparam I1.NOM_FREQ = "2.08";
    OB AFFICHEUR_pad_2 (.I(AFFICHEUR_c), .O(AFFICHEUR[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(4[14:23])
    GSR GSR_INST (.GSR(VCC_net));
    OB AFFICHEUR_pad_3 (.I(AFFICHEUR_c), .O(AFFICHEUR[3]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(4[14:23])
    OB AFFICHEUR_pad_0 (.I(VCC_net), .O(AFFICHEUR[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(4[14:23])
    OB BORROW_pad (.I(BORROW_N_61), .O(BORROW));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(5[8:14])
    OB CARRY_pad (.I(CARRY_N_58), .O(CARRY));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(6[8:13])
    OB LEDS_pad_5 (.I(VCC_net), .O(LEDS[5]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(10[14:18])
    OB LEDS_pad_4 (.I(VCC_net), .O(LEDS[4]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(10[14:18])
    OB LEDS_pad_3 (.I(VCC_net), .O(LEDS[3]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(10[14:18])
    OB LEDS_pad_2 (.I(VCC_net), .O(LEDS[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(10[14:18])
    OB LEDS_pad_1 (.I(VCC_net), .O(LEDS[1]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(10[14:18])
    OB LEDS_pad_0 (.I(VCC_net), .O(LEDS[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(10[14:18])
    OB SEGMENT_pad_6 (.I(SEGMENT_c_6), .O(SEGMENT[6]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(12[14:21])
    OB SEGMENT_pad_5 (.I(SEGMENT_c_5), .O(SEGMENT[5]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(12[14:21])
    OB SEGMENT_pad_4 (.I(SEGMENT_c_4), .O(SEGMENT[4]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(12[14:21])
    OB SEGMENT_pad_3 (.I(SEGMENT_c_3), .O(SEGMENT[3]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(12[14:21])
    OB SEGMENT_pad_2 (.I(SEGMENT_c_2), .O(SEGMENT[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(12[14:21])
    OB SEGMENT_pad_1 (.I(SEGMENT_c_1), .O(SEGMENT[1]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(12[14:21])
    OB SEGMENT_pad_0 (.I(SEGMENT_c_0), .O(SEGMENT[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(12[14:21])
    IB CDOWN_pad (.I(CDOWN), .O(CDOWN_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(7[8:13])
    IB CUP_pad (.I(CUP), .O(CUP_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(8[8:11])
    IB D_pad_3 (.I(D[3]), .O(D_c_3));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(9[14:15])
    IB D_pad_2 (.I(D[2]), .O(D_c_2));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(9[14:15])
    IB D_pad_1 (.I(D[1]), .O(D_c_1));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(9[14:15])
    IB D_pad_0 (.I(D[0]), .O(D_c_0));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(9[14:15])
    IB RAZ_pad (.I(RAZ), .O(RAZ_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(11[8:11])
    ENTITYCOMPTDECOMPT I2 (.RAZ_c(RAZ_c), .Q({Q}), .CUP_c(CUP_c), .BORROW_N_61(BORROW_N_61), 
            .\HORLOGE[19] (HORLOGE[19]), .CDOWN_c(CDOWN_c), .CARRY_N_58(CARRY_N_58), 
            .D_c_2(D_c_2), .D_c_3(D_c_3), .D_c_0(D_c_0), .D_c_1(D_c_1));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(42[20] 44[33])
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    TSALL TSALL_INST (.TSALL(AFFICHEUR_c));
    decodaffich I11 (.Q({Q}), .N_5(N_5), .AFFICHEUR_c(AFFICHEUR_c), .SEGMENT_c_6(SEGMENT_c_6), 
            .SEGMENT_c_5(SEGMENT_c_5), .SEGMENT_c_4(SEGMENT_c_4), .SEGMENT_c_3(SEGMENT_c_3), 
            .SEGMENT_c_2(SEGMENT_c_2), .SEGMENT_c_1(SEGMENT_c_1), .SEGMENT_c_0(SEGMENT_c_0)) /* synthesis NGD_DRC_MASK=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(38[13] 39[47])
    VHI i276 (.Z(VCC_net));
    
endmodule
//
// Verilog Description of module compteur20
//

module compteur20 (\HORLOGE[19] , N_5, AFFICHEUR_c);
    output \HORLOGE[19] ;
    input N_5;
    input AFFICHEUR_c;
    
    wire \HORLOGE[19]  /* synthesis SET_AS_NETWORK=HORLOGE[19], is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(15[15:22])
    wire N_5 /* synthesis is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(20[6:9])
    wire [19:0]n85;
    wire [19:0]n149;
    
    wire n330, n331, n329, n338, n337, n336, n335, n334, n333, 
        n332;
    
    FD1S3AX Qint_43__i19 (.D(n85[19]), .CK(N_5), .Q(\HORLOGE[19] )) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i19.GSR = "ENABLED";
    FD1S3AX Qint_43__i0 (.D(n85[0]), .CK(N_5), .Q(n149[0])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i0.GSR = "ENABLED";
    CCU2D Qint_43_add_4_5 (.A0(n149[3]), .B0(AFFICHEUR_c), .C0(AFFICHEUR_c), 
          .D0(AFFICHEUR_c), .A1(n149[4]), .B1(AFFICHEUR_c), .C1(AFFICHEUR_c), 
          .D1(AFFICHEUR_c), .CIN(n330), .COUT(n331), .S0(n85[3]), .S1(n85[4]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43_add_4_5.INIT0 = 16'hfaaa;
    defparam Qint_43_add_4_5.INIT1 = 16'hfaaa;
    defparam Qint_43_add_4_5.INJECT1_0 = "NO";
    defparam Qint_43_add_4_5.INJECT1_1 = "NO";
    CCU2D Qint_43_add_4_1 (.A0(AFFICHEUR_c), .B0(AFFICHEUR_c), .C0(AFFICHEUR_c), 
          .D0(AFFICHEUR_c), .A1(n149[0]), .B1(AFFICHEUR_c), .C1(AFFICHEUR_c), 
          .D1(AFFICHEUR_c), .COUT(n329), .S1(n85[0]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43_add_4_1.INIT0 = 16'hF000;
    defparam Qint_43_add_4_1.INIT1 = 16'h0555;
    defparam Qint_43_add_4_1.INJECT1_0 = "NO";
    defparam Qint_43_add_4_1.INJECT1_1 = "NO";
    CCU2D Qint_43_add_4_3 (.A0(n149[1]), .B0(AFFICHEUR_c), .C0(AFFICHEUR_c), 
          .D0(AFFICHEUR_c), .A1(n149[2]), .B1(AFFICHEUR_c), .C1(AFFICHEUR_c), 
          .D1(AFFICHEUR_c), .CIN(n329), .COUT(n330), .S0(n85[1]), .S1(n85[2]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43_add_4_3.INIT0 = 16'hfaaa;
    defparam Qint_43_add_4_3.INIT1 = 16'hfaaa;
    defparam Qint_43_add_4_3.INJECT1_0 = "NO";
    defparam Qint_43_add_4_3.INJECT1_1 = "NO";
    CCU2D Qint_43_add_4_21 (.A0(\HORLOGE[19] ), .B0(AFFICHEUR_c), .C0(AFFICHEUR_c), 
          .D0(AFFICHEUR_c), .A1(AFFICHEUR_c), .B1(AFFICHEUR_c), .C1(AFFICHEUR_c), 
          .D1(AFFICHEUR_c), .CIN(n338), .S0(n85[19]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43_add_4_21.INIT0 = 16'hfaaa;
    defparam Qint_43_add_4_21.INIT1 = 16'h0000;
    defparam Qint_43_add_4_21.INJECT1_0 = "NO";
    defparam Qint_43_add_4_21.INJECT1_1 = "NO";
    CCU2D Qint_43_add_4_19 (.A0(n149[17]), .B0(AFFICHEUR_c), .C0(AFFICHEUR_c), 
          .D0(AFFICHEUR_c), .A1(n149[18]), .B1(AFFICHEUR_c), .C1(AFFICHEUR_c), 
          .D1(AFFICHEUR_c), .CIN(n337), .COUT(n338), .S0(n85[17]), .S1(n85[18]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43_add_4_19.INIT0 = 16'hfaaa;
    defparam Qint_43_add_4_19.INIT1 = 16'hfaaa;
    defparam Qint_43_add_4_19.INJECT1_0 = "NO";
    defparam Qint_43_add_4_19.INJECT1_1 = "NO";
    CCU2D Qint_43_add_4_17 (.A0(n149[15]), .B0(AFFICHEUR_c), .C0(AFFICHEUR_c), 
          .D0(AFFICHEUR_c), .A1(n149[16]), .B1(AFFICHEUR_c), .C1(AFFICHEUR_c), 
          .D1(AFFICHEUR_c), .CIN(n336), .COUT(n337), .S0(n85[15]), .S1(n85[16]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43_add_4_17.INIT0 = 16'hfaaa;
    defparam Qint_43_add_4_17.INIT1 = 16'hfaaa;
    defparam Qint_43_add_4_17.INJECT1_0 = "NO";
    defparam Qint_43_add_4_17.INJECT1_1 = "NO";
    CCU2D Qint_43_add_4_15 (.A0(n149[13]), .B0(AFFICHEUR_c), .C0(AFFICHEUR_c), 
          .D0(AFFICHEUR_c), .A1(n149[14]), .B1(AFFICHEUR_c), .C1(AFFICHEUR_c), 
          .D1(AFFICHEUR_c), .CIN(n335), .COUT(n336), .S0(n85[13]), .S1(n85[14]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43_add_4_15.INIT0 = 16'hfaaa;
    defparam Qint_43_add_4_15.INIT1 = 16'hfaaa;
    defparam Qint_43_add_4_15.INJECT1_0 = "NO";
    defparam Qint_43_add_4_15.INJECT1_1 = "NO";
    CCU2D Qint_43_add_4_13 (.A0(n149[11]), .B0(AFFICHEUR_c), .C0(AFFICHEUR_c), 
          .D0(AFFICHEUR_c), .A1(n149[12]), .B1(AFFICHEUR_c), .C1(AFFICHEUR_c), 
          .D1(AFFICHEUR_c), .CIN(n334), .COUT(n335), .S0(n85[11]), .S1(n85[12]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43_add_4_13.INIT0 = 16'hfaaa;
    defparam Qint_43_add_4_13.INIT1 = 16'hfaaa;
    defparam Qint_43_add_4_13.INJECT1_0 = "NO";
    defparam Qint_43_add_4_13.INJECT1_1 = "NO";
    FD1S3AX Qint_43__i18 (.D(n85[18]), .CK(N_5), .Q(n149[18])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i18.GSR = "ENABLED";
    FD1S3AX Qint_43__i17 (.D(n85[17]), .CK(N_5), .Q(n149[17])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i17.GSR = "ENABLED";
    FD1S3AX Qint_43__i16 (.D(n85[16]), .CK(N_5), .Q(n149[16])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i16.GSR = "ENABLED";
    FD1S3AX Qint_43__i15 (.D(n85[15]), .CK(N_5), .Q(n149[15])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i15.GSR = "ENABLED";
    FD1S3AX Qint_43__i14 (.D(n85[14]), .CK(N_5), .Q(n149[14])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i14.GSR = "ENABLED";
    FD1S3AX Qint_43__i13 (.D(n85[13]), .CK(N_5), .Q(n149[13])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i13.GSR = "ENABLED";
    FD1S3AX Qint_43__i12 (.D(n85[12]), .CK(N_5), .Q(n149[12])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i12.GSR = "ENABLED";
    FD1S3AX Qint_43__i11 (.D(n85[11]), .CK(N_5), .Q(n149[11])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i11.GSR = "ENABLED";
    FD1S3AX Qint_43__i10 (.D(n85[10]), .CK(N_5), .Q(n149[10])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i10.GSR = "ENABLED";
    FD1S3AX Qint_43__i9 (.D(n85[9]), .CK(N_5), .Q(n149[9])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i9.GSR = "ENABLED";
    FD1S3AX Qint_43__i8 (.D(n85[8]), .CK(N_5), .Q(n149[8])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i8.GSR = "ENABLED";
    FD1S3AX Qint_43__i7 (.D(n85[7]), .CK(N_5), .Q(n149[7])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i7.GSR = "ENABLED";
    FD1S3AX Qint_43__i6 (.D(n85[6]), .CK(N_5), .Q(n149[6])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i6.GSR = "ENABLED";
    FD1S3AX Qint_43__i5 (.D(n85[5]), .CK(N_5), .Q(n149[5])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i5.GSR = "ENABLED";
    FD1S3AX Qint_43__i4 (.D(n85[4]), .CK(N_5), .Q(n149[4])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i4.GSR = "ENABLED";
    FD1S3AX Qint_43__i3 (.D(n85[3]), .CK(N_5), .Q(n149[3])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i3.GSR = "ENABLED";
    FD1S3AX Qint_43__i2 (.D(n85[2]), .CK(N_5), .Q(n149[2])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i2.GSR = "ENABLED";
    FD1S3AX Qint_43__i1 (.D(n85[1]), .CK(N_5), .Q(n149[1])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43__i1.GSR = "ENABLED";
    CCU2D Qint_43_add_4_11 (.A0(n149[9]), .B0(AFFICHEUR_c), .C0(AFFICHEUR_c), 
          .D0(AFFICHEUR_c), .A1(n149[10]), .B1(AFFICHEUR_c), .C1(AFFICHEUR_c), 
          .D1(AFFICHEUR_c), .CIN(n333), .COUT(n334), .S0(n85[9]), .S1(n85[10]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43_add_4_11.INIT0 = 16'hfaaa;
    defparam Qint_43_add_4_11.INIT1 = 16'hfaaa;
    defparam Qint_43_add_4_11.INJECT1_0 = "NO";
    defparam Qint_43_add_4_11.INJECT1_1 = "NO";
    CCU2D Qint_43_add_4_9 (.A0(n149[7]), .B0(AFFICHEUR_c), .C0(AFFICHEUR_c), 
          .D0(AFFICHEUR_c), .A1(n149[8]), .B1(AFFICHEUR_c), .C1(AFFICHEUR_c), 
          .D1(AFFICHEUR_c), .CIN(n332), .COUT(n333), .S0(n85[7]), .S1(n85[8]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43_add_4_9.INIT0 = 16'hfaaa;
    defparam Qint_43_add_4_9.INIT1 = 16'hfaaa;
    defparam Qint_43_add_4_9.INJECT1_0 = "NO";
    defparam Qint_43_add_4_9.INJECT1_1 = "NO";
    CCU2D Qint_43_add_4_7 (.A0(n149[5]), .B0(AFFICHEUR_c), .C0(AFFICHEUR_c), 
          .D0(AFFICHEUR_c), .A1(n149[6]), .B1(AFFICHEUR_c), .C1(AFFICHEUR_c), 
          .D1(AFFICHEUR_c), .CIN(n331), .COUT(n332), .S0(n85[5]), .S1(n85[6]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_43_add_4_7.INIT0 = 16'hfaaa;
    defparam Qint_43_add_4_7.INIT1 = 16'hfaaa;
    defparam Qint_43_add_4_7.INJECT1_0 = "NO";
    defparam Qint_43_add_4_7.INJECT1_1 = "NO";
    
endmodule
//
// Verilog Description of module ENTITYCOMPTDECOMPT
//

module ENTITYCOMPTDECOMPT (RAZ_c, Q, CUP_c, BORROW_N_61, \HORLOGE[19] , 
            CDOWN_c, CARRY_N_58, D_c_2, D_c_3, D_c_0, D_c_1);
    input RAZ_c;
    output [3:0]Q;
    input CUP_c;
    output BORROW_N_61;
    input \HORLOGE[19] ;
    input CDOWN_c;
    output CARRY_N_58;
    input D_c_2;
    input D_c_3;
    input D_c_0;
    input D_c_1;
    
    wire \HORLOGE[19]  /* synthesis SET_AS_NETWORK=HORLOGE[19], is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(15[15:22])
    wire [3:0]Qint;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(18[8:12])
    
    wire n271, n429, n428, n433, n430, n214, HORLOGE_19_enable_4, 
        n226;
    wire [3:0]n21;
    
    wire n360, n358, n247, n6, n4, n8, n285, n431, n287, n432;
    
    LUT4 i129_2_lut (.A(Qint[3]), .B(RAZ_c), .Z(Q[3])) /* synthesis lut_function=(A (B)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(34[7] 35[8])
    defparam i129_2_lut.init = 16'h8888;
    LUT4 i130_2_lut (.A(Qint[2]), .B(RAZ_c), .Z(Q[2])) /* synthesis lut_function=(A (B)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(34[7] 35[8])
    defparam i130_2_lut.init = 16'h8888;
    LUT4 i131_2_lut (.A(Qint[1]), .B(RAZ_c), .Z(Q[1])) /* synthesis lut_function=(A (B)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(34[7] 35[8])
    defparam i131_2_lut.init = 16'h8888;
    LUT4 i138_2_lut_3_lut_4_lut (.A(Qint[1]), .B(Qint[0]), .C(Qint[3]), 
         .D(Qint[2]), .Z(n271)) /* synthesis lut_function=(A (C)+!A (B (C)+!B (C (D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(23[4] 31[11])
    defparam i138_2_lut_3_lut_4_lut.init = 16'hf0e0;
    PFUMX i268 (.BLUT(n429), .ALUT(n428), .C0(n433), .Z(n430));
    LUT4 i265_2_lut (.A(CUP_c), .B(n214), .Z(BORROW_N_61)) /* synthesis lut_function=(A+!(B)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(37[12:69])
    defparam i265_2_lut.init = 16'hbbbb;
    FD1P3JX Qint_44__i0 (.D(n21[0]), .SP(HORLOGE_19_enable_4), .PD(n226), 
            .CK(\HORLOGE[19] ), .Q(Qint[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(23[4] 31[11])
    defparam Qint_44__i0.GSR = "ENABLED";
    FD1P3IX Qint_44__i2 (.D(n360), .SP(HORLOGE_19_enable_4), .CD(n226), 
            .CK(\HORLOGE[19] ), .Q(Qint[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(23[4] 31[11])
    defparam Qint_44__i2.GSR = "ENABLED";
    FD1P3JX Qint_44__i3 (.D(n430), .SP(HORLOGE_19_enable_4), .PD(n226), 
            .CK(\HORLOGE[19] ), .Q(Qint[3]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(23[4] 31[11])
    defparam Qint_44__i3.GSR = "ENABLED";
    FD1P3IX Qint_44__i1 (.D(n358), .SP(HORLOGE_19_enable_4), .CD(n226), 
            .CK(\HORLOGE[19] ), .Q(Qint[1]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(23[4] 31[11])
    defparam Qint_44__i1.GSR = "ENABLED";
    LUT4 i2_4_lut_rep_10 (.A(CDOWN_c), .B(CUP_c), .Z(n433)) /* synthesis lut_function=(A (B)) */ ;
    defparam i2_4_lut_rep_10.init = 16'h8888;
    LUT4 i263_4_lut (.A(Qint[3]), .B(CDOWN_c), .C(n247), .D(n6), .Z(CARRY_N_58)) /* synthesis lut_function=((B+(C+!(D)))+!A) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(36[11:68])
    defparam i263_4_lut.init = 16'hfdff;
    LUT4 i1_2_lut (.A(Qint[0]), .B(CUP_c), .Z(n6)) /* synthesis lut_function=(A (B)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(27[10:45])
    defparam i1_2_lut.init = 16'h8888;
    LUT4 i3_4_lut (.A(Qint[0]), .B(CDOWN_c), .C(Qint[3]), .D(n247), 
         .Z(n214)) /* synthesis lut_function=(!(A+((C+(D))+!B))) */ ;
    defparam i3_4_lut.init = 16'h0004;
    LUT4 i115_2_lut (.A(Qint[2]), .B(Qint[1]), .Z(n247)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i115_2_lut.init = 16'heeee;
    LUT4 Qint_3__bdd_3_lut (.A(D_c_2), .B(n4), .C(D_c_3), .Z(n428)) /* synthesis lut_function=(!(A (B (C)+!B !(C))+!A !(C))) */ ;
    defparam Qint_3__bdd_3_lut.init = 16'h7878;
    LUT4 i114_2_lut (.A(Qint[0]), .B(RAZ_c), .Z(Q[0])) /* synthesis lut_function=(A (B)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(34[7] 35[8])
    defparam i114_2_lut.init = 16'h8888;
    LUT4 i199_3_lut_4_lut (.A(CDOWN_c), .B(CUP_c), .C(Qint[0]), .D(D_c_0), 
         .Z(n21[0])) /* synthesis lut_function=(A (B (D)+!B !(C))+!A !(C)) */ ;
    defparam i199_3_lut_4_lut.init = 16'h8f07;
    LUT4 i152_3_lut_4_lut (.A(CDOWN_c), .B(CUP_c), .C(D_c_2), .D(n8), 
         .Z(n285)) /* synthesis lut_function=(A (B (C)+!B (D))+!A (D)) */ ;
    defparam i152_3_lut_4_lut.init = 16'hf780;
    LUT4 i13_4_lut (.A(n431), .B(CUP_c), .C(CDOWN_c), .D(n271), .Z(HORLOGE_19_enable_4)) /* synthesis lut_function=(A ((C+!(D))+!B)+!A (B (C+!(D))+!B !(C))) */ ;
    defparam i13_4_lut.init = 16'he3ef;
    LUT4 n8_bdd_4_lut (.A(n8), .B(Qint[0]), .C(n433), .D(Qint[1]), .Z(n4)) /* synthesis lut_function=(!(A (B (C)+!B (C+!(D)))+!A ((C+!(D))+!B))) */ ;
    defparam n8_bdd_4_lut.init = 16'h0e08;
    LUT4 i154_3_lut_4_lut (.A(CDOWN_c), .B(CUP_c), .C(D_c_1), .D(n8), 
         .Z(n287)) /* synthesis lut_function=(A (B (C)+!B (D))+!A (D)) */ ;
    defparam i154_3_lut_4_lut.init = 16'hf780;
    LUT4 i260_2_lut (.A(CUP_c), .B(CDOWN_c), .Z(n226)) /* synthesis lut_function=(!(A+(B))) */ ;
    defparam i260_2_lut.init = 16'h1111;
    LUT4 i2_3_lut_4_lut (.A(Qint[3]), .B(n432), .C(CUP_c), .D(CDOWN_c), 
         .Z(n8)) /* synthesis lut_function=(!(A (C+!(D))+!A ((C+!(D))+!B))) */ ;
    defparam i2_3_lut_4_lut.init = 16'h0e00;
    LUT4 i1_2_lut_rep_8_3_lut_4_lut (.A(Qint[1]), .B(Qint[0]), .C(Qint[3]), 
         .D(Qint[2]), .Z(n431)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(23[4] 31[11])
    defparam i1_2_lut_rep_8_3_lut_4_lut.init = 16'hfffe;
    LUT4 Qint_3__bdd_4_lut (.A(Qint[3]), .B(n4), .C(n8), .D(Qint[2]), 
         .Z(n429)) /* synthesis lut_function=(A (B (C+!(D))+!B ((D)+!C))+!A !(B (C+!(D))+!B ((D)+!C))) */ ;
    defparam Qint_3__bdd_4_lut.init = 16'ha69a;
    LUT4 i2_2_lut_rep_9_3_lut (.A(Qint[1]), .B(Qint[0]), .C(Qint[2]), 
         .Z(n432)) /* synthesis lut_function=(A+(B+(C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(23[4] 31[11])
    defparam i2_2_lut_rep_9_3_lut.init = 16'hfefe;
    LUT4 i2_4_lut_4_lut (.A(Qint[1]), .B(n433), .C(Qint[0]), .D(n287), 
         .Z(n358)) /* synthesis lut_function=(A (B (D)+!B (C (D)+!C !(D)))+!A (B (D)+!B !(C (D)+!C !(D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(23[4] 31[11])
    defparam i2_4_lut_4_lut.init = 16'hed12;
    LUT4 i2_3_lut_4_lut_adj_7 (.A(Qint[2]), .B(n433), .C(n4), .D(n285), 
         .Z(n360)) /* synthesis lut_function=(!(A (B (C (D)+!C !(D))+!B !(C (D)+!C !(D)))+!A (C (D)+!C !(D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(23[4] 31[11])
    defparam i2_3_lut_4_lut_adj_7.init = 16'h2dd2;
    
endmodule
//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module decodaffich
//

module decodaffich (Q, N_5, AFFICHEUR_c, SEGMENT_c_6, SEGMENT_c_5, 
            SEGMENT_c_4, SEGMENT_c_3, SEGMENT_c_2, SEGMENT_c_1, SEGMENT_c_0) /* synthesis NGD_DRC_MASK=1 */ ;
    input [3:0]Q;
    input N_5;
    input AFFICHEUR_c;
    output SEGMENT_c_6;
    output SEGMENT_c_5;
    output SEGMENT_c_4;
    output SEGMENT_c_3;
    output SEGMENT_c_2;
    output SEGMENT_c_1;
    output SEGMENT_c_0;
    
    wire N_5 /* synthesis is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(20[6:9])
    
    wire VCC_net;
    
    DP8KC decodaffich_0_0_0 (.DIA0(AFFICHEUR_c), .DIA1(AFFICHEUR_c), .DIA2(AFFICHEUR_c), 
          .DIA3(AFFICHEUR_c), .DIA4(AFFICHEUR_c), .DIA5(AFFICHEUR_c), 
          .DIA6(AFFICHEUR_c), .DIA7(AFFICHEUR_c), .DIA8(AFFICHEUR_c), 
          .ADA0(VCC_net), .ADA1(AFFICHEUR_c), .ADA2(AFFICHEUR_c), .ADA3(Q[0]), 
          .ADA4(Q[1]), .ADA5(Q[2]), .ADA6(Q[3]), .ADA7(AFFICHEUR_c), 
          .ADA8(AFFICHEUR_c), .ADA9(AFFICHEUR_c), .ADA10(AFFICHEUR_c), 
          .ADA11(AFFICHEUR_c), .ADA12(AFFICHEUR_c), .CEA(VCC_net), .OCEA(VCC_net), 
          .CLKA(N_5), .WEA(AFFICHEUR_c), .CSA0(AFFICHEUR_c), .CSA1(AFFICHEUR_c), 
          .CSA2(AFFICHEUR_c), .RSTA(AFFICHEUR_c), .DIB0(AFFICHEUR_c), 
          .DIB1(AFFICHEUR_c), .DIB2(AFFICHEUR_c), .DIB3(AFFICHEUR_c), 
          .DIB4(AFFICHEUR_c), .DIB5(AFFICHEUR_c), .DIB6(AFFICHEUR_c), 
          .DIB7(AFFICHEUR_c), .DIB8(AFFICHEUR_c), .ADB0(AFFICHEUR_c), 
          .ADB1(AFFICHEUR_c), .ADB2(AFFICHEUR_c), .ADB3(AFFICHEUR_c), 
          .ADB4(AFFICHEUR_c), .ADB5(AFFICHEUR_c), .ADB6(AFFICHEUR_c), 
          .ADB7(AFFICHEUR_c), .ADB8(AFFICHEUR_c), .ADB9(AFFICHEUR_c), 
          .ADB10(AFFICHEUR_c), .ADB11(AFFICHEUR_c), .ADB12(AFFICHEUR_c), 
          .CEB(VCC_net), .OCEB(VCC_net), .CLKB(AFFICHEUR_c), .WEB(AFFICHEUR_c), 
          .CSB0(AFFICHEUR_c), .CSB1(AFFICHEUR_c), .CSB2(AFFICHEUR_c), 
          .RSTB(AFFICHEUR_c), .DOA0(SEGMENT_c_0), .DOA1(SEGMENT_c_1), 
          .DOA2(SEGMENT_c_2), .DOA3(SEGMENT_c_3), .DOA4(SEGMENT_c_4), 
          .DOA5(SEGMENT_c_5), .DOA6(SEGMENT_c_6)) /* synthesis MEM_LPC_FILE="decodaffich.lpc", MEM_INIT_FILE="memoire.mem", syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=13, LSE_RCOL=47, LSE_LLINE=38, LSE_RLINE=39 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/imp1/schema.v(38[13] 39[47])
    defparam decodaffich_0_0_0.DATA_WIDTH_A = 9;
    defparam decodaffich_0_0_0.DATA_WIDTH_B = 9;
    defparam decodaffich_0_0_0.REGMODE_A = "OUTREG";
    defparam decodaffich_0_0_0.REGMODE_B = "NOREG";
    defparam decodaffich_0_0_0.CSDECODE_A = "0b000";
    defparam decodaffich_0_0_0.CSDECODE_B = "0b111";
    defparam decodaffich_0_0_0.WRITEMODE_A = "NORMAL";
    defparam decodaffich_0_0_0.WRITEMODE_B = "NORMAL";
    defparam decodaffich_0_0_0.GSR = "ENABLED";
    defparam decodaffich_0_0_0.RESETMODE = "SYNC";
    defparam decodaffich_0_0_0.ASYNC_RESET_RELEASE = "SYNC";
    defparam decodaffich_0_0_0.INIT_DATA = "DYNAMIC";
    defparam decodaffich_0_0_0.INITVAL_00 = "0x00000000000000000000000000000000000000000E2790BC390F8770DE7F00E7D0DA6609E5B00C3F";
    defparam decodaffich_0_0_0.INITVAL_01 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_02 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_03 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_04 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_05 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_06 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_07 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_08 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_09 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_0A = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_0B = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_0C = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_0D = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_0E = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_0F = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_10 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_11 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_12 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_13 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_14 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_15 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_16 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_17 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_18 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_19 = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_1A = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_1B = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_1C = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_1D = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_1E = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    defparam decodaffich_0_0_0.INITVAL_1F = "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000";
    VHI i1 (.Z(VCC_net));
    
endmodule
