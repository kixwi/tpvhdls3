// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Wed Sep 20 18:25:25 2023
//
// Verilog Description of module ENTITYCOMPTDECOMPT
//

module ENTITYCOMPTDECOMPT (CLK, RAZ, Cup, Cdown, D, CARRY, BORROW, 
            Q);   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(6[8:26])
    input CLK;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(8[2:5])
    input RAZ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(9[2:5])
    input Cup;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(10[2:5])
    input Cdown;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(10[7:12])
    input [3:0]D;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(11[2:3])
    output CARRY;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(12[2:7])
    output BORROW;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(12[9:15])
    output [3:0]Q;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(13[2:3])
    
    wire CLK_c /* synthesis SET_AS_NETWORK=CLK_c, is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(8[2:5])
    
    wire GND_net, VCC_net, RAZ_c, Cup_c, Cdown_c, D_c_3, D_c_2, 
        D_c_1, D_c_0, CARRY_c, BORROW_c, Q_c_3, Q_c_2, Q_c_1, 
        Q_c_0;
    wire [3:0]Qint;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(18[8:12])
    
    wire CARRY_N_25, CLK_c_enable_5, BORROW_N_31, n427, n347, CARRY_N_22, 
        CARRY_N_20, n4, n425, n349, n424, CLK_c_enable_4, n423, 
        n263, n288, n350, n25, n286, n428;
    
    VHI i2 (.Z(VCC_net));
    LUT4 i195_2_lut (.A(Qint[3]), .B(RAZ_c), .Z(Q_c_3)) /* synthesis lut_function=(A (B)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(39[7] 40[8])
    defparam i195_2_lut.init = 16'h8888;
    LUT4 i24_2_lut_rep_8 (.A(Cup_c), .B(Cdown_c), .Z(n427)) /* synthesis lut_function=(A (B)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(34[10:31])
    defparam i24_2_lut_rep_8.init = 16'h8888;
    LUT4 i2_4_lut_4_lut (.A(Qint[1]), .B(n427), .C(Qint[0]), .D(n288), 
         .Z(n350)) /* synthesis lut_function=(A (B (D)+!B (C (D)+!C !(D)))+!A (B (D)+!B !(C (D)+!C !(D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(23[4] 36[11])
    defparam i2_4_lut_4_lut.init = 16'hed12;
    PFUMX i348 (.BLUT(n424), .ALUT(n423), .C0(n427), .Z(n425));
    LUT4 i198_2_lut (.A(Qint[2]), .B(RAZ_c), .Z(Q_c_2)) /* synthesis lut_function=(A (B)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(39[7] 40[8])
    defparam i198_2_lut.init = 16'h8888;
    LUT4 i347_4_lut (.A(Qint[0]), .B(Qint[3]), .C(Cup_c), .D(n263), 
         .Z(CARRY_N_20)) /* synthesis lut_function=(A+(((D)+!C)+!B)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(30[5] 32[12])
    defparam i347_4_lut.init = 16'hffbf;
    LUT4 i199_2_lut (.A(Qint[1]), .B(RAZ_c), .Z(Q_c_1)) /* synthesis lut_function=(A (B)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(39[7] 40[8])
    defparam i199_2_lut.init = 16'h8888;
    OB CARRY_pad (.I(CARRY_c), .O(CARRY));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(12[2:7])
    OB BORROW_pad (.I(BORROW_c), .O(BORROW));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(12[9:15])
    FD1P3AX BORROW_40 (.D(BORROW_N_31), .SP(CARRY_N_25), .CK(CLK_c), .Q(BORROW_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(22[3] 37[10])
    defparam BORROW_40.GSR = "ENABLED";
    LUT4 i227_3_lut_4_lut (.A(Cup_c), .B(Cdown_c), .C(D_c_1), .D(CARRY_N_25), 
         .Z(n288)) /* synthesis lut_function=(A (B (C)+!B (D))+!A (D)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(34[10:31])
    defparam i227_3_lut_4_lut.init = 16'hf780;
    FD1P3IX Qint_93__i2 (.D(n349), .SP(CLK_c_enable_4), .CD(n428), .CK(CLK_c), 
            .Q(Qint[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(23[4] 36[11])
    defparam Qint_93__i2.GSR = "ENABLED";
    FD1P3JX Qint_93__i0 (.D(n25), .SP(CLK_c_enable_4), .PD(n428), .CK(CLK_c), 
            .Q(Qint[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(23[4] 36[11])
    defparam Qint_93__i0.GSR = "ENABLED";
    FD1P3IX Qint_93__i1 (.D(n350), .SP(CLK_c_enable_4), .CD(n428), .CK(CLK_c), 
            .Q(Qint[1]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(23[4] 36[11])
    defparam Qint_93__i1.GSR = "ENABLED";
    FD1P3JX Qint_93__i3 (.D(n425), .SP(CLK_c_enable_4), .PD(n428), .CK(CLK_c), 
            .Q(Qint[3]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(23[4] 36[11])
    defparam Qint_93__i3.GSR = "ENABLED";
    FD1P3AX CARRY_41 (.D(CARRY_N_20), .SP(CLK_c_enable_5), .CK(CLK_c), 
            .Q(CARRY_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(22[3] 37[10])
    defparam CARRY_41.GSR = "ENABLED";
    OB Q_pad_3 (.I(Q_c_3), .O(Q[3]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(13[2:3])
    OB Q_pad_2 (.I(Q_c_2), .O(Q[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(13[2:3])
    OB Q_pad_1 (.I(Q_c_1), .O(Q[1]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(13[2:3])
    OB Q_pad_0 (.I(Q_c_0), .O(Q[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(13[2:3])
    IB CLK_pad (.I(CLK), .O(CLK_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(8[2:5])
    IB RAZ_pad (.I(RAZ), .O(RAZ_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(9[2:5])
    IB Cup_pad (.I(Cup), .O(Cup_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(10[2:5])
    IB Cdown_pad (.I(Cdown), .O(Cdown_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(10[7:12])
    IB D_pad_3 (.I(D[3]), .O(D_c_3));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(11[2:3])
    IB D_pad_2 (.I(D[2]), .O(D_c_2));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(11[2:3])
    IB D_pad_1 (.I(D[1]), .O(D_c_1));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(11[2:3])
    IB D_pad_0 (.I(D[0]), .O(D_c_0));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(11[2:3])
    LUT4 i200_2_lut (.A(Qint[0]), .B(RAZ_c), .Z(Q_c_0)) /* synthesis lut_function=(A (B)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(39[7] 40[8])
    defparam i200_2_lut.init = 16'h8888;
    GSR GSR_INST (.GSR(VCC_net));
    LUT4 i2_4_lut (.A(n347), .B(Cdown_c), .C(Qint[3]), .D(Cup_c), .Z(CARRY_N_22)) /* synthesis lut_function=(!(A (B+(C+!(D)))+!A (B+!(D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(29[10:45])
    defparam i2_4_lut.init = 16'h1300;
    LUT4 i341_2_lut_rep_9 (.A(Cup_c), .B(Cdown_c), .Z(n428)) /* synthesis lut_function=(!(A+(B))) */ ;
    defparam i341_2_lut_rep_9.init = 16'h1111;
    LUT4 i1_2_lut_3_lut_4_lut (.A(Cup_c), .B(Cdown_c), .C(CARRY_N_22), 
         .D(CARRY_N_25), .Z(CLK_c_enable_5)) /* synthesis lut_function=(!(A ((D)+!C)+!A (((D)+!C)+!B))) */ ;
    defparam i1_2_lut_3_lut_4_lut.init = 16'h00e0;
    LUT4 i2_3_lut_4_lut_4_lut (.A(Cup_c), .B(Cdown_c), .C(CARRY_N_25), 
         .D(CARRY_N_22), .Z(CLK_c_enable_4)) /* synthesis lut_function=(A (B+(C+(D)))+!A ((C+(D))+!B)) */ ;
    defparam i2_3_lut_4_lut_4_lut.init = 16'hfff9;
    LUT4 n157_bdd_4_lut (.A(n427), .B(CARRY_N_25), .C(Qint[0]), .D(Qint[1]), 
         .Z(n4)) /* synthesis lut_function=(!(A+!(B (C+(D))+!B (C (D))))) */ ;
    defparam n157_bdd_4_lut.init = 16'h5440;
    LUT4 Qint_3__bdd_4_lut (.A(Qint[3]), .B(n4), .C(CARRY_N_25), .D(Qint[2]), 
         .Z(n424)) /* synthesis lut_function=(A (B (C+!(D))+!B ((D)+!C))+!A !(B (C+!(D))+!B ((D)+!C))) */ ;
    defparam Qint_3__bdd_4_lut.init = 16'ha69a;
    LUT4 i2_4_lut_adj_1 (.A(Qint[3]), .B(Cdown_c), .C(n347), .D(Cup_c), 
         .Z(CARRY_N_25)) /* synthesis lut_function=(!(A ((D)+!B)+!A (((D)+!C)+!B))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(24[10:45])
    defparam i2_4_lut_adj_1.init = 16'h00c8;
    LUT4 i2_2_lut_3_lut (.A(Qint[1]), .B(Qint[0]), .C(Qint[2]), .Z(n347)) /* synthesis lut_function=(A+(B+(C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(23[4] 36[11])
    defparam i2_2_lut_3_lut.init = 16'hfefe;
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    LUT4 Qint_3__bdd_3_lut (.A(D_c_2), .B(n4), .C(D_c_3), .Z(n423)) /* synthesis lut_function=(!(A (B (C)+!B !(C))+!A !(C))) */ ;
    defparam Qint_3__bdd_3_lut.init = 16'h7878;
    LUT4 i272_3_lut_4_lut (.A(Cup_c), .B(Cdown_c), .C(Qint[0]), .D(D_c_0), 
         .Z(n25)) /* synthesis lut_function=(A (B (D)+!B !(C))+!A !(C)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(34[10:31])
    defparam i272_3_lut_4_lut.init = 16'h8f07;
    VLO i354 (.Z(GND_net));
    TSALL TSALL_INST (.TSALL(GND_net));
    LUT4 i344_4_lut (.A(Qint[0]), .B(Cdown_c), .C(n263), .D(Qint[3]), 
         .Z(BORROW_N_31)) /* synthesis lut_function=(((C+(D))+!B)+!A) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(25[5] 27[12])
    defparam i344_4_lut.init = 16'hfff7;
    LUT4 i2_3_lut_4_lut (.A(Qint[2]), .B(n427), .C(n4), .D(n286), .Z(n349)) /* synthesis lut_function=(!(A (B (C (D)+!C !(D))+!B !(C (D)+!C !(D)))+!A (C (D)+!C !(D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(23[4] 36[11])
    defparam i2_3_lut_4_lut.init = 16'h2dd2;
    LUT4 i225_3_lut_4_lut (.A(Cup_c), .B(Cdown_c), .C(D_c_2), .D(CARRY_N_25), 
         .Z(n286)) /* synthesis lut_function=(A (B (C)+!B (D))+!A (D)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex3/comptdecompt.vhd(34[10:31])
    defparam i225_3_lut_4_lut.init = 16'hf780;
    LUT4 i202_2_lut (.A(Qint[2]), .B(Qint[1]), .Z(n263)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i202_2_lut.init = 16'heeee;
    
endmodule
//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

