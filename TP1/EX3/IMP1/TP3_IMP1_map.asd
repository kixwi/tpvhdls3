[ActiveSupport MAP]
Device = LCMXO2-7000HE;
Package = TQFP144;
Performance = 4;
LUTS_avail = 6864;
LUTS_used = 48;
FF_avail = 6979;
FF_used = 24;
INPUT_LVCMOS25 = 7;
OUTPUT_LVCMOS25 = 19;
IO_avail = 115;
IO_used = 26;
EBR_avail = 26;
EBR_used = 1;
; Begin EBR Section
Instance_Name = I11/decodaffich_0_0_0;
Type = DP8KC;
Width_A = 7;
Depth_A = 16;
REGMODE_A = OUTREG;
REGMODE_B = NOREG;
RESETMODE = SYNC;
ASYNC_RESET_RELEASE = SYNC;
WRITEMODE_A = NORMAL;
WRITEMODE_B = NORMAL;
GSR = DISABLED;
MEM_INIT_FILE = memoire.mem;
MEM_LPC_FILE = decodaffich.lpc;
; End EBR Section
