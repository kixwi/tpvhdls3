/* Verilog model created from schematic testschema.sch -- Sep 20, 2023 18:52 */

module testschema( osc, sed );
output osc;
output sed;
wire N_1;



VLO I1 ( .Z(N_1) );
defparam I2.NOM_FREQ="2.08";
OSCH I2 ( .OSC(osc), .SEDSTDBY(sed), .STDBY(N_1) );

endmodule // testschema
