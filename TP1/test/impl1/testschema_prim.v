// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Wed Sep 20 18:53:09 2023
//
// Verilog Description of module testschema
//

module testschema (osc, sed) /* synthesis syn_module_defined=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/test/impl1/testschema.v(3[8:18])
    output osc;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/test/impl1/testschema.v(4[8:11])
    output sed;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/test/impl1/testschema.v(5[8:11])
    
    
    wire osc_c, sed_c, GND_net, VCC_net;
    
    OSCH I2 (.STDBY(GND_net), .SEDSTDBY(sed_c), .OSC(osc_c)) /* synthesis syn_instantiated=1 */ ;
    defparam I2.NOM_FREQ = "2.08";
    OB sed_pad (.I(sed_c), .O(sed));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/test/impl1/testschema.v(5[8:11])
    VHI i12 (.Z(VCC_net));
    OB osc_pad (.I(osc_c), .O(osc));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/test/impl1/testschema.v(4[8:11])
    GSR GSR_INST (.GSR(VCC_net));
    TSALL TSALL_INST (.TSALL(GND_net));
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    VLO i4 (.Z(GND_net));
    
endmodule
//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

