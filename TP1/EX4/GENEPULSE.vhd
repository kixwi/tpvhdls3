library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY ENTITYGENEPULSE IS
	PORT(CLK, ENABLE : IN STD_LOGIC;
		Q1, Q2 : OUT STD_LOGIC
		--CCCP : OUT STD_LOGIC_VECTOR(13 DOWNTO 0)
		);
END ENTITYGENEPULSE;

ARCHITECTURE DESCRIPT OF ENTITYGENEPULSE IS
SIGNAL CPT : INTEGER RANGE 0 TO 100000;
BEGIN
	PROCESS(ENABLE, CLK)
	BEGIN
		IF(ENABLE='0') THEN CPT <=0;
		ELSIF(CLK'EVENT AND CLK='1') THEN
			IF(CPT=100000) THEN CPT <=0;
			ELSE CPT <= CPT+1;
			END IF;
		END IF;
	END PROCESS;
Q1 <= '1' WHEN (CPT>15000 AND CPT<45000) ELSE '0';
Q2 <= '1' WHEN (CPT>60000) ELSE '0';--CCCP <= CPT;
END DESCRIPT;

