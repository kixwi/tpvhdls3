// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Thu Sep 28 16:26:47 2023
//
// Verilog Description of module ENTITYGENEPULSE
//

module ENTITYGENEPULSE (CLK, ENABLE, Q1, Q2);   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(6[8:23])
    input CLK;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(7[7:10])
    input ENABLE;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(7[12:18])
    output Q1;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(8[3:5])
    output Q2;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(8[7:9])
    
    wire CLK_c /* synthesis SET_AS_NETWORK=CLK_c, is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(7[7:10])
    
    wire GND_net, VCC_net, ENABLE_c, Q1_c, Q2_c;
    wire [16:0]CPT;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(12[8:11])
    
    wire n403, n164, n360, n357, n356, n6, n6_adj_1, n365, n354, 
        n22, n371, n361, n15, n359, n14, n20, n355, n4, n17, 
        n358, n461, n460, n12, n15_adj_2, n74, n75, n76, n77, 
        n78, n79, n80, n81, n82, n83, n84, n85, n86, n87, 
        n88, n89, n90, n36, n366, n27, n417, n459;
    
    VHI i385 (.Z(VCC_net));
    LUT4 i1_2_lut_rep_2_3_lut (.A(CPT[3]), .B(CPT[4]), .C(n36), .Z(n459)) /* synthesis lut_function=(A+(B+(C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam i1_2_lut_rep_2_3_lut.init = 16'hfefe;
    OB Q1_pad (.I(Q1_c), .O(Q1));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(8[3:5])
    LUT4 i2_4_lut (.A(CPT[12]), .B(CPT[10]), .C(n371), .D(CPT[9]), .Z(n6_adj_1)) /* synthesis lut_function=(A (B+(C (D)))) */ ;
    defparam i2_4_lut.init = 16'ha888;
    CCU2D CPT_21_add_4_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[0]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .COUT(n354), 
          .S1(n90));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21_add_4_1.INIT0 = 16'hF000;
    defparam CPT_21_add_4_1.INIT1 = 16'h0555;
    defparam CPT_21_add_4_1.INJECT1_0 = "NO";
    defparam CPT_21_add_4_1.INJECT1_1 = "NO";
    LUT4 i346_4_lut (.A(CPT[14]), .B(n366), .C(CPT[13]), .D(CPT[12]), 
         .Z(n15_adj_2)) /* synthesis lut_function=(!(A+(B (C)+!B (C (D))))) */ ;
    defparam i346_4_lut.init = 16'h0515;
    LUT4 i1_4_lut (.A(CPT[14]), .B(CPT[11]), .C(n6_adj_1), .D(CPT[13]), 
         .Z(n17)) /* synthesis lut_function=(A+(B (C (D)))) */ ;
    defparam i1_4_lut.init = 16'heaaa;
    FD1S3IX CPT_21__i0 (.D(n90), .CK(CLK_c), .CD(n164), .Q(CPT[0])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21__i0.GSR = "ENABLED";
    LUT4 i2_3_lut_4_lut (.A(n36), .B(n461), .C(CPT[5]), .D(CPT[6]), 
         .Z(n365)) /* synthesis lut_function=(A (C (D))+!A (B (C (D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam i2_3_lut_4_lut.init = 16'he000;
    CCU2D CPT_21_add_4_7 (.A0(CPT[5]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[6]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n356), 
          .COUT(n357), .S0(n85), .S1(n84));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21_add_4_7.INIT0 = 16'hfaaa;
    defparam CPT_21_add_4_7.INIT1 = 16'hfaaa;
    defparam CPT_21_add_4_7.INJECT1_0 = "NO";
    defparam CPT_21_add_4_7.INJECT1_1 = "NO";
    LUT4 i2_3_lut (.A(CPT[2]), .B(CPT[0]), .C(CPT[1]), .Z(n36)) /* synthesis lut_function=(A+(B+(C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam i2_3_lut.init = 16'hfefe;
    CCU2D CPT_21_add_4_3 (.A0(CPT[1]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[2]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n354), 
          .COUT(n355), .S0(n89), .S1(n88));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21_add_4_3.INIT0 = 16'hfaaa;
    defparam CPT_21_add_4_3.INIT1 = 16'hfaaa;
    defparam CPT_21_add_4_3.INJECT1_0 = "NO";
    defparam CPT_21_add_4_3.INJECT1_1 = "NO";
    LUT4 i1_4_lut_adj_1 (.A(CPT[6]), .B(CPT[3]), .C(n36), .D(CPT[4]), 
         .Z(n27)) /* synthesis lut_function=(A+(B (C (D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(12[8:11])
    defparam i1_4_lut_adj_1.init = 16'heaaa;
    LUT4 i1_2_lut_rep_4 (.A(CPT[3]), .B(CPT[4]), .Z(n461)) /* synthesis lut_function=(A+(B)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam i1_2_lut_rep_4.init = 16'heeee;
    LUT4 i5_3_lut (.A(n417), .B(CPT[13]), .C(CPT[14]), .Z(n14)) /* synthesis lut_function=((B+(C))+!A) */ ;
    defparam i5_3_lut.init = 16'hfdfd;
    LUT4 i6_4_lut (.A(CPT[11]), .B(n12), .C(n403), .D(CPT[7]), .Z(n366)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i6_4_lut.init = 16'h8000;
    LUT4 i1_4_lut_adj_2 (.A(CPT[16]), .B(CPT[15]), .C(n6), .D(CPT[14]), 
         .Z(Q2_c)) /* synthesis lut_function=(A+(B (C (D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam i1_4_lut_adj_2.init = 16'heaaa;
    LUT4 i349_4_lut (.A(CPT[10]), .B(n460), .C(CPT[15]), .D(CPT[9]), 
         .Z(n417)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i349_4_lut.init = 16'h8000;
    CCU2D CPT_21_add_4_13 (.A0(CPT[11]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[12]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n359), 
          .COUT(n360), .S0(n79), .S1(n78));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21_add_4_13.INIT0 = 16'hfaaa;
    defparam CPT_21_add_4_13.INIT1 = 16'hfaaa;
    defparam CPT_21_add_4_13.INJECT1_0 = "NO";
    defparam CPT_21_add_4_13.INJECT1_1 = "NO";
    LUT4 i5_4_lut (.A(CPT[10]), .B(CPT[6]), .C(CPT[8]), .D(CPT[9]), 
         .Z(n12)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i5_4_lut.init = 16'h8000;
    LUT4 i1_2_lut_3_lut (.A(CPT[3]), .B(CPT[4]), .C(CPT[5]), .Z(n403)) /* synthesis lut_function=(A+(B+(C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam i1_2_lut_3_lut.init = 16'hfefe;
    CCU2D CPT_21_add_4_17 (.A0(CPT[15]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[16]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n361), 
          .S0(n75), .S1(n74));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21_add_4_17.INIT0 = 16'hfaaa;
    defparam CPT_21_add_4_17.INIT1 = 16'hfaaa;
    defparam CPT_21_add_4_17.INJECT1_0 = "NO";
    defparam CPT_21_add_4_17.INJECT1_1 = "NO";
    LUT4 i2_4_lut_adj_3 (.A(n22), .B(CPT[13]), .C(CPT[12]), .D(CPT[11]), 
         .Z(n6)) /* synthesis lut_function=(A (B (C+(D)))+!A (B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam i2_4_lut_adj_3.init = 16'hc8c0;
    OB Q2_pad (.I(Q2_c), .O(Q2));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(8[7:9])
    VLO i1 (.Z(GND_net));
    LUT4 i48_4_lut (.A(n365), .B(CPT[10]), .C(CPT[9]), .D(n4), .Z(n22)) /* synthesis lut_function=(A (B+(C))+!A (B+(C (D)))) */ ;
    defparam i48_4_lut.init = 16'hfcec;
    CCU2D CPT_21_add_4_5 (.A0(CPT[3]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[4]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n355), 
          .COUT(n356), .S0(n87), .S1(n86));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21_add_4_5.INIT0 = 16'hfaaa;
    defparam CPT_21_add_4_5.INIT1 = 16'hfaaa;
    defparam CPT_21_add_4_5.INJECT1_0 = "NO";
    defparam CPT_21_add_4_5.INJECT1_1 = "NO";
    FD1S3IX CPT_21__i16 (.D(n74), .CK(CLK_c), .CD(n164), .Q(CPT[16])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21__i16.GSR = "ENABLED";
    CCU2D CPT_21_add_4_11 (.A0(CPT[9]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[10]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n358), 
          .COUT(n359), .S0(n81), .S1(n80));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21_add_4_11.INIT0 = 16'hfaaa;
    defparam CPT_21_add_4_11.INIT1 = 16'hfaaa;
    defparam CPT_21_add_4_11.INJECT1_0 = "NO";
    defparam CPT_21_add_4_11.INJECT1_1 = "NO";
    TSALL TSALL_INST (.TSALL(GND_net));
    CCU2D CPT_21_add_4_15 (.A0(CPT[13]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[14]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n360), 
          .COUT(n361), .S0(n77), .S1(n76));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21_add_4_15.INIT0 = 16'hfaaa;
    defparam CPT_21_add_4_15.INIT1 = 16'hfaaa;
    defparam CPT_21_add_4_15.INJECT1_0 = "NO";
    defparam CPT_21_add_4_15.INJECT1_1 = "NO";
    LUT4 i1_2_lut_rep_3 (.A(CPT[5]), .B(CPT[7]), .Z(n460)) /* synthesis lut_function=(A (B)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam i1_2_lut_rep_3.init = 16'h8888;
    LUT4 i2_4_lut_4_lut (.A(CPT[5]), .B(CPT[7]), .C(CPT[8]), .D(n27), 
         .Z(n371)) /* synthesis lut_function=(A (B+(C))+!A (B (C+(D))+!B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam i2_4_lut_4_lut.init = 16'hfcf8;
    CCU2D CPT_21_add_4_9 (.A0(CPT[7]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[8]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n357), 
          .COUT(n358), .S0(n83), .S1(n82));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21_add_4_9.INIT0 = 16'hfaaa;
    defparam CPT_21_add_4_9.INIT1 = 16'hfaaa;
    defparam CPT_21_add_4_9.INJECT1_0 = "NO";
    defparam CPT_21_add_4_9.INJECT1_1 = "NO";
    LUT4 i6_4_lut_adj_4 (.A(n459), .B(CPT[11]), .C(CPT[16]), .D(CPT[8]), 
         .Z(n15)) /* synthesis lut_function=(A+(B+((D)+!C))) */ ;
    defparam i6_4_lut_adj_4.init = 16'hffef;
    GSR GSR_INST (.GSR(ENABLE_c));
    LUT4 i1_2_lut (.A(CPT[16]), .B(n20), .Z(Q1_c)) /* synthesis lut_function=(!(A+!(B))) */ ;
    defparam i1_2_lut.init = 16'h4444;
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    LUT4 i1_2_lut_adj_5 (.A(CPT[8]), .B(CPT[7]), .Z(n4)) /* synthesis lut_function=(A+(B)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(12[8:11])
    defparam i1_2_lut_adj_5.init = 16'heeee;
    PFUMX i34 (.BLUT(n17), .ALUT(n15_adj_2), .C0(CPT[15]), .Z(n20));
    FD1S3IX CPT_21__i15 (.D(n75), .CK(CLK_c), .CD(n164), .Q(CPT[15])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21__i15.GSR = "ENABLED";
    FD1S3IX CPT_21__i14 (.D(n76), .CK(CLK_c), .CD(n164), .Q(CPT[14])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21__i14.GSR = "ENABLED";
    FD1S3IX CPT_21__i13 (.D(n77), .CK(CLK_c), .CD(n164), .Q(CPT[13])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21__i13.GSR = "ENABLED";
    FD1S3IX CPT_21__i12 (.D(n78), .CK(CLK_c), .CD(n164), .Q(CPT[12])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21__i12.GSR = "ENABLED";
    FD1S3IX CPT_21__i11 (.D(n79), .CK(CLK_c), .CD(n164), .Q(CPT[11])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21__i11.GSR = "ENABLED";
    FD1S3IX CPT_21__i10 (.D(n80), .CK(CLK_c), .CD(n164), .Q(CPT[10])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21__i10.GSR = "ENABLED";
    FD1S3IX CPT_21__i9 (.D(n81), .CK(CLK_c), .CD(n164), .Q(CPT[9])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21__i9.GSR = "ENABLED";
    FD1S3IX CPT_21__i8 (.D(n82), .CK(CLK_c), .CD(n164), .Q(CPT[8])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21__i8.GSR = "ENABLED";
    FD1S3IX CPT_21__i7 (.D(n83), .CK(CLK_c), .CD(n164), .Q(CPT[7])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21__i7.GSR = "ENABLED";
    FD1S3IX CPT_21__i6 (.D(n84), .CK(CLK_c), .CD(n164), .Q(CPT[6])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21__i6.GSR = "ENABLED";
    FD1S3IX CPT_21__i5 (.D(n85), .CK(CLK_c), .CD(n164), .Q(CPT[5])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21__i5.GSR = "ENABLED";
    FD1S3IX CPT_21__i4 (.D(n86), .CK(CLK_c), .CD(n164), .Q(CPT[4])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21__i4.GSR = "ENABLED";
    FD1S3IX CPT_21__i3 (.D(n87), .CK(CLK_c), .CD(n164), .Q(CPT[3])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21__i3.GSR = "ENABLED";
    FD1S3IX CPT_21__i2 (.D(n88), .CK(CLK_c), .CD(n164), .Q(CPT[2])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21__i2.GSR = "ENABLED";
    FD1S3IX CPT_21__i1 (.D(n89), .CK(CLK_c), .CD(n164), .Q(CPT[1])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam CPT_21__i1.GSR = "ENABLED";
    LUT4 i355_4_lut (.A(n15), .B(CPT[6]), .C(n14), .D(CPT[12]), .Z(n164)) /* synthesis lut_function=(!(A+(B+(C+(D))))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(19[16:19])
    defparam i355_4_lut.init = 16'h0001;
    IB CLK_pad (.I(CLK), .O(CLK_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(7[7:10])
    IB ENABLE_pad (.I(ENABLE), .O(ENABLE_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(7[12:18])
    
endmodule
//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

