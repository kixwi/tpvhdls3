/* Verilog model created from schematic SCHEMA.sch -- Sep 28, 2023 17:10 */

module SCHEMA( ENABLE, Q1, Q2, STDBY );
 input ENABLE;
output Q1;
output Q2;
 input STDBY;
wire N_1;
wire N_2;



ENTOSCILLATEUR I4 ( .OSCIL(N_2), .STDBY(STDBY) );
ENTITYGENEPULSE I2 ( .CLK(N_1), .ENABLE(ENABLE), .Q1(Q1), .Q2(Q2) );
PLL1 I5 ( .CLKI(N_2), .CLKOP(N_1) );

endmodule // SCHEMA
