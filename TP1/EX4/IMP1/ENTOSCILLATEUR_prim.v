// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Thu Sep 28 16:57:03 2023
//
// Verilog Description of module ENTOSCILLATEUR
//

module ENTOSCILLATEUR (STDBY, OSCIL, SEDSTDBY);   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/oscillateur.vhd(8[8:22])
    input STDBY;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/oscillateur.vhd(10[2:7])
    output OSCIL;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/oscillateur.vhd(11[2:7])
    output SEDSTDBY;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/oscillateur.vhd(12[2:10])
    
    
    wire STDBY_c, OSCIL_c, SEDSTDBY_c, GND_net, VCC_net;
    
    OB OSCIL_pad (.I(OSCIL_c), .O(OSCIL));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/oscillateur.vhd(11[2:7])
    VLO i23 (.Z(GND_net));
    OSCH M (.STDBY(STDBY_c), .SEDSTDBY(SEDSTDBY_c), .OSC(OSCIL_c)) /* synthesis syn_instantiated=1 */ ;
    defparam M.NOM_FREQ = "133";
    OB SEDSTDBY_pad (.I(SEDSTDBY_c), .O(SEDSTDBY));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/oscillateur.vhd(12[2:10])
    IB STDBY_pad (.I(STDBY), .O(STDBY_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/oscillateur.vhd(10[2:7])
    GSR GSR_INST (.GSR(VCC_net));
    TSALL TSALL_INST (.TSALL(GND_net));
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    VHI i24 (.Z(VCC_net));
    
endmodule
//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

