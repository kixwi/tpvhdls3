// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Thu Sep 28 17:10:06 2023
//
// Verilog Description of module SCHEMA
//

module SCHEMA (ENABLE, Q1, Q2, STDBY) /* synthesis syn_module_defined=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(3[8:14])
    input ENABLE;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(4[8:14])
    output Q1;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(5[8:10])
    output Q2;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(6[8:10])
    input STDBY;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(7[8:13])
    
    wire N_1 /* synthesis SET_AS_NETWORK=N_1, is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(8[6:9])
    wire N_2 /* synthesis is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(9[6:9])
    
    wire ENABLE_c, Q1_c, Q2_c, STDBY_c, GND_net, VCC_net;
    
    ENTOSCILLATEUR I4 (.STDBY_c(STDBY_c), .N_2(N_2));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(13[16:49])
    ENTITYGENEPULSE I2 (.Q2_c(Q2_c), .N_1(N_1), .Q1_c(Q1_c), .GND_net(GND_net));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(14[17:68])
    TSALL TSALL_INST (.TSALL(GND_net));
    IB STDBY_pad (.I(STDBY), .O(STDBY_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(7[8:13])
    IB ENABLE_pad (.I(ENABLE), .O(ENABLE_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(4[8:14])
    OB Q2_pad (.I(Q2_c), .O(Q2));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(6[8:10])
    OB Q1_pad (.I(Q1_c), .O(Q1));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(5[8:10])
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    GSR GSR_INST (.GSR(ENABLE_c));
    PLL1 I5 (.N_2(N_2), .N_1(N_1), .GND_net(GND_net)) /* synthesis NGD_DRC_MASK=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(15[6:36])
    VLO i1 (.Z(GND_net));
    VHI i370 (.Z(VCC_net));
    
endmodule
//
// Verilog Description of module ENTOSCILLATEUR
//

module ENTOSCILLATEUR (STDBY_c, N_2);
    input STDBY_c;
    output N_2;
    
    wire N_2 /* synthesis is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(9[6:9])
    
    OSCH M (.STDBY(STDBY_c), .OSC(N_2)) /* synthesis syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=16, LSE_RCOL=49, LSE_LLINE=13, LSE_RLINE=13 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(13[16:49])
    defparam M.NOM_FREQ = "133";
    
endmodule
//
// Verilog Description of module ENTITYGENEPULSE
//

module ENTITYGENEPULSE (Q2_c, N_1, Q1_c, GND_net);
    output Q2_c;
    input N_1;
    output Q1_c;
    input GND_net;
    
    wire N_1 /* synthesis SET_AS_NETWORK=N_1, is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(8[6:9])
    wire [16:0]CPT;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(14[8:11])
    
    wire n12, n18, n6, n10, n4, n5, n22, n4_adj_40, n138;
    wire [16:0]n73;
    
    wire n121, n14, n117, n15, n14_adj_41, n19, n17, n346, n334, 
        n307, n295, n294, n293, n292, n291, n290, n289, n288;
    
    LUT4 i5_4_lut (.A(CPT[6]), .B(CPT[10]), .C(CPT[7]), .D(CPT[11]), 
         .Z(n12)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i5_4_lut.init = 16'h8000;
    LUT4 i2_4_lut (.A(CPT[11]), .B(CPT[10]), .C(n18), .D(CPT[9]), .Z(n6)) /* synthesis lut_function=(A (B+(C (D)))) */ ;
    defparam i2_4_lut.init = 16'ha888;
    LUT4 i1_2_lut (.A(CPT[8]), .B(CPT[13]), .Z(n10)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i1_2_lut.init = 16'heeee;
    LUT4 i23_4_lut (.A(CPT[6]), .B(CPT[8]), .C(CPT[7]), .D(n4), .Z(n18)) /* synthesis lut_function=(A (B+(C))+!A (B+(C (D)))) */ ;
    defparam i23_4_lut.init = 16'hfcec;
    LUT4 i48_4_lut (.A(n5), .B(CPT[16]), .C(CPT[15]), .D(CPT[13]), .Z(Q2_c)) /* synthesis lut_function=(A (B+(C (D)))+!A (B)) */ ;
    defparam i48_4_lut.init = 16'heccc;
    LUT4 i1_4_lut (.A(n22), .B(CPT[14]), .C(CPT[12]), .D(CPT[11]), .Z(n5)) /* synthesis lut_function=(A (B (C+(D)))+!A (B (C))) */ ;
    defparam i1_4_lut.init = 16'hc8c0;
    LUT4 i41_4_lut (.A(CPT[8]), .B(CPT[10]), .C(CPT[9]), .D(n4_adj_40), 
         .Z(n22)) /* synthesis lut_function=(A (B+(C))+!A (B+(C (D)))) */ ;
    defparam i41_4_lut.init = 16'hfcec;
    FD1S3IX CPT_11__i1 (.D(n73[1]), .CK(N_1), .CD(n138), .Q(CPT[1])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11__i1.GSR = "ENABLED";
    LUT4 i1_4_lut_adj_1 (.A(CPT[7]), .B(CPT[6]), .C(CPT[5]), .D(n121), 
         .Z(n4_adj_40)) /* synthesis lut_function=(A+(B (C (D)))) */ ;
    defparam i1_4_lut_adj_1.init = 16'heaaa;
    FD1S3IX CPT_11__i0 (.D(n73[0]), .CK(N_1), .CD(n138), .Q(CPT[0])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11__i0.GSR = "ENABLED";
    LUT4 i1_2_lut_adj_2 (.A(CPT[16]), .B(n14), .Z(Q1_c)) /* synthesis lut_function=(!(A+!(B))) */ ;
    defparam i1_2_lut_adj_2.init = 16'h4444;
    LUT4 i1_4_lut_adj_3 (.A(CPT[5]), .B(CPT[4]), .C(CPT[3]), .D(n117), 
         .Z(n4)) /* synthesis lut_function=(A+(B (C (D)))) */ ;
    defparam i1_4_lut_adj_3.init = 16'heaaa;
    LUT4 i331_4_lut (.A(n15), .B(n121), .C(n14_adj_41), .D(CPT[16]), 
         .Z(n138)) /* synthesis lut_function=(!(A+(B+(C+!(D))))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(20[7:17])
    defparam i331_4_lut.init = 16'h0100;
    PFUMX i32 (.BLUT(n19), .ALUT(n17), .C0(CPT[15]), .Z(n14));
    LUT4 i2_3_lut (.A(CPT[2]), .B(CPT[1]), .C(CPT[0]), .Z(n117)) /* synthesis lut_function=(A+(B+(C))) */ ;
    defparam i2_3_lut.init = 16'hfefe;
    FD1S3IX CPT_11__i16 (.D(n73[16]), .CK(N_1), .CD(n138), .Q(CPT[16])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11__i16.GSR = "ENABLED";
    LUT4 i315_3_lut (.A(CPT[7]), .B(CPT[15]), .C(CPT[10]), .Z(n346)) /* synthesis lut_function=(A (B (C))) */ ;
    defparam i315_3_lut.init = 16'h8080;
    LUT4 i1_2_lut_3_lut (.A(CPT[3]), .B(CPT[4]), .C(CPT[5]), .Z(n334)) /* synthesis lut_function=(A+(B+(C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(20[7:17])
    defparam i1_2_lut_3_lut.init = 16'hfefe;
    LUT4 i1_2_lut_3_lut_adj_4 (.A(CPT[3]), .B(CPT[4]), .C(n117), .Z(n121)) /* synthesis lut_function=(A+(B+(C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(20[7:17])
    defparam i1_2_lut_3_lut_adj_4.init = 16'hfefe;
    LUT4 i1_4_lut_adj_5 (.A(CPT[14]), .B(CPT[12]), .C(n6), .D(CPT[13]), 
         .Z(n19)) /* synthesis lut_function=(A+(B (C (D)))) */ ;
    defparam i1_4_lut_adj_5.init = 16'heaaa;
    LUT4 i314_4_lut (.A(CPT[14]), .B(n307), .C(CPT[13]), .D(CPT[12]), 
         .Z(n17)) /* synthesis lut_function=(!(A+(B (C)+!B (C (D))))) */ ;
    defparam i314_4_lut.init = 16'h0515;
    LUT4 i5_4_lut_adj_6 (.A(CPT[5]), .B(n10), .C(n346), .D(CPT[9]), 
         .Z(n14_adj_41)) /* synthesis lut_function=((B+!(C (D)))+!A) */ ;
    defparam i5_4_lut_adj_6.init = 16'hdfff;
    FD1S3IX CPT_11__i2 (.D(n73[2]), .CK(N_1), .CD(n138), .Q(CPT[2])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11__i2.GSR = "ENABLED";
    LUT4 i6_4_lut (.A(CPT[6]), .B(CPT[11]), .C(CPT[12]), .D(CPT[14]), 
         .Z(n15)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;
    defparam i6_4_lut.init = 16'hfffe;
    LUT4 i6_4_lut_adj_7 (.A(CPT[9]), .B(n12), .C(n334), .D(CPT[8]), 
         .Z(n307)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i6_4_lut_adj_7.init = 16'h8000;
    CCU2D CPT_11_add_4_17 (.A0(CPT[15]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[16]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n295), 
          .S0(n73[15]), .S1(n73[16]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11_add_4_17.INIT0 = 16'hfaaa;
    defparam CPT_11_add_4_17.INIT1 = 16'hfaaa;
    defparam CPT_11_add_4_17.INJECT1_0 = "NO";
    defparam CPT_11_add_4_17.INJECT1_1 = "NO";
    CCU2D CPT_11_add_4_15 (.A0(CPT[13]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[14]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n294), 
          .COUT(n295), .S0(n73[13]), .S1(n73[14]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11_add_4_15.INIT0 = 16'hfaaa;
    defparam CPT_11_add_4_15.INIT1 = 16'hfaaa;
    defparam CPT_11_add_4_15.INJECT1_0 = "NO";
    defparam CPT_11_add_4_15.INJECT1_1 = "NO";
    CCU2D CPT_11_add_4_13 (.A0(CPT[11]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[12]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n293), 
          .COUT(n294), .S0(n73[11]), .S1(n73[12]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11_add_4_13.INIT0 = 16'hfaaa;
    defparam CPT_11_add_4_13.INIT1 = 16'hfaaa;
    defparam CPT_11_add_4_13.INJECT1_0 = "NO";
    defparam CPT_11_add_4_13.INJECT1_1 = "NO";
    FD1S3IX CPT_11__i15 (.D(n73[15]), .CK(N_1), .CD(n138), .Q(CPT[15])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11__i15.GSR = "ENABLED";
    FD1S3IX CPT_11__i14 (.D(n73[14]), .CK(N_1), .CD(n138), .Q(CPT[14])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11__i14.GSR = "ENABLED";
    FD1S3IX CPT_11__i13 (.D(n73[13]), .CK(N_1), .CD(n138), .Q(CPT[13])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11__i13.GSR = "ENABLED";
    FD1S3IX CPT_11__i12 (.D(n73[12]), .CK(N_1), .CD(n138), .Q(CPT[12])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11__i12.GSR = "ENABLED";
    FD1S3IX CPT_11__i11 (.D(n73[11]), .CK(N_1), .CD(n138), .Q(CPT[11])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11__i11.GSR = "ENABLED";
    FD1S3IX CPT_11__i10 (.D(n73[10]), .CK(N_1), .CD(n138), .Q(CPT[10])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11__i10.GSR = "ENABLED";
    FD1S3IX CPT_11__i9 (.D(n73[9]), .CK(N_1), .CD(n138), .Q(CPT[9])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11__i9.GSR = "ENABLED";
    FD1S3IX CPT_11__i8 (.D(n73[8]), .CK(N_1), .CD(n138), .Q(CPT[8])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11__i8.GSR = "ENABLED";
    FD1S3IX CPT_11__i7 (.D(n73[7]), .CK(N_1), .CD(n138), .Q(CPT[7])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11__i7.GSR = "ENABLED";
    FD1S3IX CPT_11__i6 (.D(n73[6]), .CK(N_1), .CD(n138), .Q(CPT[6])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11__i6.GSR = "ENABLED";
    FD1S3IX CPT_11__i5 (.D(n73[5]), .CK(N_1), .CD(n138), .Q(CPT[5])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11__i5.GSR = "ENABLED";
    FD1S3IX CPT_11__i4 (.D(n73[4]), .CK(N_1), .CD(n138), .Q(CPT[4])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11__i4.GSR = "ENABLED";
    FD1S3IX CPT_11__i3 (.D(n73[3]), .CK(N_1), .CD(n138), .Q(CPT[3])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11__i3.GSR = "ENABLED";
    CCU2D CPT_11_add_4_11 (.A0(CPT[9]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[10]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n292), 
          .COUT(n293), .S0(n73[9]), .S1(n73[10]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11_add_4_11.INIT0 = 16'hfaaa;
    defparam CPT_11_add_4_11.INIT1 = 16'hfaaa;
    defparam CPT_11_add_4_11.INJECT1_0 = "NO";
    defparam CPT_11_add_4_11.INJECT1_1 = "NO";
    CCU2D CPT_11_add_4_9 (.A0(CPT[7]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[8]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n291), 
          .COUT(n292), .S0(n73[7]), .S1(n73[8]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11_add_4_9.INIT0 = 16'hfaaa;
    defparam CPT_11_add_4_9.INIT1 = 16'hfaaa;
    defparam CPT_11_add_4_9.INJECT1_0 = "NO";
    defparam CPT_11_add_4_9.INJECT1_1 = "NO";
    CCU2D CPT_11_add_4_7 (.A0(CPT[5]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[6]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n290), 
          .COUT(n291), .S0(n73[5]), .S1(n73[6]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11_add_4_7.INIT0 = 16'hfaaa;
    defparam CPT_11_add_4_7.INIT1 = 16'hfaaa;
    defparam CPT_11_add_4_7.INJECT1_0 = "NO";
    defparam CPT_11_add_4_7.INJECT1_1 = "NO";
    CCU2D CPT_11_add_4_5 (.A0(CPT[3]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[4]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n289), 
          .COUT(n290), .S0(n73[3]), .S1(n73[4]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11_add_4_5.INIT0 = 16'hfaaa;
    defparam CPT_11_add_4_5.INIT1 = 16'hfaaa;
    defparam CPT_11_add_4_5.INJECT1_0 = "NO";
    defparam CPT_11_add_4_5.INJECT1_1 = "NO";
    CCU2D CPT_11_add_4_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[0]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .COUT(n288), 
          .S1(n73[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11_add_4_1.INIT0 = 16'hF000;
    defparam CPT_11_add_4_1.INIT1 = 16'h0555;
    defparam CPT_11_add_4_1.INJECT1_0 = "NO";
    defparam CPT_11_add_4_1.INJECT1_1 = "NO";
    CCU2D CPT_11_add_4_3 (.A0(CPT[1]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[2]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n288), 
          .COUT(n289), .S0(n73[1]), .S1(n73[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/genepulse.vhd(21[16:19])
    defparam CPT_11_add_4_3.INIT0 = 16'hfaaa;
    defparam CPT_11_add_4_3.INIT1 = 16'hfaaa;
    defparam CPT_11_add_4_3.INJECT1_0 = "NO";
    defparam CPT_11_add_4_3.INJECT1_1 = "NO";
    
endmodule
//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

//
// Verilog Description of module PLL1
//

module PLL1 (N_2, N_1, GND_net) /* synthesis NGD_DRC_MASK=1 */ ;
    input N_2;
    output N_1;
    input GND_net;
    
    wire N_2 /* synthesis is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(9[6:9])
    wire N_1 /* synthesis SET_AS_NETWORK=N_1, is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(8[6:9])
    
    EHXPLLJ PLLInst_0 (.CLKI(N_2), .CLKFB(N_1), .PHASESEL0(GND_net), .PHASESEL1(GND_net), 
            .PHASEDIR(GND_net), .PHASESTEP(GND_net), .LOADREG(GND_net), 
            .STDBY(GND_net), .PLLWAKESYNC(GND_net), .RST(GND_net), .RESETC(GND_net), 
            .RESETD(GND_net), .RESETM(GND_net), .ENCLKOP(GND_net), .ENCLKOS(GND_net), 
            .ENCLKOS2(GND_net), .ENCLKOS3(GND_net), .PLLCLK(GND_net), 
            .PLLRST(GND_net), .PLLSTB(GND_net), .PLLWE(GND_net), .PLLDATI0(GND_net), 
            .PLLDATI1(GND_net), .PLLDATI2(GND_net), .PLLDATI3(GND_net), 
            .PLLDATI4(GND_net), .PLLDATI5(GND_net), .PLLDATI6(GND_net), 
            .PLLDATI7(GND_net), .PLLADDR0(GND_net), .PLLADDR1(GND_net), 
            .PLLADDR2(GND_net), .PLLADDR3(GND_net), .PLLADDR4(GND_net), 
            .CLKOP(N_1)) /* synthesis FREQUENCY_PIN_CLKOP="99.750000", FREQUENCY_PIN_CLKI="133.000000", ICP_CURRENT="7", LPF_RESISTOR="8", syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=6, LSE_RCOL=36, LSE_LLINE=15, LSE_RLINE=15 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex4/imp1/schema.v(15[6:36])
    defparam PLLInst_0.CLKI_DIV = 4;
    defparam PLLInst_0.CLKFB_DIV = 3;
    defparam PLLInst_0.CLKOP_DIV = 5;
    defparam PLLInst_0.CLKOS_DIV = 1;
    defparam PLLInst_0.CLKOS2_DIV = 1;
    defparam PLLInst_0.CLKOS3_DIV = 1;
    defparam PLLInst_0.CLKOP_ENABLE = "ENABLED";
    defparam PLLInst_0.CLKOS_ENABLE = "DISABLED";
    defparam PLLInst_0.CLKOS2_ENABLE = "DISABLED";
    defparam PLLInst_0.CLKOS3_ENABLE = "DISABLED";
    defparam PLLInst_0.VCO_BYPASS_A0 = "DISABLED";
    defparam PLLInst_0.VCO_BYPASS_B0 = "DISABLED";
    defparam PLLInst_0.VCO_BYPASS_C0 = "DISABLED";
    defparam PLLInst_0.VCO_BYPASS_D0 = "DISABLED";
    defparam PLLInst_0.CLKOP_CPHASE = 4;
    defparam PLLInst_0.CLKOS_CPHASE = 0;
    defparam PLLInst_0.CLKOS2_CPHASE = 0;
    defparam PLLInst_0.CLKOS3_CPHASE = 0;
    defparam PLLInst_0.CLKOP_FPHASE = 0;
    defparam PLLInst_0.CLKOS_FPHASE = 0;
    defparam PLLInst_0.CLKOS2_FPHASE = 0;
    defparam PLLInst_0.CLKOS3_FPHASE = 0;
    defparam PLLInst_0.FEEDBK_PATH = "CLKOP";
    defparam PLLInst_0.FRACN_ENABLE = "DISABLED";
    defparam PLLInst_0.FRACN_DIV = 0;
    defparam PLLInst_0.CLKOP_TRIM_POL = "RISING";
    defparam PLLInst_0.CLKOP_TRIM_DELAY = 0;
    defparam PLLInst_0.CLKOS_TRIM_POL = "FALLING";
    defparam PLLInst_0.CLKOS_TRIM_DELAY = 0;
    defparam PLLInst_0.PLL_USE_WB = "DISABLED";
    defparam PLLInst_0.PREDIVIDER_MUXA1 = 0;
    defparam PLLInst_0.PREDIVIDER_MUXB1 = 0;
    defparam PLLInst_0.PREDIVIDER_MUXC1 = 0;
    defparam PLLInst_0.PREDIVIDER_MUXD1 = 0;
    defparam PLLInst_0.OUTDIVIDER_MUXA2 = "DIVA";
    defparam PLLInst_0.OUTDIVIDER_MUXB2 = "DIVB";
    defparam PLLInst_0.OUTDIVIDER_MUXC2 = "DIVC";
    defparam PLLInst_0.OUTDIVIDER_MUXD2 = "DIVD";
    defparam PLLInst_0.PLL_LOCK_MODE = 0;
    defparam PLLInst_0.STDBY_ENABLE = "DISABLED";
    defparam PLLInst_0.DPHASE_SOURCE = "DISABLED";
    defparam PLLInst_0.PLLRST_ENA = "DISABLED";
    defparam PLLInst_0.MRST_ENA = "DISABLED";
    defparam PLLInst_0.DCRST_ENA = "DISABLED";
    defparam PLLInst_0.DDRST_ENA = "DISABLED";
    defparam PLLInst_0.INTFB_WAKE = "DISABLED";
    
endmodule
