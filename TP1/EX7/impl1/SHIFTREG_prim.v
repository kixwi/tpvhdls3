// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Fri Oct 13 09:52:23 2023
//
// Verilog Description of module SHIFTREG
//

module SHIFTREG (STDBY, SEL, D, Q, RAZ);   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(8[8:16])
    input STDBY;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(11[3:8])
    input [1:0]SEL;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(12[3:6])
    input [7:0]D;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(13[3:4])
    output [7:0]Q;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(14[3:4])
    input RAZ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(15[3:6])
    
    wire CLKINT /* synthesis SET_AS_NETWORK=CLKINT, is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(34[8:14])
    wire CLKINT2 /* synthesis is_clock=1, SET_AS_NETWORK=CLKINT2 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(35[8:15])
    
    wire GND_net, STDBY_c, SEL_c_1, SEL_c_0, D_c_7, D_c_6, D_c_5, 
        D_c_4, D_c_3, D_c_2, D_c_1, D_c_0, Q_c_7, Q_c_6, Q_c_5, 
        Q_c_4, Q_c_3, Q_c_2, Q_c_1, Q_c_0, RAZ_c;
    wire [7:0]STMP;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(33[8:12])
    wire [7:0]STMP_7__N_1;
    
    wire VCC_net, n1, n1_adj_53, n1_adj_54, n1_adj_55, n1_adj_56, 
        n1_adj_57, CLKINT2_enable_8;
    
    VHI i346 (.Z(VCC_net));
    OB Q_pad_7 (.I(Q_c_7), .O(Q[7]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(14[3:4])
    LUT4 STMP_7__I_0_i1_1_lut (.A(STMP[0]), .Z(Q_c_0)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(67[10:14])
    defparam STMP_7__I_0_i1_1_lut.init = 16'h5555;
    OSCH u1 (.STDBY(STDBY_c), .OSC(CLKINT)) /* synthesis syn_instantiated=1 */ ;
    defparam u1.NOM_FREQ = "2.08";
    LUT4 SEL_1__I_0_Mux_5_i3_3_lut (.A(n1_adj_56), .B(STMP[6]), .C(SEL_c_1), 
         .Z(STMP_7__N_1[5])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(55[3] 64[12])
    defparam SEL_1__I_0_Mux_5_i3_3_lut.init = 16'hcaca;
    OB Q_pad_6 (.I(Q_c_6), .O(Q[6]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(14[3:4])
    LUT4 STMP_7__I_0_i8_1_lut (.A(STMP[7]), .Z(Q_c_7)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(67[10:14])
    defparam STMP_7__I_0_i8_1_lut.init = 16'h5555;
    TSALL TSALL_INST (.TSALL(GND_net));
    LUT4 i336_2_lut (.A(SEL_c_0), .B(SEL_c_1), .Z(CLKINT2_enable_8)) /* synthesis lut_function=(!(A (B))) */ ;
    defparam i336_2_lut.init = 16'h7777;
    LUT4 i27_3_lut (.A(D_c_7), .B(STMP[6]), .C(SEL_c_0), .Z(STMP_7__N_1[7])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(55[3] 64[12])
    defparam i27_3_lut.init = 16'hcaca;
    LUT4 SEL_1__I_0_Mux_1_i1_3_lut (.A(D_c_1), .B(STMP[0]), .C(SEL_c_0), 
         .Z(n1_adj_54)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(55[3] 64[12])
    defparam SEL_1__I_0_Mux_1_i1_3_lut.init = 16'hcaca;
    LUT4 SEL_1__I_0_Mux_0_i3_3_lut (.A(D_c_0), .B(STMP[1]), .C(SEL_c_1), 
         .Z(STMP_7__N_1[0])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(55[3] 64[12])
    defparam SEL_1__I_0_Mux_0_i3_3_lut.init = 16'hcaca;
    LUT4 STMP_7__I_0_i6_1_lut (.A(STMP[5]), .Z(Q_c_5)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(67[10:14])
    defparam STMP_7__I_0_i6_1_lut.init = 16'h5555;
    LUT4 STMP_7__I_0_i5_1_lut (.A(STMP[4]), .Z(Q_c_4)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(67[10:14])
    defparam STMP_7__I_0_i5_1_lut.init = 16'h5555;
    LUT4 STMP_7__I_0_i4_1_lut (.A(STMP[3]), .Z(Q_c_3)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(67[10:14])
    defparam STMP_7__I_0_i4_1_lut.init = 16'h5555;
    LUT4 SEL_1__I_0_Mux_5_i1_3_lut (.A(D_c_5), .B(STMP[4]), .C(SEL_c_0), 
         .Z(n1_adj_56)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(55[3] 64[12])
    defparam SEL_1__I_0_Mux_5_i1_3_lut.init = 16'hcaca;
    LUT4 SEL_1__I_0_Mux_4_i3_3_lut (.A(n1_adj_53), .B(STMP[5]), .C(SEL_c_1), 
         .Z(STMP_7__N_1[4])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(55[3] 64[12])
    defparam SEL_1__I_0_Mux_4_i3_3_lut.init = 16'hcaca;
    LUT4 SEL_1__I_0_Mux_4_i1_3_lut (.A(D_c_4), .B(STMP[3]), .C(SEL_c_0), 
         .Z(n1_adj_53)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(55[3] 64[12])
    defparam SEL_1__I_0_Mux_4_i1_3_lut.init = 16'hcaca;
    FD1P3AX STMP_i7 (.D(STMP_7__N_1[7]), .SP(CLKINT2_enable_8), .CK(CLKINT2), 
            .Q(STMP[7]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(52[3] 65[10])
    defparam STMP_i7.GSR = "ENABLED";
    LUT4 SEL_1__I_0_Mux_3_i3_3_lut (.A(n1), .B(STMP[4]), .C(SEL_c_1), 
         .Z(STMP_7__N_1[3])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(55[3] 64[12])
    defparam SEL_1__I_0_Mux_3_i3_3_lut.init = 16'hcaca;
    LUT4 SEL_1__I_0_Mux_3_i1_3_lut (.A(D_c_3), .B(STMP[2]), .C(SEL_c_0), 
         .Z(n1)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(55[3] 64[12])
    defparam SEL_1__I_0_Mux_3_i1_3_lut.init = 16'hcaca;
    LUT4 SEL_1__I_0_Mux_2_i3_3_lut (.A(n1_adj_55), .B(STMP[3]), .C(SEL_c_1), 
         .Z(STMP_7__N_1[2])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(55[3] 64[12])
    defparam SEL_1__I_0_Mux_2_i3_3_lut.init = 16'hcaca;
    CLK_GEN u2 (.CLKINT(CLKINT), .GND_net(GND_net), .CLKINT2(CLKINT2));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(45[7:14])
    LUT4 SEL_1__I_0_Mux_2_i1_3_lut (.A(D_c_2), .B(STMP[1]), .C(SEL_c_0), 
         .Z(n1_adj_55)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(55[3] 64[12])
    defparam SEL_1__I_0_Mux_2_i1_3_lut.init = 16'hcaca;
    LUT4 SEL_1__I_0_Mux_1_i3_3_lut (.A(n1_adj_54), .B(STMP[2]), .C(SEL_c_1), 
         .Z(STMP_7__N_1[1])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(55[3] 64[12])
    defparam SEL_1__I_0_Mux_1_i3_3_lut.init = 16'hcaca;
    FD1P3AX STMP_i6 (.D(STMP_7__N_1[6]), .SP(CLKINT2_enable_8), .CK(CLKINT2), 
            .Q(STMP[6]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(52[3] 65[10])
    defparam STMP_i6.GSR = "ENABLED";
    FD1P3AX STMP_i5 (.D(STMP_7__N_1[5]), .SP(CLKINT2_enable_8), .CK(CLKINT2), 
            .Q(STMP[5]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(52[3] 65[10])
    defparam STMP_i5.GSR = "ENABLED";
    FD1P3AX STMP_i4 (.D(STMP_7__N_1[4]), .SP(CLKINT2_enable_8), .CK(CLKINT2), 
            .Q(STMP[4]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(52[3] 65[10])
    defparam STMP_i4.GSR = "ENABLED";
    FD1P3AX STMP_i3 (.D(STMP_7__N_1[3]), .SP(CLKINT2_enable_8), .CK(CLKINT2), 
            .Q(STMP[3]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(52[3] 65[10])
    defparam STMP_i3.GSR = "ENABLED";
    FD1P3AX STMP_i2 (.D(STMP_7__N_1[2]), .SP(CLKINT2_enable_8), .CK(CLKINT2), 
            .Q(STMP[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(52[3] 65[10])
    defparam STMP_i2.GSR = "ENABLED";
    FD1P3AX STMP_i1 (.D(STMP_7__N_1[1]), .SP(CLKINT2_enable_8), .CK(CLKINT2), 
            .Q(STMP[1]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(52[3] 65[10])
    defparam STMP_i1.GSR = "ENABLED";
    FD1P3AX STMP_i0 (.D(STMP_7__N_1[0]), .SP(CLKINT2_enable_8), .CK(CLKINT2), 
            .Q(STMP[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(52[3] 65[10])
    defparam STMP_i0.GSR = "ENABLED";
    GSR GSR_INST (.GSR(RAZ_c));
    OB Q_pad_5 (.I(Q_c_5), .O(Q[5]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(14[3:4])
    OB Q_pad_4 (.I(Q_c_4), .O(Q[4]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(14[3:4])
    OB Q_pad_3 (.I(Q_c_3), .O(Q[3]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(14[3:4])
    OB Q_pad_2 (.I(Q_c_2), .O(Q[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(14[3:4])
    OB Q_pad_1 (.I(Q_c_1), .O(Q[1]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(14[3:4])
    OB Q_pad_0 (.I(Q_c_0), .O(Q[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(14[3:4])
    IB STDBY_pad (.I(STDBY), .O(STDBY_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(11[3:8])
    IB SEL_pad_1 (.I(SEL[1]), .O(SEL_c_1));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(12[3:6])
    IB SEL_pad_0 (.I(SEL[0]), .O(SEL_c_0));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(12[3:6])
    IB D_pad_7 (.I(D[7]), .O(D_c_7));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(13[3:4])
    IB D_pad_6 (.I(D[6]), .O(D_c_6));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(13[3:4])
    IB D_pad_5 (.I(D[5]), .O(D_c_5));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(13[3:4])
    IB D_pad_4 (.I(D[4]), .O(D_c_4));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(13[3:4])
    IB D_pad_3 (.I(D[3]), .O(D_c_3));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(13[3:4])
    IB D_pad_2 (.I(D[2]), .O(D_c_2));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(13[3:4])
    IB D_pad_1 (.I(D[1]), .O(D_c_1));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(13[3:4])
    IB D_pad_0 (.I(D[0]), .O(D_c_0));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(13[3:4])
    IB RAZ_pad (.I(RAZ), .O(RAZ_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(15[3:6])
    LUT4 STMP_7__I_0_i7_1_lut (.A(STMP[6]), .Z(Q_c_6)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(67[10:14])
    defparam STMP_7__I_0_i7_1_lut.init = 16'h5555;
    LUT4 STMP_7__I_0_i3_1_lut (.A(STMP[2]), .Z(Q_c_2)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(67[10:14])
    defparam STMP_7__I_0_i3_1_lut.init = 16'h5555;
    LUT4 SEL_1__I_0_Mux_6_i3_3_lut (.A(n1_adj_57), .B(STMP[7]), .C(SEL_c_1), 
         .Z(STMP_7__N_1[6])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(55[3] 64[12])
    defparam SEL_1__I_0_Mux_6_i3_3_lut.init = 16'hcaca;
    LUT4 STMP_7__I_0_i2_1_lut (.A(STMP[1]), .Z(Q_c_1)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(67[10:14])
    defparam STMP_7__I_0_i2_1_lut.init = 16'h5555;
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    VLO i1 (.Z(GND_net));
    LUT4 SEL_1__I_0_Mux_6_i1_3_lut (.A(D_c_6), .B(STMP[5]), .C(SEL_c_0), 
         .Z(n1_adj_57)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(55[3] 64[12])
    defparam SEL_1__I_0_Mux_6_i1_3_lut.init = 16'hcaca;
    
endmodule
//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module CLK_GEN
//

module CLK_GEN (CLKINT, GND_net, CLKINT2);
    input CLKINT;
    input GND_net;
    output CLKINT2;
    
    wire CLKINT /* synthesis SET_AS_NETWORK=CLKINT, is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(34[8:14])
    wire CLKINT2 /* synthesis is_clock=1, SET_AS_NETWORK=CLKINT2 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex7/decal.vhd(35[8:15])
    wire [19:0]CPT;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(12[8:11])
    
    wire n86;
    wire [19:0]n85;
    
    wire n317, n318, n391, n370, n316, n315, n320, n321, n362, 
        n19, n15, n14, n314, n20, n323, n322, n319, n358, 
        n360;
    
    FD1S3IX CPT_13__i19 (.D(n85[19]), .CK(CLKINT), .CD(n86), .Q(CPT[19])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i19.GSR = "DISABLED";
    CCU2D CPT_13_add_4_9 (.A0(CPT[7]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[8]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n317), 
          .COUT(n318), .S0(n85[7]), .S1(n85[8]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13_add_4_9.INIT0 = 16'hfaaa;
    defparam CPT_13_add_4_9.INIT1 = 16'hfaaa;
    defparam CPT_13_add_4_9.INJECT1_0 = "NO";
    defparam CPT_13_add_4_9.INJECT1_1 = "NO";
    LUT4 i331_4_lut (.A(CPT[16]), .B(CPT[8]), .C(CPT[15]), .D(n391), 
         .Z(n370)) /* synthesis lut_function=(!(A+(B+(C+(D))))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(23[22:27])
    defparam i331_4_lut.init = 16'h0001;
    CCU2D CPT_13_add_4_7 (.A0(CPT[5]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[6]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n316), 
          .COUT(n317), .S0(n85[5]), .S1(n85[6]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13_add_4_7.INIT0 = 16'hfaaa;
    defparam CPT_13_add_4_7.INIT1 = 16'hfaaa;
    defparam CPT_13_add_4_7.INJECT1_0 = "NO";
    defparam CPT_13_add_4_7.INJECT1_1 = "NO";
    CCU2D CPT_13_add_4_5 (.A0(CPT[3]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[4]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n315), 
          .COUT(n316), .S0(n85[3]), .S1(n85[4]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13_add_4_5.INIT0 = 16'hfaaa;
    defparam CPT_13_add_4_5.INIT1 = 16'hfaaa;
    defparam CPT_13_add_4_5.INJECT1_0 = "NO";
    defparam CPT_13_add_4_5.INJECT1_1 = "NO";
    CCU2D CPT_13_add_4_15 (.A0(CPT[13]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[14]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n320), 
          .COUT(n321), .S0(n85[13]), .S1(n85[14]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13_add_4_15.INIT0 = 16'hfaaa;
    defparam CPT_13_add_4_15.INIT1 = 16'hfaaa;
    defparam CPT_13_add_4_15.INJECT1_0 = "NO";
    defparam CPT_13_add_4_15.INJECT1_1 = "NO";
    LUT4 i322_3_lut (.A(CPT[16]), .B(CPT[13]), .C(CPT[19]), .Z(n362)) /* synthesis lut_function=(A (B (C))) */ ;
    defparam i322_3_lut.init = 16'h8080;
    LUT4 i7_4_lut (.A(CPT[18]), .B(CPT[14]), .C(CPT[0]), .D(CPT[17]), 
         .Z(n19)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(23[22:27])
    defparam i7_4_lut.init = 16'hfffe;
    LUT4 i8_4_lut_rep_1 (.A(n15), .B(CPT[6]), .C(n14), .D(CPT[5]), .Z(n391)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(23[22:27])
    defparam i8_4_lut_rep_1.init = 16'hfffe;
    CCU2D CPT_13_add_4_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[0]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .COUT(n314), 
          .S1(n85[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13_add_4_1.INIT0 = 16'hF000;
    defparam CPT_13_add_4_1.INIT1 = 16'h0555;
    defparam CPT_13_add_4_1.INJECT1_0 = "NO";
    defparam CPT_13_add_4_1.INJECT1_1 = "NO";
    LUT4 i8_4_lut (.A(CPT[1]), .B(CPT[19]), .C(CPT[10]), .D(CPT[13]), 
         .Z(n20)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(23[22:27])
    defparam i8_4_lut.init = 16'hfffe;
    CCU2D CPT_13_add_4_21 (.A0(CPT[19]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(GND_net), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n323), 
          .S0(n85[19]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13_add_4_21.INIT0 = 16'hfaaa;
    defparam CPT_13_add_4_21.INIT1 = 16'h0000;
    defparam CPT_13_add_4_21.INJECT1_0 = "NO";
    defparam CPT_13_add_4_21.INJECT1_1 = "NO";
    CCU2D CPT_13_add_4_19 (.A0(CPT[17]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[18]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n322), 
          .COUT(n323), .S0(n85[17]), .S1(n85[18]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13_add_4_19.INIT0 = 16'hfaaa;
    defparam CPT_13_add_4_19.INIT1 = 16'hfaaa;
    defparam CPT_13_add_4_19.INJECT1_0 = "NO";
    defparam CPT_13_add_4_19.INJECT1_1 = "NO";
    CCU2D CPT_13_add_4_3 (.A0(CPT[1]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[2]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n314), 
          .COUT(n315), .S0(n85[1]), .S1(n85[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13_add_4_3.INIT0 = 16'hfaaa;
    defparam CPT_13_add_4_3.INIT1 = 16'hfaaa;
    defparam CPT_13_add_4_3.INJECT1_0 = "NO";
    defparam CPT_13_add_4_3.INJECT1_1 = "NO";
    CCU2D CPT_13_add_4_13 (.A0(CPT[11]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[12]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n319), 
          .COUT(n320), .S0(n85[11]), .S1(n85[12]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13_add_4_13.INIT0 = 16'hfaaa;
    defparam CPT_13_add_4_13.INIT1 = 16'hfaaa;
    defparam CPT_13_add_4_13.INJECT1_0 = "NO";
    defparam CPT_13_add_4_13.INJECT1_1 = "NO";
    CCU2D CPT_13_add_4_11 (.A0(CPT[9]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[10]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n318), 
          .COUT(n319), .S0(n85[9]), .S1(n85[10]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13_add_4_11.INIT0 = 16'hfaaa;
    defparam CPT_13_add_4_11.INIT1 = 16'hfaaa;
    defparam CPT_13_add_4_11.INJECT1_0 = "NO";
    defparam CPT_13_add_4_11.INJECT1_1 = "NO";
    CCU2D CPT_13_add_4_17 (.A0(CPT[15]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[16]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n321), 
          .COUT(n322), .S0(n85[15]), .S1(n85[16]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13_add_4_17.INIT0 = 16'hfaaa;
    defparam CPT_13_add_4_17.INIT1 = 16'hfaaa;
    defparam CPT_13_add_4_17.INJECT1_0 = "NO";
    defparam CPT_13_add_4_17.INJECT1_1 = "NO";
    FD1S3IX CPT_13__i18 (.D(n85[18]), .CK(CLKINT), .CD(n86), .Q(CPT[18])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i18.GSR = "DISABLED";
    FD1S3IX CPT_13__i17 (.D(n85[17]), .CK(CLKINT), .CD(n86), .Q(CPT[17])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i17.GSR = "DISABLED";
    FD1S3IX CPT_13__i16 (.D(n85[16]), .CK(CLKINT), .CD(n86), .Q(CPT[16])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i16.GSR = "DISABLED";
    FD1S3IX CPT_13__i15 (.D(n85[15]), .CK(CLKINT), .CD(n86), .Q(CPT[15])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i15.GSR = "DISABLED";
    FD1S3IX CPT_13__i14 (.D(n85[14]), .CK(CLKINT), .CD(n86), .Q(CPT[14])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i14.GSR = "DISABLED";
    FD1S3IX CPT_13__i13 (.D(n85[13]), .CK(CLKINT), .CD(n86), .Q(CPT[13])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i13.GSR = "DISABLED";
    FD1S3IX CPT_13__i12 (.D(n85[12]), .CK(CLKINT), .CD(n86), .Q(CPT[12])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i12.GSR = "DISABLED";
    FD1S3IX CPT_13__i11 (.D(n85[11]), .CK(CLKINT), .CD(n86), .Q(CPT[11])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i11.GSR = "DISABLED";
    FD1S3IX CPT_13__i10 (.D(n85[10]), .CK(CLKINT), .CD(n86), .Q(CPT[10])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i10.GSR = "DISABLED";
    FD1S3IX CPT_13__i9 (.D(n85[9]), .CK(CLKINT), .CD(n86), .Q(CPT[9])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i9.GSR = "DISABLED";
    FD1S3IX CPT_13__i8 (.D(n85[8]), .CK(CLKINT), .CD(n86), .Q(CPT[8])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i8.GSR = "DISABLED";
    FD1S3IX CPT_13__i7 (.D(n85[7]), .CK(CLKINT), .CD(n86), .Q(CPT[7])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i7.GSR = "DISABLED";
    FD1S3IX CPT_13__i6 (.D(n85[6]), .CK(CLKINT), .CD(n86), .Q(CPT[6])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i6.GSR = "DISABLED";
    FD1S3IX CPT_13__i5 (.D(n85[5]), .CK(CLKINT), .CD(n86), .Q(CPT[5])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i5.GSR = "DISABLED";
    FD1S3IX CPT_13__i4 (.D(n85[4]), .CK(CLKINT), .CD(n86), .Q(CPT[4])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i4.GSR = "DISABLED";
    FD1S3IX CPT_13__i3 (.D(n85[3]), .CK(CLKINT), .CD(n86), .Q(CPT[3])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i3.GSR = "DISABLED";
    FD1S3IX CPT_13__i2 (.D(n85[2]), .CK(CLKINT), .CD(n86), .Q(CPT[2])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i2.GSR = "DISABLED";
    FD1S3IX CPT_13__i1 (.D(n85[1]), .CK(CLKINT), .CD(n86), .Q(CPT[1])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i1.GSR = "DISABLED";
    LUT4 i318_4_lut (.A(CPT[14]), .B(CPT[15]), .C(CPT[17]), .D(CPT[10]), 
         .Z(n358)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i318_4_lut.init = 16'h8000;
    FD1S3IX CPT_13__i0 (.D(n85[0]), .CK(CLKINT), .CD(n86), .Q(CPT[0])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_13__i0.GSR = "DISABLED";
    LUT4 i6_4_lut (.A(CPT[7]), .B(CPT[9]), .C(CPT[11]), .D(CPT[4]), 
         .Z(n15)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(23[22:27])
    defparam i6_4_lut.init = 16'hfffe;
    LUT4 i5_3_lut (.A(CPT[2]), .B(CPT[12]), .C(CPT[3]), .Z(n14)) /* synthesis lut_function=(A+(B+(C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(23[22:27])
    defparam i5_3_lut.init = 16'hfefe;
    LUT4 i333_3_lut (.A(n370), .B(n19), .C(n20), .Z(CLKINT2)) /* synthesis lut_function=(!((B+(C))+!A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(23[22:27])
    defparam i333_3_lut.init = 16'h0202;
    LUT4 i330_4_lut_4_lut (.A(n391), .B(n360), .C(n362), .D(n358), .Z(n86)) /* synthesis lut_function=(!(A+!(B (C (D))))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex5/clk_gen.vhd(18[6:17])
    defparam i330_4_lut_4_lut.init = 16'h4000;
    LUT4 i320_4_lut (.A(CPT[18]), .B(CPT[1]), .C(CPT[8]), .D(CPT[0]), 
         .Z(n360)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i320_4_lut.init = 16'h8000;
    
endmodule
//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

