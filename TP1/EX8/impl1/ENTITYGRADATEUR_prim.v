// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Fri Oct 13 11:23:09 2023
//
// Verilog Description of module ENTITYGRADATEUR
//

module ENTITYGRADATEUR (MLI, SIGNALLED);   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/gradateur.vhd(8[8:23])
    input MLI;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/gradateur.vhd(10[3:6])
    output [7:0]SIGNALLED;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/gradateur.vhd(11[3:12])
    
    
    wire GND_net, n3_c, VCC_net;
    
    VHI i12 (.Z(VCC_net));
    OB SIGNALLED_pad_7 (.I(GND_net), .O(SIGNALLED[7]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/gradateur.vhd(11[3:12])
    OB SIGNALLED_pad_6 (.I(GND_net), .O(SIGNALLED[6]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/gradateur.vhd(11[3:12])
    OB SIGNALLED_pad_5 (.I(GND_net), .O(SIGNALLED[5]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/gradateur.vhd(11[3:12])
    OB SIGNALLED_pad_4 (.I(GND_net), .O(SIGNALLED[4]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/gradateur.vhd(11[3:12])
    OB SIGNALLED_pad_3 (.I(GND_net), .O(SIGNALLED[3]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/gradateur.vhd(11[3:12])
    OB SIGNALLED_pad_2 (.I(GND_net), .O(SIGNALLED[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/gradateur.vhd(11[3:12])
    OB SIGNALLED_pad_1 (.I(GND_net), .O(SIGNALLED[1]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/gradateur.vhd(11[3:12])
    OB SIGNALLED_pad_0 (.I(n3_c), .O(SIGNALLED[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/gradateur.vhd(11[3:12])
    IB n3_pad (.I(MLI), .O(n3_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/gradateur.vhd(10[3:6])
    GSR GSR_INST (.GSR(VCC_net));
    TSALL TSALL_INST (.TSALL(GND_net));
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    VLO i1 (.Z(GND_net));
    
endmodule
//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

