// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Fri Oct 13 11:31:18 2023
//
// Verilog Description of module schema
//

module schema (CLEAR, CLKEN, DATAB, SIGNALLED, STDBY) /* synthesis syn_module_defined=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(3[8:14])
    input CLEAR;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(4[8:13])
    input CLKEN;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(5[8:13])
    input [7:0]DATAB;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(6[14:19])
    output [7:0]SIGNALLED;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(7[14:23])
    input STDBY;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(8[8:13])
    
    wire CLK /* synthesis is_clock=1, SET_AS_NETWORK=CLK */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(11[6:9])
    
    wire CLEAR_c, CLKEN_c, DATAB_c_7, DATAB_c_6, DATAB_c_5, DATAB_c_4, 
        DATAB_c_3, DATAB_c_2, DATAB_c_1, DATAB_c_0, VCC_net, SIGNALLED_c_0, 
        STDBY_c;
    wire [7:0]DATAA;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(9[14:19])
    
    wire GND_net;
    
    VHI i2 (.Z(VCC_net));
    COMP8 I3 (.VCC_net(VCC_net), .GND_net(GND_net), .DATAA({DATAA}), .DATAB_c_1(DATAB_c_1), 
          .DATAB_c_0(DATAB_c_0), .DATAB_c_3(DATAB_c_3), .DATAB_c_2(DATAB_c_2), 
          .DATAB_c_5(DATAB_c_5), .DATAB_c_4(DATAB_c_4), .DATAB_c_7(DATAB_c_7), 
          .DATAB_c_6(DATAB_c_6), .SIGNALLED_c_0(SIGNALLED_c_0)) /* synthesis NGD_DRC_MASK=1, syn_module_defined=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(18[7:64])
    OB SIGNALLED_pad_1 (.I(VCC_net), .O(SIGNALLED[1]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(7[14:23])
    OB SIGNALLED_pad_3 (.I(VCC_net), .O(SIGNALLED[3]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(7[14:23])
    OB SIGNALLED_pad_4 (.I(VCC_net), .O(SIGNALLED[4]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(7[14:23])
    OSCH I1 (.STDBY(STDBY_c), .OSC(CLK)) /* synthesis syn_instantiated=1 */ ;
    defparam I1.NOM_FREQ = "2.08";
    OB SIGNALLED_pad_5 (.I(VCC_net), .O(SIGNALLED[5]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(7[14:23])
    OB SIGNALLED_pad_6 (.I(VCC_net), .O(SIGNALLED[6]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(7[14:23])
    OB SIGNALLED_pad_7 (.I(VCC_net), .O(SIGNALLED[7]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(7[14:23])
    COUNT8 I2 (.CLK(CLK), .CLKEN_c(CLKEN_c), .CLEAR_c(CLEAR_c), .DATAA({DATAA}), 
           .GND_net(GND_net), .VCC_net(VCC_net)) /* synthesis NGD_DRC_MASK=1, syn_module_defined=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(17[8:72])
    OB SIGNALLED_pad_2 (.I(VCC_net), .O(SIGNALLED[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(7[14:23])
    OB SIGNALLED_pad_0 (.I(SIGNALLED_c_0), .O(SIGNALLED[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(7[14:23])
    IB CLEAR_pad (.I(CLEAR), .O(CLEAR_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(4[8:13])
    IB CLKEN_pad (.I(CLKEN), .O(CLKEN_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(5[8:13])
    IB DATAB_pad_7 (.I(DATAB[7]), .O(DATAB_c_7));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(6[14:19])
    IB DATAB_pad_6 (.I(DATAB[6]), .O(DATAB_c_6));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(6[14:19])
    IB DATAB_pad_5 (.I(DATAB[5]), .O(DATAB_c_5));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(6[14:19])
    IB DATAB_pad_4 (.I(DATAB[4]), .O(DATAB_c_4));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(6[14:19])
    IB DATAB_pad_3 (.I(DATAB[3]), .O(DATAB_c_3));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(6[14:19])
    IB DATAB_pad_2 (.I(DATAB[2]), .O(DATAB_c_2));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(6[14:19])
    IB DATAB_pad_1 (.I(DATAB[1]), .O(DATAB_c_1));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(6[14:19])
    IB DATAB_pad_0 (.I(DATAB[0]), .O(DATAB_c_0));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(6[14:19])
    IB STDBY_pad (.I(STDBY), .O(STDBY_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(8[8:13])
    GSR GSR_INST (.GSR(VCC_net));
    TSALL TSALL_INST (.TSALL(GND_net));
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    VLO i7 (.Z(GND_net));
    
endmodule
//
// Verilog Description of module COMP8
//

module COMP8 (VCC_net, GND_net, DATAA, DATAB_c_1, DATAB_c_0, DATAB_c_3, 
            DATAB_c_2, DATAB_c_5, DATAB_c_4, DATAB_c_7, DATAB_c_6, 
            SIGNALLED_c_0) /* synthesis NGD_DRC_MASK=1, syn_module_defined=1 */ ;
    input VCC_net;
    input GND_net;
    input [7:0]DATAA;
    input DATAB_c_1;
    input DATAB_c_0;
    input DATAB_c_3;
    input DATAB_c_2;
    input DATAB_c_5;
    input DATAB_c_4;
    input DATAB_c_7;
    input DATAB_c_6;
    output SIGNALLED_c_0;
    
    
    wire cmp_ci, co0, co1, co2, altb_out_c, co3;
    
    FADD2B cmp_ci_a (.A0(VCC_net), .A1(VCC_net), .B0(VCC_net), .B1(VCC_net), 
           .CI(GND_net), .COUT(cmp_ci)) /* synthesis syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=7, LSE_RCOL=64, LSE_LLINE=18, LSE_RLINE=18 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(18[7:64])
    AGEB2 cmp_0 (.A0(DATAA[0]), .A1(DATAA[1]), .B0(DATAB_c_0), .B1(DATAB_c_1), 
          .CI(cmp_ci), .GE(co0)) /* synthesis syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=7, LSE_RCOL=64, LSE_LLINE=18, LSE_RLINE=18 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(18[7:64])
    AGEB2 cmp_1 (.A0(DATAA[2]), .A1(DATAA[3]), .B0(DATAB_c_2), .B1(DATAB_c_3), 
          .CI(co0), .GE(co1)) /* synthesis syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=7, LSE_RCOL=64, LSE_LLINE=18, LSE_RLINE=18 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(18[7:64])
    AGEB2 cmp_2 (.A0(DATAA[4]), .A1(DATAA[5]), .B0(DATAB_c_4), .B1(DATAB_c_5), 
          .CI(co1), .GE(co2)) /* synthesis syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=7, LSE_RCOL=64, LSE_LLINE=18, LSE_RLINE=18 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(18[7:64])
    AGEB2 cmp_3 (.A0(DATAA[6]), .A1(DATAA[7]), .B0(DATAB_c_6), .B1(DATAB_c_7), 
          .CI(co2), .GE(altb_out_c)) /* synthesis syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=7, LSE_RCOL=64, LSE_LLINE=18, LSE_RLINE=18 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(18[7:64])
    FADD2B a0 (.A0(GND_net), .A1(GND_net), .B0(GND_net), .B1(GND_net), 
           .CI(altb_out_c), .S0(co3)) /* synthesis syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=7, LSE_RCOL=64, LSE_LLINE=18, LSE_RLINE=18 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(18[7:64])
    INV INV_0 (.A(co3), .Z(SIGNALLED_c_0)) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=7, LSE_RCOL=64, LSE_LLINE=18, LSE_RLINE=18 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(18[7:64])
    
endmodule
//
// Verilog Description of module COUNT8
//

module COUNT8 (CLK, CLKEN_c, CLEAR_c, DATAA, GND_net, VCC_net) /* synthesis NGD_DRC_MASK=1, syn_module_defined=1 */ ;
    input CLK;
    input CLKEN_c;
    input CLEAR_c;
    output [7:0]DATAA;
    input GND_net;
    input VCC_net;
    
    wire CLK /* synthesis is_clock=1, SET_AS_NETWORK=CLK */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(11[6:9])
    
    wire idataout1, idataout2, idataout3, idataout4, idataout5, idataout6, 
        idataout7, cnt_ci, idataout0, co0, co1, co2;
    
    FD1P3DX FF_6 (.D(idataout1), .SP(CLKEN_c), .CK(CLK), .CD(CLEAR_c), 
            .Q(DATAA[1])) /* synthesis GSR="ENABLED", syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=8, LSE_RCOL=72, LSE_LLINE=17, LSE_RLINE=17 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/count8.v(33[13:79])
    defparam FF_6.GSR = "ENABLED";
    FD1P3DX FF_5 (.D(idataout2), .SP(CLKEN_c), .CK(CLK), .CD(CLEAR_c), 
            .Q(DATAA[2])) /* synthesis GSR="ENABLED", syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=8, LSE_RCOL=72, LSE_LLINE=17, LSE_RLINE=17 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/count8.v(36[13:79])
    defparam FF_5.GSR = "ENABLED";
    FD1P3DX FF_4 (.D(idataout3), .SP(CLKEN_c), .CK(CLK), .CD(CLEAR_c), 
            .Q(DATAA[3])) /* synthesis GSR="ENABLED", syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=8, LSE_RCOL=72, LSE_LLINE=17, LSE_RLINE=17 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/count8.v(39[13:79])
    defparam FF_4.GSR = "ENABLED";
    FD1P3DX FF_3 (.D(idataout4), .SP(CLKEN_c), .CK(CLK), .CD(CLEAR_c), 
            .Q(DATAA[4])) /* synthesis GSR="ENABLED", syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=8, LSE_RCOL=72, LSE_LLINE=17, LSE_RLINE=17 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/count8.v(42[13:79])
    defparam FF_3.GSR = "ENABLED";
    FD1P3DX FF_2 (.D(idataout5), .SP(CLKEN_c), .CK(CLK), .CD(CLEAR_c), 
            .Q(DATAA[5])) /* synthesis GSR="ENABLED", syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=8, LSE_RCOL=72, LSE_LLINE=17, LSE_RLINE=17 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/count8.v(45[13:79])
    defparam FF_2.GSR = "ENABLED";
    FD1P3DX FF_1 (.D(idataout6), .SP(CLKEN_c), .CK(CLK), .CD(CLEAR_c), 
            .Q(DATAA[6])) /* synthesis GSR="ENABLED", syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=8, LSE_RCOL=72, LSE_LLINE=17, LSE_RLINE=17 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/count8.v(48[13:79])
    defparam FF_1.GSR = "ENABLED";
    FD1P3DX FF_0 (.D(idataout7), .SP(CLKEN_c), .CK(CLK), .CD(CLEAR_c), 
            .Q(DATAA[7])) /* synthesis GSR="ENABLED", syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=8, LSE_RCOL=72, LSE_LLINE=17, LSE_RLINE=17 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/count8.v(51[13:79])
    defparam FF_0.GSR = "ENABLED";
    FADD2B cnt_cia (.A0(GND_net), .A1(VCC_net), .B0(GND_net), .B1(VCC_net), 
           .CI(GND_net), .COUT(cnt_ci)) /* synthesis syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=8, LSE_RCOL=72, LSE_LLINE=17, LSE_RLINE=17 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(17[8:72])
    CU2 cnt_0 (.CI(cnt_ci), .PC0(DATAA[0]), .PC1(DATAA[1]), .CO(co0), 
        .NC0(idataout0), .NC1(idataout1)) /* synthesis syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=8, LSE_RCOL=72, LSE_LLINE=17, LSE_RLINE=17 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(17[8:72])
    CU2 cnt_1 (.CI(co0), .PC0(DATAA[2]), .PC1(DATAA[3]), .CO(co1), .NC0(idataout2), 
        .NC1(idataout3)) /* synthesis syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=8, LSE_RCOL=72, LSE_LLINE=17, LSE_RLINE=17 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(17[8:72])
    CU2 cnt_2 (.CI(co1), .PC0(DATAA[4]), .PC1(DATAA[5]), .CO(co2), .NC0(idataout4), 
        .NC1(idataout5)) /* synthesis syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=8, LSE_RCOL=72, LSE_LLINE=17, LSE_RLINE=17 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(17[8:72])
    CU2 cnt_3 (.CI(co2), .PC0(DATAA[6]), .PC1(DATAA[7]), .NC0(idataout6), 
        .NC1(idataout7)) /* synthesis syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=8, LSE_RCOL=72, LSE_LLINE=17, LSE_RLINE=17 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/impl1/schema.v(17[8:72])
    FD1P3DX FF_7 (.D(idataout0), .SP(CLKEN_c), .CK(CLK), .CD(CLEAR_c), 
            .Q(DATAA[0])) /* synthesis GSR="ENABLED", syn_instantiated=1, LSE_LINE_FILE_ID=3, LSE_LCOL=8, LSE_RCOL=72, LSE_LLINE=17, LSE_RLINE=17 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex8/count8.v(30[13:79])
    defparam FF_7.GSR = "ENABLED";
    
endmodule
//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

