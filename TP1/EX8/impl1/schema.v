/* Verilog model created from schematic schema.sch -- Oct 13, 2023 11:26 */

module schema( CLEAR, CLKEN, DATAB, SIGNALLED, STDBY );
 input CLEAR;
 input CLKEN;
 input [7:0] DATAB;
output [7:0] SIGNALLED;
 input STDBY;
  wire [7:0] DATAA;
wire MLI;
wire CLK;



defparam I1.NOM_FREQ="2.08";
OSCH I1 ( .OSC(CLK), .STDBY(STDBY) );
COUNT8 I2 ( .Aclr(CLEAR), .Clk_En(CLKEN), .Clock(CLK), .Q(DATAA[7:0]) );
COMP8 I3 ( .ALTB(MLI), .DataA(DATAA[7:0]), .DataB(DATAB[7:0]) );
ENTITYGRADATEUR I12 ( .MLI(MLI), .SIGNALLED(SIGNALLED[7:0]) );

endmodule // schema
