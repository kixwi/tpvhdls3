library ieee;
LIBRARY MACHXO2;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
USE MACHXO2.ALL;

ENTITY DETECTMOORE IS
	PORT(RST, INPUT, STDBYINP : IN STD_LOGIC;
	S, CLKOUT : OUT STD_LOGIC);
END DETECTMOORE;

ARCHITECTURE TOP OF DETECTMOORE IS

COMPONENT OSCH
GENERIC (NOM_FREQ : STRING);
PORT(
	STDBY : IN STD_LOGIC;
	OSC : OUT STD_LOGIC;
	SEDSTDBY :  OUT STD_LOGIC);
END COMPONENT;

TYPE TYPE_ETAT IS (E0, E1, E2);
SIGNAL X : TYPE_ETAT;

SIGNAL SED : STD_LOGIC;
SIGNAL CLKINT : STD_LOGIC;

BEGIN
	U1 : OSCH GENERIC MAP ("2.08") PORT MAP(STDBY => STDBYINP, OSC => CLKINT, SEDSTDBY => SED);
	
	PROCESS(CLKINT, RST)
	BEGIN
		IF(CLKINT'EVENT AND CLKINT='1') THEN
			IF(RST='0') THEN X <= E0;
			ELSE 
				CASE X IS
					WHEN E0 => 
						IF(INPUT='0') THEN X <= E0;
						ELSE X <= E1;
						END IF;
					WHEN E1 => 
						IF(INPUT='0') THEN X <= E2;
						ELSE X <= E1;
						END IF;
					WHEN E2 => 
						IF(INPUT='0') THEN X <= E0;
						ELSE X <= E1;
						END IF;
				END CASE;
			END IF;
		END IF;
	END PROCESS;
S <= '1' WHEN X=E2 ELSE '0';
CLKOUT <= CLKINT;
END TOP;