// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Sat Oct 07 08:52:10 2023
//
// Verilog Description of module DETECTMOORE
//

module DETECTMOORE (RST, INPUT, STDBYINP, S, CLKOUT);   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/moore.vhd(8[8:19])
    input RST;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/moore.vhd(9[7:10])
    input INPUT;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/moore.vhd(9[12:17])
    input STDBYINP;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/moore.vhd(9[19:27])
    output S;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/moore.vhd(10[2:3])
    output CLKOUT;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/moore.vhd(10[5:11])
    
    wire CLKOUT_c /* synthesis is_clock=1, SET_AS_NETWORK=CLKOUT_c */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/moore.vhd(10[5:11])
    
    wire GND_net, VCC_net, n84, RST_c, INPUT_c, STDBYINP_c, S_c, 
        n86, n48, n49, n88, n66;
    
    VHI i2 (.Z(VCC_net));
    IB RST_pad (.I(RST), .O(RST_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/moore.vhd(9[7:10])
    OB S_pad (.I(S_c), .O(S));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/moore.vhd(10[2:3])
    LUT4 i55_3_lut (.A(INPUT_c), .B(n49), .C(S_c), .Z(n86)) /* synthesis lut_function=(!(A+!(B+(C)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/moore.vhd(37[5] 50[14])
    defparam i55_3_lut.init = 16'h5454;
    LUT4 i54_4_lut (.A(INPUT_c), .B(S_c), .C(n49), .D(n48), .Z(n84)) /* synthesis lut_function=(A (B+(C+(D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/moore.vhd(37[5] 50[14])
    defparam i54_4_lut.init = 16'haaa8;
    VLO i68 (.Z(GND_net));
    OSCH U1 (.STDBY(STDBYINP_c), .OSC(CLKOUT_c)) /* synthesis syn_instantiated=1 */ ;
    defparam U1.NOM_FREQ = "2.08";
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    FD1S3IX X_FSM_i2 (.D(n84), .CK(CLKOUT_c), .CD(n88), .Q(n48));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/moore.vhd(37[5] 50[14])
    defparam X_FSM_i2.GSR = "ENABLED";
    FD1S3JX X_FSM_i1 (.D(n86), .CK(CLKOUT_c), .PD(n88), .Q(n49));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/moore.vhd(37[5] 50[14])
    defparam X_FSM_i1.GSR = "ENABLED";
    IB STDBYINP_pad (.I(STDBYINP), .O(STDBYINP_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/moore.vhd(9[19:27])
    IB INPUT_pad (.I(INPUT), .O(INPUT_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/moore.vhd(9[12:17])
    TSALL TSALL_INST (.TSALL(GND_net));
    FD1S3IX X_FSM_i3 (.D(n66), .CK(CLKOUT_c), .CD(n88), .Q(S_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/moore.vhd(37[5] 50[14])
    defparam X_FSM_i3.GSR = "ENABLED";
    OB CLKOUT_pad (.I(CLKOUT_c), .O(CLKOUT));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/moore.vhd(10[5:11])
    LUT4 i42_2_lut (.A(INPUT_c), .B(n48), .Z(n66)) /* synthesis lut_function=(!(A+!(B))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/moore.vhd(37[5] 50[14])
    defparam i42_2_lut.init = 16'h4444;
    LUT4 i57_1_lut (.A(RST_c), .Z(n88)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/moore.vhd(9[7:10])
    defparam i57_1_lut.init = 16'h5555;
    GSR GSR_INST (.GSR(VCC_net));
    
endmodule
//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

