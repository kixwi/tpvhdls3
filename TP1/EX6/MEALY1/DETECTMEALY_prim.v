// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Sat Oct 07 09:38:20 2023
//
// Verilog Description of module DETECTMEALY
//

module DETECTMEALY (RST, INPUT, STDBYINP, S, CLKOUT);   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/mealy.vhd(8[8:19])
    input RST;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/mealy.vhd(9[7:10])
    input INPUT;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/mealy.vhd(9[12:17])
    input STDBYINP;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/mealy.vhd(9[19:27])
    output S;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/mealy.vhd(10[2:3])
    output CLKOUT;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/mealy.vhd(10[5:11])
    
    wire CLKOUT_c /* synthesis is_clock=1, SET_AS_NETWORK=CLKOUT_c */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/mealy.vhd(10[5:11])
    
    wire GND_net, RST_c, INPUT_c, STDBYINP_c, S_c, X, VCC_net, 
        n23;
    
    VHI i29 (.Z(VCC_net));
    OB S_pad (.I(S_c), .O(S));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/mealy.vhd(10[2:3])
    TSALL TSALL_INST (.TSALL(GND_net));
    VLO i35 (.Z(GND_net));
    OSCH U1 (.STDBY(STDBYINP_c), .OSC(CLKOUT_c)) /* synthesis syn_instantiated=1 */ ;
    defparam U1.NOM_FREQ = "2.08";
    OB CLKOUT_pad (.I(CLKOUT_c), .O(CLKOUT));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/mealy.vhd(10[5:11])
    IB RST_pad (.I(RST), .O(RST_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/mealy.vhd(9[7:10])
    IB INPUT_pad (.I(INPUT), .O(INPUT_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/mealy.vhd(9[12:17])
    IB STDBYINP_pad (.I(STDBYINP), .O(STDBYINP_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/mealy.vhd(9[19:27])
    GSR GSR_INST (.GSR(VCC_net));
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    FD1S3IX X_16 (.D(INPUT_c), .CK(CLKOUT_c), .CD(n23), .Q(X));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/mealy.vhd(34[3] 46[10])
    defparam X_16.GSR = "ENABLED";
    LUT4 X_I_0_2_lut (.A(X), .B(INPUT_c), .Z(S_c)) /* synthesis lut_function=(!((B)+!A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/mealy.vhd(48[16:34])
    defparam X_I_0_2_lut.init = 16'h2222;
    LUT4 i30_1_lut (.A(RST_c), .Z(n23)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl s3/tp1/ex6/mealy.vhd(9[7:10])
    defparam i30_1_lut.init = 16'h5555;
    
endmodule
//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

