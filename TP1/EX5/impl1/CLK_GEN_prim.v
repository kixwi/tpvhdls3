// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Thu Oct 05 09:27:53 2023
//
// Verilog Description of module CLK_GEN
//

module CLK_GEN (MCLK, EN_500MS);   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(6[8:15])
    input MCLK;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(7[6:10])
    output EN_500MS;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(8[2:10])
    
    wire MCLK_c /* synthesis SET_AS_NETWORK=MCLK_c, is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(7[6:10])
    
    wire GND_net, VCC_net, EN_500MS_c;
    wire [19:0]CPT;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(12[8:11])
    
    wire n111, n20, n19, n15, n14, n280, n232, n278, n236, 
        n235, n234, n233, n239, n238, n237, n272, n241, n240, 
        n86, n87, n88, n89, n90, n91, n92, n93, n94, n95, 
        n96, n97, n98, n99, n100, n101, n102, n103, n104, 
        n105, n306, n288;
    
    VHI i2 (.Z(VCC_net));
    OB EN_500MS_pad (.I(EN_500MS_c), .O(EN_500MS));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(8[2:10])
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    FD1S3IX CPT_15__i0 (.D(n105), .CK(MCLK_c), .CD(n111), .Q(CPT[0])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i0.GSR = "ENABLED";
    CCU2D CPT_15_add_4_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[0]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .COUT(n232), 
          .S1(n105));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15_add_4_1.INIT0 = 16'hF000;
    defparam CPT_15_add_4_1.INIT1 = 16'h0555;
    defparam CPT_15_add_4_1.INJECT1_0 = "NO";
    defparam CPT_15_add_4_1.INJECT1_1 = "NO";
    CCU2D CPT_15_add_4_21 (.A0(CPT[19]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(GND_net), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n241), 
          .S0(n86));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15_add_4_21.INIT0 = 16'hfaaa;
    defparam CPT_15_add_4_21.INIT1 = 16'h0000;
    defparam CPT_15_add_4_21.INJECT1_0 = "NO";
    defparam CPT_15_add_4_21.INJECT1_1 = "NO";
    CCU2D CPT_15_add_4_19 (.A0(CPT[17]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[18]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n240), 
          .COUT(n241), .S0(n88), .S1(n87));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15_add_4_19.INIT0 = 16'hfaaa;
    defparam CPT_15_add_4_19.INIT1 = 16'hfaaa;
    defparam CPT_15_add_4_19.INJECT1_0 = "NO";
    defparam CPT_15_add_4_19.INJECT1_1 = "NO";
    VLO i1 (.Z(GND_net));
    LUT4 i203_4_lut (.A(CPT[16]), .B(CPT[8]), .C(CPT[15]), .D(n306), 
         .Z(n288)) /* synthesis lut_function=(!(A+(B+(C+(D))))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(23[22:27])
    defparam i203_4_lut.init = 16'h0001;
    LUT4 i8_4_lut (.A(CPT[1]), .B(CPT[19]), .C(CPT[10]), .D(CPT[13]), 
         .Z(n20)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(23[22:27])
    defparam i8_4_lut.init = 16'hfffe;
    TSALL TSALL_INST (.TSALL(GND_net));
    CCU2D CPT_15_add_4_15 (.A0(CPT[13]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[14]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n238), 
          .COUT(n239), .S0(n92), .S1(n91));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15_add_4_15.INIT0 = 16'hfaaa;
    defparam CPT_15_add_4_15.INIT1 = 16'hfaaa;
    defparam CPT_15_add_4_15.INJECT1_0 = "NO";
    defparam CPT_15_add_4_15.INJECT1_1 = "NO";
    LUT4 i193_4_lut (.A(CPT[14]), .B(CPT[15]), .C(CPT[17]), .D(CPT[10]), 
         .Z(n278)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i193_4_lut.init = 16'h8000;
    FD1S3IX CPT_15__i19 (.D(n86), .CK(MCLK_c), .CD(n111), .Q(CPT[19])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i19.GSR = "ENABLED";
    IB MCLK_pad (.I(MCLK), .O(MCLK_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(7[6:10])
    GSR GSR_INST (.GSR(VCC_net));
    LUT4 i7_4_lut (.A(CPT[18]), .B(CPT[14]), .C(CPT[0]), .D(CPT[17]), 
         .Z(n19)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(23[22:27])
    defparam i7_4_lut.init = 16'hfffe;
    LUT4 i187_3_lut (.A(CPT[16]), .B(CPT[13]), .C(CPT[19]), .Z(n272)) /* synthesis lut_function=(A (B (C))) */ ;
    defparam i187_3_lut.init = 16'h8080;
    CCU2D CPT_15_add_4_9 (.A0(CPT[7]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[8]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n235), 
          .COUT(n236), .S0(n98), .S1(n97));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15_add_4_9.INIT0 = 16'hfaaa;
    defparam CPT_15_add_4_9.INIT1 = 16'hfaaa;
    defparam CPT_15_add_4_9.INJECT1_0 = "NO";
    defparam CPT_15_add_4_9.INJECT1_1 = "NO";
    CCU2D CPT_15_add_4_7 (.A0(CPT[5]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[6]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n234), 
          .COUT(n235), .S0(n100), .S1(n99));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15_add_4_7.INIT0 = 16'hfaaa;
    defparam CPT_15_add_4_7.INIT1 = 16'hfaaa;
    defparam CPT_15_add_4_7.INJECT1_0 = "NO";
    defparam CPT_15_add_4_7.INJECT1_1 = "NO";
    CCU2D CPT_15_add_4_5 (.A0(CPT[3]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[4]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n233), 
          .COUT(n234), .S0(n102), .S1(n101));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15_add_4_5.INIT0 = 16'hfaaa;
    defparam CPT_15_add_4_5.INIT1 = 16'hfaaa;
    defparam CPT_15_add_4_5.INJECT1_0 = "NO";
    defparam CPT_15_add_4_5.INJECT1_1 = "NO";
    CCU2D CPT_15_add_4_3 (.A0(CPT[1]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[2]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n232), 
          .COUT(n233), .S0(n104), .S1(n103));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15_add_4_3.INIT0 = 16'hfaaa;
    defparam CPT_15_add_4_3.INIT1 = 16'hfaaa;
    defparam CPT_15_add_4_3.INJECT1_0 = "NO";
    defparam CPT_15_add_4_3.INJECT1_1 = "NO";
    CCU2D CPT_15_add_4_13 (.A0(CPT[11]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[12]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n237), 
          .COUT(n238), .S0(n94), .S1(n93));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15_add_4_13.INIT0 = 16'hfaaa;
    defparam CPT_15_add_4_13.INIT1 = 16'hfaaa;
    defparam CPT_15_add_4_13.INJECT1_0 = "NO";
    defparam CPT_15_add_4_13.INJECT1_1 = "NO";
    CCU2D CPT_15_add_4_11 (.A0(CPT[9]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[10]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n236), 
          .COUT(n237), .S0(n96), .S1(n95));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15_add_4_11.INIT0 = 16'hfaaa;
    defparam CPT_15_add_4_11.INIT1 = 16'hfaaa;
    defparam CPT_15_add_4_11.INJECT1_0 = "NO";
    defparam CPT_15_add_4_11.INJECT1_1 = "NO";
    CCU2D CPT_15_add_4_17 (.A0(CPT[15]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[16]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n239), 
          .COUT(n240), .S0(n90), .S1(n89));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15_add_4_17.INIT0 = 16'hfaaa;
    defparam CPT_15_add_4_17.INIT1 = 16'hfaaa;
    defparam CPT_15_add_4_17.INJECT1_0 = "NO";
    defparam CPT_15_add_4_17.INJECT1_1 = "NO";
    LUT4 i195_4_lut (.A(CPT[18]), .B(CPT[1]), .C(CPT[8]), .D(CPT[0]), 
         .Z(n280)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i195_4_lut.init = 16'h8000;
    FD1S3IX CPT_15__i18 (.D(n87), .CK(MCLK_c), .CD(n111), .Q(CPT[18])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i18.GSR = "ENABLED";
    FD1S3IX CPT_15__i17 (.D(n88), .CK(MCLK_c), .CD(n111), .Q(CPT[17])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i17.GSR = "ENABLED";
    FD1S3IX CPT_15__i16 (.D(n89), .CK(MCLK_c), .CD(n111), .Q(CPT[16])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i16.GSR = "ENABLED";
    FD1S3IX CPT_15__i15 (.D(n90), .CK(MCLK_c), .CD(n111), .Q(CPT[15])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i15.GSR = "ENABLED";
    FD1S3IX CPT_15__i14 (.D(n91), .CK(MCLK_c), .CD(n111), .Q(CPT[14])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i14.GSR = "ENABLED";
    FD1S3IX CPT_15__i13 (.D(n92), .CK(MCLK_c), .CD(n111), .Q(CPT[13])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i13.GSR = "ENABLED";
    FD1S3IX CPT_15__i12 (.D(n93), .CK(MCLK_c), .CD(n111), .Q(CPT[12])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i12.GSR = "ENABLED";
    FD1S3IX CPT_15__i11 (.D(n94), .CK(MCLK_c), .CD(n111), .Q(CPT[11])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i11.GSR = "ENABLED";
    FD1S3IX CPT_15__i10 (.D(n95), .CK(MCLK_c), .CD(n111), .Q(CPT[10])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i10.GSR = "ENABLED";
    FD1S3IX CPT_15__i9 (.D(n96), .CK(MCLK_c), .CD(n111), .Q(CPT[9])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i9.GSR = "ENABLED";
    FD1S3IX CPT_15__i8 (.D(n97), .CK(MCLK_c), .CD(n111), .Q(CPT[8])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i8.GSR = "ENABLED";
    FD1S3IX CPT_15__i7 (.D(n98), .CK(MCLK_c), .CD(n111), .Q(CPT[7])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i7.GSR = "ENABLED";
    FD1S3IX CPT_15__i6 (.D(n99), .CK(MCLK_c), .CD(n111), .Q(CPT[6])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i6.GSR = "ENABLED";
    FD1S3IX CPT_15__i5 (.D(n100), .CK(MCLK_c), .CD(n111), .Q(CPT[5])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i5.GSR = "ENABLED";
    FD1S3IX CPT_15__i4 (.D(n101), .CK(MCLK_c), .CD(n111), .Q(CPT[4])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i4.GSR = "ENABLED";
    FD1S3IX CPT_15__i3 (.D(n102), .CK(MCLK_c), .CD(n111), .Q(CPT[3])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i3.GSR = "ENABLED";
    FD1S3IX CPT_15__i2 (.D(n103), .CK(MCLK_c), .CD(n111), .Q(CPT[2])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i2.GSR = "ENABLED";
    FD1S3IX CPT_15__i1 (.D(n104), .CK(MCLK_c), .CD(n111), .Q(CPT[1])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_15__i1.GSR = "ENABLED";
    LUT4 i8_4_lut_rep_1 (.A(n15), .B(CPT[6]), .C(n14), .D(CPT[5]), .Z(n306)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(23[22:27])
    defparam i8_4_lut_rep_1.init = 16'hfffe;
    LUT4 i205_3_lut (.A(n288), .B(n19), .C(n20), .Z(EN_500MS_c)) /* synthesis lut_function=(!((B+(C))+!A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(23[22:27])
    defparam i205_3_lut.init = 16'h0202;
    LUT4 i6_4_lut (.A(CPT[7]), .B(CPT[9]), .C(CPT[11]), .D(CPT[4]), 
         .Z(n15)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(23[22:27])
    defparam i6_4_lut.init = 16'hfffe;
    LUT4 i5_3_lut (.A(CPT[2]), .B(CPT[12]), .C(CPT[3]), .Z(n14)) /* synthesis lut_function=(A+(B+(C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(23[22:27])
    defparam i5_3_lut.init = 16'hfefe;
    LUT4 i202_4_lut_4_lut (.A(n306), .B(n280), .C(n272), .D(n278), .Z(n111)) /* synthesis lut_function=(!(A+!(B (C (D))))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(18[6:17])
    defparam i202_4_lut_4_lut.init = 16'h4000;
    
endmodule
//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

