// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Thu Oct 05 12:08:05 2023
//
// Verilog Description of module COMPTEUR0TO99
//

module COMPTEUR0TO99 (STDBY, RAZ, SEDSTBY, UNITES, DIZAINES);   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(8[8:21])
    input STDBY;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(10[2:7])
    input RAZ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(10[9:12])
    output SEDSTBY;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(11[2:9])
    output [3:0]UNITES;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(12[2:8])
    output [3:0]DIZAINES;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(12[10:18])
    
    wire CLKINT /* synthesis SET_AS_NETWORK=CLKINT, is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(30[8:14])
    wire S0INT /* synthesis SET_AS_NETWORK=S0INT, is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(31[8:13])
    
    wire GND_net, VCC_net, STDBY_c, RAZ_c, UNITES_c_3, UNITES_c_2, 
        UNITES_c_1, UNITES_c_0, DIZAINES_c_3, DIZAINES_c_2, DIZAINES_c_1, 
        DIZAINES_c_0, UNITES_3__N_15, DIZAINES_3__N_20, n32, n33, 
        n34, n25, n24, n23, n22, n440, n127;
    
    VHI i434 (.Z(VCC_net));
    OB UNITES_pad_2 (.I(UNITES_c_2), .O(UNITES[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(12[2:8])
    OB UNITES_pad_3 (.I(UNITES_c_3), .O(UNITES[3]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(12[2:8])
    LUT4 i394_2_lut (.A(DIZAINES_c_3), .B(DIZAINES_c_0), .Z(n440)) /* synthesis lut_function=(A (B)) */ ;
    defparam i394_2_lut.init = 16'h8888;
    OSCH M (.STDBY(STDBY_c), .OSC(CLKINT)) /* synthesis syn_instantiated=1 */ ;
    defparam M.NOM_FREQ = "2.08";
    LUT4 i1_2_lut (.A(DIZAINES_c_0), .B(UNITES_3__N_15), .Z(n127)) /* synthesis lut_function=(!(A (B)+!A !(B))) */ ;
    defparam i1_2_lut.init = 16'h6666;
    OB SEDSTBY_pad (.I(GND_net), .O(SEDSTBY));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(11[2:9])
    FD1S3IX DIZAINESINT__i1 (.D(n127), .CK(S0INT), .CD(DIZAINES_3__N_20), 
            .Q(DIZAINES_c_0));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(54[3] 59[10])
    defparam DIZAINESINT__i1.GSR = "ENABLED";
    LUT4 i60_3_lut_4_lut (.A(DIZAINES_c_1), .B(DIZAINES_c_0), .C(DIZAINES_c_2), 
         .D(DIZAINES_c_3), .Z(n32)) /* synthesis lut_function=(!(A (B (C (D)+!C !(D))+!B !(D))+!A !(D))) */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam i60_3_lut_4_lut.init = 16'h7f80;
    LUT4 i334_1_lut (.A(UNITES_c_0), .Z(n25)) /* synthesis lut_function=(!(A)) */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam i334_1_lut.init = 16'h5555;
    LUT4 i420_4_lut (.A(DIZAINES_c_2), .B(UNITES_3__N_15), .C(DIZAINES_c_1), 
         .D(n440), .Z(DIZAINES_3__N_20)) /* synthesis lut_function=(!(A+((C+!(D))+!B))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(56[7:23])
    defparam i420_4_lut.init = 16'h0400;
    FD1S3IX UNITESINT_32__i1 (.D(n25), .CK(S0INT), .CD(UNITES_3__N_15), 
            .Q(UNITES_c_0));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam UNITESINT_32__i1.GSR = "ENABLED";
    LUT4 i343_2_lut_3_lut (.A(UNITES_c_1), .B(UNITES_c_0), .C(UNITES_c_2), 
         .Z(n23)) /* synthesis lut_function=(!(A (B (C)+!B !(C))+!A !(C))) */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam i343_2_lut_3_lut.init = 16'h7878;
    GSR GSR_INST (.GSR(RAZ_c));
    LUT4 i415_4_lut (.A(UNITES_c_0), .B(UNITES_c_1), .C(UNITES_c_2), .D(UNITES_c_3), 
         .Z(UNITES_3__N_15)) /* synthesis lut_function=(!((B+(C+!(D)))+!A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(45[7:23])
    defparam i415_4_lut.init = 16'h0200;
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    CLK_GEN N (.GND_net(GND_net), .CLKINT(CLKINT), .S0INT(S0INT));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(37[5:12])
    TSALL TSALL_INST (.TSALL(GND_net));
    VLO i1 (.Z(GND_net));
    LUT4 i53_2_lut_3_lut (.A(DIZAINES_c_1), .B(DIZAINES_c_0), .C(DIZAINES_c_2), 
         .Z(n33)) /* synthesis lut_function=(!(A (B (C)+!B !(C))+!A !(C))) */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam i53_2_lut_3_lut.init = 16'h7878;
    LUT4 i336_2_lut (.A(UNITES_c_1), .B(UNITES_c_0), .Z(n24)) /* synthesis lut_function=(!(A (B)+!A !(B))) */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam i336_2_lut.init = 16'h6666;
    LUT4 i350_3_lut_4_lut (.A(UNITES_c_1), .B(UNITES_c_0), .C(UNITES_c_2), 
         .D(UNITES_c_3), .Z(n22)) /* synthesis lut_function=(!(A (B (C (D)+!C !(D))+!B !(D))+!A !(D))) */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam i350_3_lut_4_lut.init = 16'h7f80;
    LUT4 i46_2_lut (.A(DIZAINES_c_1), .B(DIZAINES_c_0), .Z(n34)) /* synthesis lut_function=(!(A (B)+!A !(B))) */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam i46_2_lut.init = 16'h6666;
    FD1S3IX UNITESINT_32__i4 (.D(n22), .CK(S0INT), .CD(UNITES_3__N_15), 
            .Q(UNITES_c_3));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam UNITESINT_32__i4.GSR = "ENABLED";
    FD1S3IX UNITESINT_32__i3 (.D(n23), .CK(S0INT), .CD(UNITES_3__N_15), 
            .Q(UNITES_c_2));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam UNITESINT_32__i3.GSR = "ENABLED";
    FD1S3IX UNITESINT_32__i2 (.D(n24), .CK(S0INT), .CD(UNITES_3__N_15), 
            .Q(UNITES_c_1));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam UNITESINT_32__i2.GSR = "ENABLED";
    FD1P3IX DIZAINESINT__i4 (.D(n32), .SP(UNITES_3__N_15), .CD(DIZAINES_3__N_20), 
            .CK(S0INT), .Q(DIZAINES_c_3));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(54[3] 59[10])
    defparam DIZAINESINT__i4.GSR = "ENABLED";
    FD1P3IX DIZAINESINT__i3 (.D(n33), .SP(UNITES_3__N_15), .CD(DIZAINES_3__N_20), 
            .CK(S0INT), .Q(DIZAINES_c_2));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(54[3] 59[10])
    defparam DIZAINESINT__i3.GSR = "ENABLED";
    FD1P3IX DIZAINESINT__i2 (.D(n34), .SP(UNITES_3__N_15), .CD(DIZAINES_3__N_20), 
            .CK(S0INT), .Q(DIZAINES_c_1));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(54[3] 59[10])
    defparam DIZAINESINT__i2.GSR = "ENABLED";
    OB UNITES_pad_1 (.I(UNITES_c_1), .O(UNITES[1]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(12[2:8])
    OB UNITES_pad_0 (.I(UNITES_c_0), .O(UNITES[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(12[2:8])
    OB DIZAINES_pad_3 (.I(DIZAINES_c_3), .O(DIZAINES[3]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(12[10:18])
    OB DIZAINES_pad_2 (.I(DIZAINES_c_2), .O(DIZAINES[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(12[10:18])
    OB DIZAINES_pad_1 (.I(DIZAINES_c_1), .O(DIZAINES[1]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(12[10:18])
    OB DIZAINES_pad_0 (.I(DIZAINES_c_0), .O(DIZAINES[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(12[10:18])
    IB STDBY_pad (.I(STDBY), .O(STDBY_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(10[2:7])
    IB RAZ_pad (.I(RAZ), .O(RAZ_c));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(10[9:12])
    
endmodule
//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

//
// Verilog Description of module CLK_GEN
//

module CLK_GEN (GND_net, CLKINT, S0INT);
    input GND_net;
    input CLKINT;
    output S0INT;
    
    wire CLKINT /* synthesis SET_AS_NETWORK=CLKINT, is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(30[8:14])
    wire S0INT /* synthesis SET_AS_NETWORK=S0INT, is_clock=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/compteurdecimal0to99.vhd(31[8:13])
    wire [19:0]CPT;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(12[8:11])
    
    wire n448, n15, n14, n393;
    wire [19:0]n85;
    
    wire n394, n398, n399, n452, n486, n136, n395, n396, n397, 
        n442, n467, n19, n20, n400, n401, n402;
    
    LUT4 i402_4_lut (.A(CPT[18]), .B(CPT[1]), .C(CPT[8]), .D(CPT[0]), 
         .Z(n448)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i402_4_lut.init = 16'h8000;
    LUT4 i6_4_lut (.A(CPT[7]), .B(CPT[9]), .C(CPT[11]), .D(CPT[4]), 
         .Z(n15)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(18[6:17])
    defparam i6_4_lut.init = 16'hfffe;
    LUT4 i5_3_lut (.A(CPT[2]), .B(CPT[12]), .C(CPT[3]), .Z(n14)) /* synthesis lut_function=(A+(B+(C))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(18[6:17])
    defparam i5_3_lut.init = 16'hfefe;
    CCU2D CPT_33_add_4_3 (.A0(CPT[1]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[2]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n393), 
          .COUT(n394), .S0(n85[1]), .S1(n85[2]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33_add_4_3.INIT0 = 16'hfaaa;
    defparam CPT_33_add_4_3.INIT1 = 16'hfaaa;
    defparam CPT_33_add_4_3.INJECT1_0 = "NO";
    defparam CPT_33_add_4_3.INJECT1_1 = "NO";
    CCU2D CPT_33_add_4_13 (.A0(CPT[11]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[12]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n398), 
          .COUT(n399), .S0(n85[11]), .S1(n85[12]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33_add_4_13.INIT0 = 16'hfaaa;
    defparam CPT_33_add_4_13.INIT1 = 16'hfaaa;
    defparam CPT_33_add_4_13.INJECT1_0 = "NO";
    defparam CPT_33_add_4_13.INJECT1_1 = "NO";
    LUT4 i406_4_lut (.A(CPT[14]), .B(CPT[15]), .C(CPT[17]), .D(CPT[10]), 
         .Z(n452)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i406_4_lut.init = 16'h8000;
    LUT4 i8_4_lut_rep_2 (.A(n15), .B(CPT[6]), .C(n14), .D(CPT[5]), .Z(n486)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(18[6:17])
    defparam i8_4_lut_rep_2.init = 16'hfffe;
    FD1S3IX CPT_33__i0 (.D(n85[0]), .CK(CLKINT), .CD(n136), .Q(CPT[0])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i0.GSR = "DISABLED";
    FD1S3IX CPT_33__i19 (.D(n85[19]), .CK(CLKINT), .CD(n136), .Q(CPT[19])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i19.GSR = "DISABLED";
    CCU2D CPT_33_add_4_7 (.A0(CPT[5]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[6]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n395), 
          .COUT(n396), .S0(n85[5]), .S1(n85[6]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33_add_4_7.INIT0 = 16'hfaaa;
    defparam CPT_33_add_4_7.INIT1 = 16'hfaaa;
    defparam CPT_33_add_4_7.INJECT1_0 = "NO";
    defparam CPT_33_add_4_7.INJECT1_1 = "NO";
    CCU2D CPT_33_add_4_9 (.A0(CPT[7]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[8]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n396), 
          .COUT(n397), .S0(n85[7]), .S1(n85[8]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33_add_4_9.INIT0 = 16'hfaaa;
    defparam CPT_33_add_4_9.INIT1 = 16'hfaaa;
    defparam CPT_33_add_4_9.INJECT1_0 = "NO";
    defparam CPT_33_add_4_9.INJECT1_1 = "NO";
    LUT4 i396_3_lut (.A(CPT[16]), .B(CPT[13]), .C(CPT[19]), .Z(n442)) /* synthesis lut_function=(A (B (C))) */ ;
    defparam i396_3_lut.init = 16'h8080;
    LUT4 i424_3_lut (.A(n467), .B(n19), .C(n20), .Z(S0INT)) /* synthesis lut_function=(!((B+(C))+!A)) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(23[22:27])
    defparam i424_3_lut.init = 16'h0202;
    CCU2D CPT_33_add_4_17 (.A0(CPT[15]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[16]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n400), 
          .COUT(n401), .S0(n85[15]), .S1(n85[16]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33_add_4_17.INIT0 = 16'hfaaa;
    defparam CPT_33_add_4_17.INIT1 = 16'hfaaa;
    defparam CPT_33_add_4_17.INJECT1_0 = "NO";
    defparam CPT_33_add_4_17.INJECT1_1 = "NO";
    CCU2D CPT_33_add_4_15 (.A0(CPT[13]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[14]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n399), 
          .COUT(n400), .S0(n85[13]), .S1(n85[14]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33_add_4_15.INIT0 = 16'hfaaa;
    defparam CPT_33_add_4_15.INIT1 = 16'hfaaa;
    defparam CPT_33_add_4_15.INJECT1_0 = "NO";
    defparam CPT_33_add_4_15.INJECT1_1 = "NO";
    LUT4 i417_4_lut_4_lut (.A(n486), .B(n448), .C(n442), .D(n452), .Z(n136)) /* synthesis lut_function=(!(A+!(B (C (D))))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(18[6:17])
    defparam i417_4_lut_4_lut.init = 16'h4000;
    LUT4 i422_4_lut (.A(CPT[16]), .B(CPT[8]), .C(CPT[15]), .D(n486), 
         .Z(n467)) /* synthesis lut_function=(!(A+(B+(C+(D))))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(23[22:27])
    defparam i422_4_lut.init = 16'h0001;
    CCU2D CPT_33_add_4_11 (.A0(CPT[9]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[10]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n397), 
          .COUT(n398), .S0(n85[9]), .S1(n85[10]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33_add_4_11.INIT0 = 16'hfaaa;
    defparam CPT_33_add_4_11.INIT1 = 16'hfaaa;
    defparam CPT_33_add_4_11.INJECT1_0 = "NO";
    defparam CPT_33_add_4_11.INJECT1_1 = "NO";
    CCU2D CPT_33_add_4_5 (.A0(CPT[3]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[4]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n394), 
          .COUT(n395), .S0(n85[3]), .S1(n85[4]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33_add_4_5.INIT0 = 16'hfaaa;
    defparam CPT_33_add_4_5.INIT1 = 16'hfaaa;
    defparam CPT_33_add_4_5.INJECT1_0 = "NO";
    defparam CPT_33_add_4_5.INJECT1_1 = "NO";
    CCU2D CPT_33_add_4_21 (.A0(CPT[19]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(GND_net), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n402), 
          .S0(n85[19]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33_add_4_21.INIT0 = 16'hfaaa;
    defparam CPT_33_add_4_21.INIT1 = 16'h0000;
    defparam CPT_33_add_4_21.INJECT1_0 = "NO";
    defparam CPT_33_add_4_21.INJECT1_1 = "NO";
    CCU2D CPT_33_add_4_19 (.A0(CPT[17]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[18]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n401), 
          .COUT(n402), .S0(n85[17]), .S1(n85[18]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33_add_4_19.INIT0 = 16'hfaaa;
    defparam CPT_33_add_4_19.INIT1 = 16'hfaaa;
    defparam CPT_33_add_4_19.INJECT1_0 = "NO";
    defparam CPT_33_add_4_19.INJECT1_1 = "NO";
    LUT4 i7_4_lut (.A(CPT[18]), .B(CPT[14]), .C(CPT[0]), .D(CPT[17]), 
         .Z(n19)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(18[6:17])
    defparam i7_4_lut.init = 16'hfffe;
    FD1S3IX CPT_33__i18 (.D(n85[18]), .CK(CLKINT), .CD(n136), .Q(CPT[18])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i18.GSR = "DISABLED";
    FD1S3IX CPT_33__i17 (.D(n85[17]), .CK(CLKINT), .CD(n136), .Q(CPT[17])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i17.GSR = "DISABLED";
    FD1S3IX CPT_33__i16 (.D(n85[16]), .CK(CLKINT), .CD(n136), .Q(CPT[16])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i16.GSR = "DISABLED";
    FD1S3IX CPT_33__i15 (.D(n85[15]), .CK(CLKINT), .CD(n136), .Q(CPT[15])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i15.GSR = "DISABLED";
    FD1S3IX CPT_33__i14 (.D(n85[14]), .CK(CLKINT), .CD(n136), .Q(CPT[14])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i14.GSR = "DISABLED";
    FD1S3IX CPT_33__i13 (.D(n85[13]), .CK(CLKINT), .CD(n136), .Q(CPT[13])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i13.GSR = "DISABLED";
    FD1S3IX CPT_33__i12 (.D(n85[12]), .CK(CLKINT), .CD(n136), .Q(CPT[12])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i12.GSR = "DISABLED";
    FD1S3IX CPT_33__i11 (.D(n85[11]), .CK(CLKINT), .CD(n136), .Q(CPT[11])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i11.GSR = "DISABLED";
    FD1S3IX CPT_33__i10 (.D(n85[10]), .CK(CLKINT), .CD(n136), .Q(CPT[10])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i10.GSR = "DISABLED";
    FD1S3IX CPT_33__i9 (.D(n85[9]), .CK(CLKINT), .CD(n136), .Q(CPT[9])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i9.GSR = "DISABLED";
    FD1S3IX CPT_33__i8 (.D(n85[8]), .CK(CLKINT), .CD(n136), .Q(CPT[8])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i8.GSR = "DISABLED";
    FD1S3IX CPT_33__i7 (.D(n85[7]), .CK(CLKINT), .CD(n136), .Q(CPT[7])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i7.GSR = "DISABLED";
    FD1S3IX CPT_33__i6 (.D(n85[6]), .CK(CLKINT), .CD(n136), .Q(CPT[6])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i6.GSR = "DISABLED";
    FD1S3IX CPT_33__i5 (.D(n85[5]), .CK(CLKINT), .CD(n136), .Q(CPT[5])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i5.GSR = "DISABLED";
    FD1S3IX CPT_33__i4 (.D(n85[4]), .CK(CLKINT), .CD(n136), .Q(CPT[4])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i4.GSR = "DISABLED";
    FD1S3IX CPT_33__i3 (.D(n85[3]), .CK(CLKINT), .CD(n136), .Q(CPT[3])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i3.GSR = "DISABLED";
    FD1S3IX CPT_33__i2 (.D(n85[2]), .CK(CLKINT), .CD(n136), .Q(CPT[2])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i2.GSR = "DISABLED";
    FD1S3IX CPT_33__i1 (.D(n85[1]), .CK(CLKINT), .CD(n136), .Q(CPT[1])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33__i1.GSR = "DISABLED";
    CCU2D CPT_33_add_4_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(CPT[0]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .COUT(n393), 
          .S1(n85[0]));   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(19[15:18])
    defparam CPT_33_add_4_1.INIT0 = 16'hF000;
    defparam CPT_33_add_4_1.INIT1 = 16'h0555;
    defparam CPT_33_add_4_1.INJECT1_0 = "NO";
    defparam CPT_33_add_4_1.INJECT1_1 = "NO";
    LUT4 i8_4_lut (.A(CPT[1]), .B(CPT[19]), .C(CPT[10]), .D(CPT[13]), 
         .Z(n20)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/users/user.desktop-9hj28hq/documents/cours/l2 geii/s3/vhdl/tp1/ex5/clk_gen.vhd(18[6:17])
    defparam i8_4_lut.init = 16'hfffe;
    
endmodule
//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

