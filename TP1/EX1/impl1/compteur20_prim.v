// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Mon Sep 18 08:37:35 2023
//
// Verilog Description of module compteur20
//

module compteur20 (clock, reset, Q);   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(6[8:18])
    input clock;   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(9[2:7])
    input reset;   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(10[2:7])
    output [19:0]Q;   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    
    wire clock_c /* synthesis SET_AS_NETWORK=clock_c, is_clock=1 */ ;   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(9[2:7])
    
    wire GND_net, VCC_net, reset_c, Q_c_19, Q_c_18, Q_c_17, Q_c_16, 
        Q_c_15, Q_c_14, Q_c_13, Q_c_12, Q_c_11, Q_c_10, Q_c_9, 
        Q_c_8, Q_c_7, Q_c_6, Q_c_5, Q_c_4, Q_c_3, Q_c_2, Q_c_1, 
        Q_c_0, n93, n86, n87, n88, n89, n90, n91, n92, n93_adj_1, 
        n94_adj_2, n95_adj_3, n96_adj_4, n97_adj_5, n98_adj_6, n99_adj_7, 
        n100_adj_8, n101_adj_9, n102_adj_10, n103_adj_11, n104, n105, 
        n103, n102, n101, n100, n99, n98, n97, n96, n95, n94;
    
    VHI i2 (.Z(VCC_net));
    OB Q_pad_19 (.I(Q_c_19), .O(Q[19]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    OB Q_pad_18 (.I(Q_c_18), .O(Q[18]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    FD1S3IX Qint_13__i20 (.D(n86), .CK(clock_c), .CD(n93), .Q(Q_c_19)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i20.GSR = "ENABLED";
    GSR GSR_INST (.GSR(VCC_net));
    FD1S3IX Qint_13__i1 (.D(n105), .CK(clock_c), .CD(n93), .Q(Q_c_0)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i1.GSR = "ENABLED";
    TSALL TSALL_INST (.TSALL(GND_net));
    FD1S3IX Qint_13__i19 (.D(n87), .CK(clock_c), .CD(n93), .Q(Q_c_18)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i19.GSR = "ENABLED";
    FD1S3IX Qint_13__i18 (.D(n88), .CK(clock_c), .CD(n93), .Q(Q_c_17)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i18.GSR = "ENABLED";
    FD1S3IX Qint_13__i17 (.D(n89), .CK(clock_c), .CD(n93), .Q(Q_c_16)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i17.GSR = "ENABLED";
    FD1S3IX Qint_13__i16 (.D(n90), .CK(clock_c), .CD(n93), .Q(Q_c_15)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i16.GSR = "ENABLED";
    FD1S3IX Qint_13__i15 (.D(n91), .CK(clock_c), .CD(n93), .Q(Q_c_14)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i15.GSR = "ENABLED";
    FD1S3IX Qint_13__i14 (.D(n92), .CK(clock_c), .CD(n93), .Q(Q_c_13)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i14.GSR = "ENABLED";
    FD1S3IX Qint_13__i13 (.D(n93_adj_1), .CK(clock_c), .CD(n93), .Q(Q_c_12)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i13.GSR = "ENABLED";
    FD1S3IX Qint_13__i12 (.D(n94_adj_2), .CK(clock_c), .CD(n93), .Q(Q_c_11)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i12.GSR = "ENABLED";
    FD1S3IX Qint_13__i11 (.D(n95_adj_3), .CK(clock_c), .CD(n93), .Q(Q_c_10)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i11.GSR = "ENABLED";
    FD1S3IX Qint_13__i10 (.D(n96_adj_4), .CK(clock_c), .CD(n93), .Q(Q_c_9)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i10.GSR = "ENABLED";
    FD1S3IX Qint_13__i9 (.D(n97_adj_5), .CK(clock_c), .CD(n93), .Q(Q_c_8)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i9.GSR = "ENABLED";
    FD1S3IX Qint_13__i8 (.D(n98_adj_6), .CK(clock_c), .CD(n93), .Q(Q_c_7)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i8.GSR = "ENABLED";
    FD1S3IX Qint_13__i7 (.D(n99_adj_7), .CK(clock_c), .CD(n93), .Q(Q_c_6)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i7.GSR = "ENABLED";
    FD1S3IX Qint_13__i6 (.D(n100_adj_8), .CK(clock_c), .CD(n93), .Q(Q_c_5)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i6.GSR = "ENABLED";
    FD1S3IX Qint_13__i5 (.D(n101_adj_9), .CK(clock_c), .CD(n93), .Q(Q_c_4)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i5.GSR = "ENABLED";
    FD1S3IX Qint_13__i4 (.D(n102_adj_10), .CK(clock_c), .CD(n93), .Q(Q_c_3)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i4.GSR = "ENABLED";
    FD1S3IX Qint_13__i3 (.D(n103_adj_11), .CK(clock_c), .CD(n93), .Q(Q_c_2)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i3.GSR = "ENABLED";
    FD1S3IX Qint_13__i2 (.D(n104), .CK(clock_c), .CD(n93), .Q(Q_c_1)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13__i2.GSR = "ENABLED";
    LUT4 i15_1_lut (.A(reset_c), .Z(n93)) /* synthesis lut_function=(!(A)) */ ;   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(10[2:7])
    defparam i15_1_lut.init = 16'h5555;
    VLO i1 (.Z(GND_net));
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    CCU2D Qint_13_add_4_21 (.A0(Q_c_19), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(GND_net), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n103), 
          .S0(n86));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13_add_4_21.INIT0 = 16'hfaaa;
    defparam Qint_13_add_4_21.INIT1 = 16'h0000;
    defparam Qint_13_add_4_21.INJECT1_0 = "NO";
    defparam Qint_13_add_4_21.INJECT1_1 = "NO";
    CCU2D Qint_13_add_4_19 (.A0(Q_c_17), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_18), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n102), 
          .COUT(n103), .S0(n88), .S1(n87));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13_add_4_19.INIT0 = 16'hfaaa;
    defparam Qint_13_add_4_19.INIT1 = 16'hfaaa;
    defparam Qint_13_add_4_19.INJECT1_0 = "NO";
    defparam Qint_13_add_4_19.INJECT1_1 = "NO";
    CCU2D Qint_13_add_4_17 (.A0(Q_c_15), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_16), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n101), 
          .COUT(n102), .S0(n90), .S1(n89));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13_add_4_17.INIT0 = 16'hfaaa;
    defparam Qint_13_add_4_17.INIT1 = 16'hfaaa;
    defparam Qint_13_add_4_17.INJECT1_0 = "NO";
    defparam Qint_13_add_4_17.INJECT1_1 = "NO";
    CCU2D Qint_13_add_4_15 (.A0(Q_c_13), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_14), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n100), 
          .COUT(n101), .S0(n92), .S1(n91));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13_add_4_15.INIT0 = 16'hfaaa;
    defparam Qint_13_add_4_15.INIT1 = 16'hfaaa;
    defparam Qint_13_add_4_15.INJECT1_0 = "NO";
    defparam Qint_13_add_4_15.INJECT1_1 = "NO";
    CCU2D Qint_13_add_4_13 (.A0(Q_c_11), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_12), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n99), 
          .COUT(n100), .S0(n94_adj_2), .S1(n93_adj_1));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13_add_4_13.INIT0 = 16'hfaaa;
    defparam Qint_13_add_4_13.INIT1 = 16'hfaaa;
    defparam Qint_13_add_4_13.INJECT1_0 = "NO";
    defparam Qint_13_add_4_13.INJECT1_1 = "NO";
    CCU2D Qint_13_add_4_11 (.A0(Q_c_9), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_10), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n98), 
          .COUT(n99), .S0(n96_adj_4), .S1(n95_adj_3));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13_add_4_11.INIT0 = 16'hfaaa;
    defparam Qint_13_add_4_11.INIT1 = 16'hfaaa;
    defparam Qint_13_add_4_11.INJECT1_0 = "NO";
    defparam Qint_13_add_4_11.INJECT1_1 = "NO";
    CCU2D Qint_13_add_4_9 (.A0(Q_c_7), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_8), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n97), 
          .COUT(n98), .S0(n98_adj_6), .S1(n97_adj_5));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13_add_4_9.INIT0 = 16'hfaaa;
    defparam Qint_13_add_4_9.INIT1 = 16'hfaaa;
    defparam Qint_13_add_4_9.INJECT1_0 = "NO";
    defparam Qint_13_add_4_9.INJECT1_1 = "NO";
    CCU2D Qint_13_add_4_7 (.A0(Q_c_5), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_6), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n96), 
          .COUT(n97), .S0(n100_adj_8), .S1(n99_adj_7));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13_add_4_7.INIT0 = 16'hfaaa;
    defparam Qint_13_add_4_7.INIT1 = 16'hfaaa;
    defparam Qint_13_add_4_7.INJECT1_0 = "NO";
    defparam Qint_13_add_4_7.INJECT1_1 = "NO";
    CCU2D Qint_13_add_4_5 (.A0(Q_c_3), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_4), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n95), 
          .COUT(n96), .S0(n102_adj_10), .S1(n101_adj_9));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13_add_4_5.INIT0 = 16'hfaaa;
    defparam Qint_13_add_4_5.INIT1 = 16'hfaaa;
    defparam Qint_13_add_4_5.INJECT1_0 = "NO";
    defparam Qint_13_add_4_5.INJECT1_1 = "NO";
    CCU2D Qint_13_add_4_3 (.A0(Q_c_1), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_2), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n94), 
          .COUT(n95), .S0(n104), .S1(n103_adj_11));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13_add_4_3.INIT0 = 16'hfaaa;
    defparam Qint_13_add_4_3.INIT1 = 16'hfaaa;
    defparam Qint_13_add_4_3.INJECT1_0 = "NO";
    defparam Qint_13_add_4_3.INJECT1_1 = "NO";
    CCU2D Qint_13_add_4_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_0), .B1(GND_net), .C1(GND_net), .D1(GND_net), .COUT(n94), 
          .S1(n105));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_13_add_4_1.INIT0 = 16'hF000;
    defparam Qint_13_add_4_1.INIT1 = 16'h0555;
    defparam Qint_13_add_4_1.INJECT1_0 = "NO";
    defparam Qint_13_add_4_1.INJECT1_1 = "NO";
    OB Q_pad_17 (.I(Q_c_17), .O(Q[17]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    OB Q_pad_16 (.I(Q_c_16), .O(Q[16]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    OB Q_pad_15 (.I(Q_c_15), .O(Q[15]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    OB Q_pad_14 (.I(Q_c_14), .O(Q[14]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    OB Q_pad_13 (.I(Q_c_13), .O(Q[13]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    OB Q_pad_12 (.I(Q_c_12), .O(Q[12]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    OB Q_pad_11 (.I(Q_c_11), .O(Q[11]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    OB Q_pad_10 (.I(Q_c_10), .O(Q[10]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    OB Q_pad_9 (.I(Q_c_9), .O(Q[9]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    OB Q_pad_8 (.I(Q_c_8), .O(Q[8]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    OB Q_pad_7 (.I(Q_c_7), .O(Q[7]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    OB Q_pad_6 (.I(Q_c_6), .O(Q[6]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    OB Q_pad_5 (.I(Q_c_5), .O(Q[5]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    OB Q_pad_4 (.I(Q_c_4), .O(Q[4]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    OB Q_pad_3 (.I(Q_c_3), .O(Q[3]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    OB Q_pad_2 (.I(Q_c_2), .O(Q[2]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    OB Q_pad_1 (.I(Q_c_1), .O(Q[1]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    OB Q_pad_0 (.I(Q_c_0), .O(Q[0]));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(11[2:3])
    IB clock_pad (.I(clock), .O(clock_c));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(9[2:7])
    IB reset_pad (.I(reset), .O(reset_c));   // l:/travail/vhdl_s3/tp1/compteurtuto.vhd(10[2:7])
    
endmodule
//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

