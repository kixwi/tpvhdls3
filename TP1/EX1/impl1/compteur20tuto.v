/* Verilog model created from schematic compteur20tuto.sch -- Sep 18, 2023 09:27 */

module compteur20tuto( clock, Q, reset );
output clock;
output [19:0] Q;
 input reset;
wire N_1;



VLO I3 ( .Z(N_1) );
defparam I1.NOM_FREQ="2.08";
OSCH I1 ( .OSC(clock), .STDBY(N_1) );
compteur20 I2 ( .clock(clock), .Q(Q[19:0]), .reset(reset) );

endmodule // compteur20tuto
