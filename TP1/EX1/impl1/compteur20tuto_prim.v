// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.12.0.240.2
// Netlist written on Mon Sep 18 09:32:01 2023
//
// Verilog Description of module compteur20tuto
//

module compteur20tuto (clock, Q, reset) /* synthesis syn_module_defined=1 */ ;   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(3[8:22])
    output clock;   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(4[8:13])
    output [19:0]Q;   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    input reset;   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(6[8:13])
    
    wire clock_c /* synthesis SET_AS_NETWORK=clock_c, is_clock=1 */ ;   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(4[8:13])
    
    wire Q_c_19, Q_c_18, Q_c_17, Q_c_16, Q_c_15, Q_c_14, Q_c_13, 
        Q_c_12, Q_c_11, Q_c_10, Q_c_9, Q_c_8, Q_c_7, Q_c_6, Q_c_5, 
        Q_c_4, Q_c_3, Q_c_2, Q_c_1, Q_c_0, reset_c, GND_net, n27, 
        VCC_net;
    
    OSCH I1 (.STDBY(GND_net), .OSC(clock_c)) /* synthesis syn_instantiated=1 */ ;
    defparam I1.NOM_FREQ = "2.08";
    VLO i4 (.Z(GND_net));
    OB Q_pad_19 (.I(Q_c_19), .O(Q[19]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    OB clock_pad (.I(clock_c), .O(clock));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(4[8:13])
    GSR GSR_INST (.GSR(VCC_net));
    TSALL TSALL_INST (.TSALL(GND_net));
    LUT4 i10_1_lut (.A(reset_c), .Z(n27)) /* synthesis lut_function=(!(A)) */ ;   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(6[8:13])
    defparam i10_1_lut.init = 16'h5555;
    compteur20 I2 (.Q_c_19(Q_c_19), .clock_c(clock_c), .n27(n27), .Q_c_0(Q_c_0), 
            .Q_c_18(Q_c_18), .Q_c_17(Q_c_17), .Q_c_16(Q_c_16), .Q_c_15(Q_c_15), 
            .Q_c_14(Q_c_14), .Q_c_13(Q_c_13), .Q_c_12(Q_c_12), .Q_c_11(Q_c_11), 
            .Q_c_10(Q_c_10), .Q_c_9(Q_c_9), .Q_c_8(Q_c_8), .Q_c_7(Q_c_7), 
            .Q_c_6(Q_c_6), .Q_c_5(Q_c_5), .Q_c_4(Q_c_4), .Q_c_3(Q_c_3), 
            .Q_c_2(Q_c_2), .Q_c_1(Q_c_1), .GND_net(GND_net));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(14[12:60])
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    OB Q_pad_18 (.I(Q_c_18), .O(Q[18]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    OB Q_pad_17 (.I(Q_c_17), .O(Q[17]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    OB Q_pad_16 (.I(Q_c_16), .O(Q[16]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    OB Q_pad_15 (.I(Q_c_15), .O(Q[15]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    OB Q_pad_14 (.I(Q_c_14), .O(Q[14]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    OB Q_pad_13 (.I(Q_c_13), .O(Q[13]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    OB Q_pad_12 (.I(Q_c_12), .O(Q[12]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    OB Q_pad_11 (.I(Q_c_11), .O(Q[11]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    OB Q_pad_10 (.I(Q_c_10), .O(Q[10]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    OB Q_pad_9 (.I(Q_c_9), .O(Q[9]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    OB Q_pad_8 (.I(Q_c_8), .O(Q[8]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    OB Q_pad_7 (.I(Q_c_7), .O(Q[7]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    OB Q_pad_6 (.I(Q_c_6), .O(Q[6]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    OB Q_pad_5 (.I(Q_c_5), .O(Q[5]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    OB Q_pad_4 (.I(Q_c_4), .O(Q[4]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    OB Q_pad_3 (.I(Q_c_3), .O(Q[3]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    OB Q_pad_2 (.I(Q_c_2), .O(Q[2]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    OB Q_pad_1 (.I(Q_c_1), .O(Q[1]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    OB Q_pad_0 (.I(Q_c_0), .O(Q[0]));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(5[15:16])
    IB reset_pad (.I(reset), .O(reset_c));   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(6[8:13])
    VHI i11 (.Z(VCC_net));
    
endmodule
//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module compteur20
//

module compteur20 (Q_c_19, clock_c, n27, Q_c_0, Q_c_18, Q_c_17, 
            Q_c_16, Q_c_15, Q_c_14, Q_c_13, Q_c_12, Q_c_11, Q_c_10, 
            Q_c_9, Q_c_8, Q_c_7, Q_c_6, Q_c_5, Q_c_4, Q_c_3, Q_c_2, 
            Q_c_1, GND_net);
    output Q_c_19;
    input clock_c;
    input n27;
    output Q_c_0;
    output Q_c_18;
    output Q_c_17;
    output Q_c_16;
    output Q_c_15;
    output Q_c_14;
    output Q_c_13;
    output Q_c_12;
    output Q_c_11;
    output Q_c_10;
    output Q_c_9;
    output Q_c_8;
    output Q_c_7;
    output Q_c_6;
    output Q_c_5;
    output Q_c_4;
    output Q_c_3;
    output Q_c_2;
    output Q_c_1;
    input GND_net;
    
    wire clock_c /* synthesis SET_AS_NETWORK=clock_c, is_clock=1 */ ;   // l:/travail/vhdl_s3/tp1/impl1/compteur20tuto.v(4[8:13])
    wire [19:0]n85;
    
    wire n37, n36, n35, n34, n33, n32, n31, n30, n29, n28;
    
    FD1S3IX Qint_8__i20 (.D(n85[19]), .CK(clock_c), .CD(n27), .Q(Q_c_19)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i20.GSR = "ENABLED";
    FD1S3IX Qint_8__i1 (.D(n85[0]), .CK(clock_c), .CD(n27), .Q(Q_c_0)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i1.GSR = "ENABLED";
    FD1S3IX Qint_8__i19 (.D(n85[18]), .CK(clock_c), .CD(n27), .Q(Q_c_18)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i19.GSR = "ENABLED";
    FD1S3IX Qint_8__i18 (.D(n85[17]), .CK(clock_c), .CD(n27), .Q(Q_c_17)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i18.GSR = "ENABLED";
    FD1S3IX Qint_8__i17 (.D(n85[16]), .CK(clock_c), .CD(n27), .Q(Q_c_16)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i17.GSR = "ENABLED";
    FD1S3IX Qint_8__i16 (.D(n85[15]), .CK(clock_c), .CD(n27), .Q(Q_c_15)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i16.GSR = "ENABLED";
    FD1S3IX Qint_8__i15 (.D(n85[14]), .CK(clock_c), .CD(n27), .Q(Q_c_14)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i15.GSR = "ENABLED";
    FD1S3IX Qint_8__i14 (.D(n85[13]), .CK(clock_c), .CD(n27), .Q(Q_c_13)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i14.GSR = "ENABLED";
    FD1S3IX Qint_8__i13 (.D(n85[12]), .CK(clock_c), .CD(n27), .Q(Q_c_12)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i13.GSR = "ENABLED";
    FD1S3IX Qint_8__i12 (.D(n85[11]), .CK(clock_c), .CD(n27), .Q(Q_c_11)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i12.GSR = "ENABLED";
    FD1S3IX Qint_8__i11 (.D(n85[10]), .CK(clock_c), .CD(n27), .Q(Q_c_10)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i11.GSR = "ENABLED";
    FD1S3IX Qint_8__i10 (.D(n85[9]), .CK(clock_c), .CD(n27), .Q(Q_c_9)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i10.GSR = "ENABLED";
    FD1S3IX Qint_8__i9 (.D(n85[8]), .CK(clock_c), .CD(n27), .Q(Q_c_8)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i9.GSR = "ENABLED";
    FD1S3IX Qint_8__i8 (.D(n85[7]), .CK(clock_c), .CD(n27), .Q(Q_c_7)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i8.GSR = "ENABLED";
    FD1S3IX Qint_8__i7 (.D(n85[6]), .CK(clock_c), .CD(n27), .Q(Q_c_6)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i7.GSR = "ENABLED";
    FD1S3IX Qint_8__i6 (.D(n85[5]), .CK(clock_c), .CD(n27), .Q(Q_c_5)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i6.GSR = "ENABLED";
    FD1S3IX Qint_8__i5 (.D(n85[4]), .CK(clock_c), .CD(n27), .Q(Q_c_4)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i5.GSR = "ENABLED";
    FD1S3IX Qint_8__i4 (.D(n85[3]), .CK(clock_c), .CD(n27), .Q(Q_c_3)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i4.GSR = "ENABLED";
    FD1S3IX Qint_8__i3 (.D(n85[2]), .CK(clock_c), .CD(n27), .Q(Q_c_2)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i3.GSR = "ENABLED";
    FD1S3IX Qint_8__i2 (.D(n85[1]), .CK(clock_c), .CD(n27), .Q(Q_c_1)) /* synthesis syn_use_carry_chain=1 */ ;   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8__i2.GSR = "ENABLED";
    CCU2D Qint_8_add_4_21 (.A0(Q_c_19), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(GND_net), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n37), 
          .S0(n85[19]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8_add_4_21.INIT0 = 16'hfaaa;
    defparam Qint_8_add_4_21.INIT1 = 16'h0000;
    defparam Qint_8_add_4_21.INJECT1_0 = "NO";
    defparam Qint_8_add_4_21.INJECT1_1 = "NO";
    CCU2D Qint_8_add_4_19 (.A0(Q_c_17), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_18), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n36), 
          .COUT(n37), .S0(n85[17]), .S1(n85[18]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8_add_4_19.INIT0 = 16'hfaaa;
    defparam Qint_8_add_4_19.INIT1 = 16'hfaaa;
    defparam Qint_8_add_4_19.INJECT1_0 = "NO";
    defparam Qint_8_add_4_19.INJECT1_1 = "NO";
    CCU2D Qint_8_add_4_17 (.A0(Q_c_15), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_16), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n35), 
          .COUT(n36), .S0(n85[15]), .S1(n85[16]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8_add_4_17.INIT0 = 16'hfaaa;
    defparam Qint_8_add_4_17.INIT1 = 16'hfaaa;
    defparam Qint_8_add_4_17.INJECT1_0 = "NO";
    defparam Qint_8_add_4_17.INJECT1_1 = "NO";
    CCU2D Qint_8_add_4_15 (.A0(Q_c_13), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_14), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n34), 
          .COUT(n35), .S0(n85[13]), .S1(n85[14]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8_add_4_15.INIT0 = 16'hfaaa;
    defparam Qint_8_add_4_15.INIT1 = 16'hfaaa;
    defparam Qint_8_add_4_15.INJECT1_0 = "NO";
    defparam Qint_8_add_4_15.INJECT1_1 = "NO";
    CCU2D Qint_8_add_4_13 (.A0(Q_c_11), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_12), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n33), 
          .COUT(n34), .S0(n85[11]), .S1(n85[12]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8_add_4_13.INIT0 = 16'hfaaa;
    defparam Qint_8_add_4_13.INIT1 = 16'hfaaa;
    defparam Qint_8_add_4_13.INJECT1_0 = "NO";
    defparam Qint_8_add_4_13.INJECT1_1 = "NO";
    CCU2D Qint_8_add_4_11 (.A0(Q_c_9), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_10), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n32), 
          .COUT(n33), .S0(n85[9]), .S1(n85[10]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8_add_4_11.INIT0 = 16'hfaaa;
    defparam Qint_8_add_4_11.INIT1 = 16'hfaaa;
    defparam Qint_8_add_4_11.INJECT1_0 = "NO";
    defparam Qint_8_add_4_11.INJECT1_1 = "NO";
    CCU2D Qint_8_add_4_9 (.A0(Q_c_7), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_8), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n31), 
          .COUT(n32), .S0(n85[7]), .S1(n85[8]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8_add_4_9.INIT0 = 16'hfaaa;
    defparam Qint_8_add_4_9.INIT1 = 16'hfaaa;
    defparam Qint_8_add_4_9.INJECT1_0 = "NO";
    defparam Qint_8_add_4_9.INJECT1_1 = "NO";
    CCU2D Qint_8_add_4_7 (.A0(Q_c_5), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_6), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n30), 
          .COUT(n31), .S0(n85[5]), .S1(n85[6]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8_add_4_7.INIT0 = 16'hfaaa;
    defparam Qint_8_add_4_7.INIT1 = 16'hfaaa;
    defparam Qint_8_add_4_7.INJECT1_0 = "NO";
    defparam Qint_8_add_4_7.INJECT1_1 = "NO";
    CCU2D Qint_8_add_4_5 (.A0(Q_c_3), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_4), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n29), 
          .COUT(n30), .S0(n85[3]), .S1(n85[4]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8_add_4_5.INIT0 = 16'hfaaa;
    defparam Qint_8_add_4_5.INIT1 = 16'hfaaa;
    defparam Qint_8_add_4_5.INJECT1_0 = "NO";
    defparam Qint_8_add_4_5.INJECT1_1 = "NO";
    CCU2D Qint_8_add_4_3 (.A0(Q_c_1), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_2), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n28), 
          .COUT(n29), .S0(n85[1]), .S1(n85[2]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8_add_4_3.INIT0 = 16'hfaaa;
    defparam Qint_8_add_4_3.INIT1 = 16'hfaaa;
    defparam Qint_8_add_4_3.INJECT1_0 = "NO";
    defparam Qint_8_add_4_3.INJECT1_1 = "NO";
    CCU2D Qint_8_add_4_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(Q_c_0), .B1(GND_net), .C1(GND_net), .D1(GND_net), .COUT(n28), 
          .S1(n85[0]));   // C:/lscc/diamond/3.12/ispfpga/vhdl_packages/syn_unsi.vhd(118[20:31])
    defparam Qint_8_add_4_1.INIT0 = 16'hF000;
    defparam Qint_8_add_4_1.INIT1 = 16'h0555;
    defparam Qint_8_add_4_1.INJECT1_0 = "NO";
    defparam Qint_8_add_4_1.INJECT1_1 = "NO";
    
endmodule
//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

