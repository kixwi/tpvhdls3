library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity compteur20 is

port(
	clock : in std_logic;
	reset : in std_logic;
	Q : out std_logic_vector(19 downto 0));
end;

architecture description of compteur20 is
	signal Qint : std_logic_vector(19 downto 0);
	begin
		process(clock)
		begin
			if(clock='1' and clock'event) then
				if(reset='0') then Qint <= x"00000";
				else
				Qint <= Qint + 1;
				end if;
			end if;
		end process;
	Q <= Qint;
end description;
